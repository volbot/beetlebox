package org.volbot.beetlebox.entity.storage;

import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.LarvaJarItem;
import org.volbot.beetlebox.item.tools.JarUtils;

public class EntityPlacementDispenserBehavior extends FallibleItemDispenserBehavior {
	@Override
	protected ItemStack dispenseSilently(BlockPointer pointer, ItemStack stack) {
		this.setSuccess(false);
		Item item = stack.getItem();
		if (item instanceof BeetleJarItem || item instanceof LarvaJarItem) {
			Direction direction = pointer.getBlockState().get(DispenserBlock.FACING);
			BlockPos blockPos = pointer.getPos().offset(direction);
			this.setSuccess(JarUtils.trySpawnFromJar(pointer.getWorld(), stack, blockPos, null, null).isPresent());
		}
		return stack;
	}
}
