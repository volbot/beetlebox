package org.volbot.beetlebox.entity.storage;

public interface IEntityContainer {

    //####################
    //## LIST BEHAVIOUR ##
    //####################

	/**
	 * Fetches the entity currently stored at the given slot. 
     * If the slot is empty, or is outside the bounds of this inventory, returns ContainedEntity.EMPTY.
	 */
    ContainedEntity getContained(int slot);
	/**
	 * Removes and returns the entity currently stored at the indicated slot.
	 */
    ContainedEntity removeContained(int slot);
    void setContained(int slot, ContainedEntity e);
    boolean isValid(int slot, ContainedEntity e);

    //#####################
    //## STACK BEHAVIOUR ##
    //#####################
    
	/**
	 * Removes the entity currently stored at the highest-numbered slot.
     * If slot is empty, returns ContainedEntity.EMPTY.
	 */
    ContainedEntity popContained();
	/**
	 * Inserts supplied entity into the highest-numbered available slot.
     *
     * @return true on success, false on failure.
	 */
    boolean pushContained(ContainedEntity e);
	boolean canPushContained();
	boolean canPopContained();

    //###################
    //## GENERAL UTILS ##
    //###################

    int getEntityContainerSize();
    boolean isEntityContainerEmpty();
	
}
