package org.volbot.beetlebox.entity.beetle;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.control.FlightMoveControl;
import net.minecraft.entity.ai.control.MoveControl;
import net.minecraft.entity.ai.goal.AttackWithOwnerGoal;
import net.minecraft.entity.ai.goal.EscapeDangerGoal;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.ai.goal.RevengeGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.TemptGoal;
import net.minecraft.entity.ai.goal.TrackOwnerAttackerGoal;
import net.minecraft.entity.ai.pathing.BirdNavigation;
import net.minecraft.entity.ai.pathing.EntityNavigation;
import net.minecraft.entity.ai.pathing.SpiderNavigation;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.DamageTypes;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.recipe.Ingredient;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.EntityView;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.World;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleClass;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.entity.beetle.ai.*;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;
import org.volbot.beetlebox.registry.BBSoundRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BeetleEntity extends TameableEntity {

  private static final TrackedData<Byte> CLIMBING =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.BYTE);
  private static final TrackedData<Byte> FLYING =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.BYTE);
  private static final TrackedData<Byte> TARGETING =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.BYTE);
  private static final TrackedData<Integer> BB_SIZE =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.INTEGER);
  private static final TrackedData<Integer> BB_DAMAGE =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.INTEGER);
  private static final TrackedData<Integer> BB_SPEED =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.INTEGER);
  private static final TrackedData<Integer> BB_HEALTH =
      DataTracker.registerData(BeetleEntity.class, TrackedDataHandlerRegistry.INTEGER);

  private Multimap<EntityAttribute, EntityAttributeModifier> current_modifiers =
      HashMultimap.create();

  private boolean isLandNavigator;
  public boolean unSynced = true;
  public int tame_progress = 0;
  public int attackTime = -1;
  public int timeFlying = 5;
  public boolean wasFlying = false;
  public BeetleClass beetle_class = BeetleClass.NONE;

  public Ingredient healing_ingredient = Ingredient.ofItems(Items.SUGAR_CANE);

  public BeetleEntity(EntityType<? extends TameableEntity> entityType, World world) {
    super(entityType, world);

    switchNavigator(true);
  }

  @Override
  protected void initGoals() {
    this.goalSelector.add(0, new SwimGoal(this));
    this.goalSelector.add(0, new EscapeDangerGoal(this, 1.0));
    this.goalSelector.add(2, new BeetleMeleeAttackGoal(this, 1.0, false));
    this.goalSelector.add(3, new BeetleFlyWithPlayerGoal(this, 1.0));
    this.goalSelector.add(4, new FollowOwnerGoal(this, 1.0, 6.0f, 2.0f, false));
    this.goalSelector.add(
        5, new TemptGoal(this, 1.0, Ingredient.ofItems(ItemRegistry.BEETLE_JELLY), false));
    this.goalSelector.add(6, new BeetleFlyToTreeGoal(this, 0.75));
    this.goalSelector.add(7, new LookAtEntityGoal(this, PlayerEntity.class, 8.0f));
    this.goalSelector.add(8, new LookAroundGoal(this));
    this.targetSelector.add(0, new TrackOwnerAttackerGoal(this));
    this.targetSelector.add(1, new AttackWithOwnerGoal(this));
    this.targetSelector.add(2, new BeetleWildFightGoal(this, 25));
    this.targetSelector.add(3, new RevengeGoal(this, PlayerEntity.class));
  }

  @Override
  public void tick() {
    super.tick();
    World world = getWorld();
    // if (!world.isClient) {
    this.setClimbingWall(this.horizontalCollision && !this.isFlying());
    final boolean isFlying = isFlying();
    if (isFlying && this.isLandNavigator) {
      switchNavigator(false);
    }
    if (!isFlying && !this.isLandNavigator) {
      switchNavigator(true);
    }
    if (isFlying != wasFlying) {
      timeFlying = 0;
      wasFlying = isFlying;
    }
    timeFlying++;
    if (isFlying) {
      this.setNoGravity(true);
      if (this.isInLove()) {
        this.setFlying(false);
      }
      if (timeFlying % 8 == 1) {
        this.playSound(BBSoundRegistry.BEETLE_FLY, 1.0F, 1.0F);
      }
    } else {
      this.setNoGravity(false);
    }
    // }
    if (world.isClient()) {
      if (this.isAttacking() && this.attackTime < 0) {
        this.attackTime = 5;
      }
      if (attackTime >= 0) {
        this.attackTime--;
      }
    }
  }

  @Override
  protected void eat(PlayerEntity player, Hand hand, ItemStack stack) {
    if (stack.isOf(Items.SUGAR_CANE)) this.heal(2.5f);
    if (stack.isOf(ItemRegistry.BEETLE_JELLY)) {
      if (!this.eatJelly(stack)) return;
    }
    if (stack.isOf(ItemRegistry.UPGRADE_DORMANT)) {
      NbtCompound item_nbt = stack.getOrCreateNbt();
      if (item_nbt.contains("beetle_helmet_attack")) {
        this.dropItem(ItemRegistry.UPGRADE_H_ATTACK);
      }
      if (item_nbt.contains("beetle_helmet_nv")) {
        this.dropItem(ItemRegistry.UPGRADE_H_NV);
      }
      if (item_nbt.contains("beetle_chest_elytra")) {
        this.dropItem(ItemRegistry.UPGRADE_C_ELYTRA);
      }
      if (item_nbt.contains("beetle_chest_boost")) {
        this.dropItem(ItemRegistry.UPGRADE_C_BOOST);
      }
      if (item_nbt.contains("beetle_legs_wallclimb")) {
        this.dropItem(ItemRegistry.UPGRADE_L_CLIMB);
      }
      if (item_nbt.contains("beetle_legs_2jump")) {
        this.dropItem(ItemRegistry.UPGRADE_L_2JUMP);
      }
      if (item_nbt.contains("beetle_boots_falldamage")) {
        this.dropItem(ItemRegistry.UPGRADE_B_FALLDAM);
      }
      if (item_nbt.contains("beetle_boots_speed")) {
        this.dropItem(ItemRegistry.UPGRADE_B_SPEED);
      }
      if (item_nbt.contains("beetle_boots_step")) {
        this.dropItem(ItemRegistry.UPGRADE_B_STEP);
      }
    }
    super.eat(player, hand, stack);
  }

  public boolean eatJelly(ItemStack stack) {
    NbtCompound item_nbt = stack.getOrCreateNbt();
    String fruit_type = item_nbt.getString("FruitType");

    BeetleClass beetle_class =
        BeetleClass.fromFruit(fruit_type); // try to get a beetle class from the fruit type
    if (beetle_class != BeetleClass.NONE
        && beetle_class != null) { // if it produces a Class, we have a Class Jelly
      if (this.beetle_class != beetle_class) {
        this.beetle_class = beetle_class;
        return true;
      } else {
        return false;
      }
    } else { // otherwise, we have a Stat Jelly, so we continue
      float mult =
          (float) BBConfigRegistry.JELLY_STAT_INCREMENT
              / (float) BBConfigRegistry.JELLY_STAT_INCREMENT_LIMIT;
      if (!item_nbt.getBoolean("Increase")) mult *= -1;

      BeetleStat beetle_stat = BeetleStat.fromFruit(fruit_type);
      float prev = this.getStat(beetle_stat) / 10f;
      if (item_nbt.getInt("Level") < (int) Math.floor(prev)) return false;
      return this.setStat(beetle_stat, (int) (10f * (prev + mult)));
    }
  }

  @Override
  public ActionResult interactMob(PlayerEntity player, Hand hand) {
    ItemStack itemStack = player.getStackInHand(hand);
    if (itemStack.isOf(ItemRegistry.NET) || itemStack.isOf(ItemRegistry.BEETLE_JAR)) {
      return ActionResult.PASS;
    }
    World world = getWorld();
    if (itemStack.isOf(ItemRegistry.UPGRADE_DORMANT) || itemStack.isOf(ItemRegistry.BEETLE_JELLY)) {
      if (!world.isClient && this.canEat()) {
        this.eat(player, hand, itemStack);
        if (world.isClient) {
          return ActionResult.CONSUME;
        }
        return ActionResult.SUCCESS;
      }
    }
    if (this.isHealingItem(itemStack)) {
      if (this.getHealth() < this.getMaxHealth()) {
        this.eat(player, hand, itemStack);
        if (world.isClient) {
          return ActionResult.CONSUME;
        }
        return ActionResult.SUCCESS;
      }
    }
    return super.interactMob(player, hand);
  }

  @Override
  protected void drop(DamageSource source) {
    Entity entity = source.getAttacker();
    if (entity instanceof BeetleEntity) {
      super.drop(source);
    }
  }

  @Override
  public boolean damage(DamageSource source, float amount) {
    if (source.isOf(DamageTypes.CACTUS) || source.isOf(DamageTypes.FALL)) {
      return false;
    }
    World world = getWorld();
    if (!world.isClient) {
      this.setSitting(false);
    }
    Entity e = source.getAttacker();
    if (e instanceof BeetleEntity) {
      BeetleEntity b = (BeetleEntity) e;
      if (!this.isTamed() && amount >= this.getHealth()) {
        // beetle fight lost
        // this beetle should fly away and drop elytron
        if (!world.isClient) {
          this.drop(source); // do item drops as if on death
          this.runAway(b);
        }

        return false;
      }
    }
    return super.damage(source, amount);
  }

  public void runAway(LivingEntity attacker) {
    World world = this.getWorld();
    if (world.isClient) return;
    BeetleProjectileEntity proj = new BeetleProjectileEntity(world, attacker, this);
    float speed =
        (float) (3 * this.getAttributes().getValue(EntityAttributes.GENERIC_MOVEMENT_SPEED));
    proj.setVelocity(
        attacker, (float) (-12.5 * Math.PI), (float) attacker.getYaw(), 0.0F, speed * 2, 1.0F);

    proj.pickupType = PersistentProjectileEntity.PickupPermission.DISALLOWED;

    world.spawnEntity(proj);
    this.discard();
  }

  @Override
  public boolean tryAttack(Entity target) {
    float f = (float) this.getAttributeValue(EntityAttributes.GENERIC_ATTACK_DAMAGE);
    float g = (float) this.getAttributeValue(EntityAttributes.GENERIC_ATTACK_KNOCKBACK);
    float ability = (float) this.getAttributeValue(BeetleAttributes.ATTACK_TYPE);
    f /= 4;
    g /= 4;
    boolean bl;
    if (target instanceof BeetleEntity) {
      bl = target.damage(this.getDamageSources().mobAttack(this), f);
      if (!bl) {
        // beetle fight won
        // opposing beetle should fly away, with chance to drop an elytron
        bl = true;
      }
    } else bl = super.tryAttack(target);
    if (bl) {
      BeetleAbilities.BeetleAbility.values()[(int) ability].attack(this, target);
    }
    return bl;
  }

  // ---------------------
  // -- DATA MANAGEMENT --
  // ---------------------

  @Override
  @SuppressWarnings("unchecked")
  public PassiveEntity createChild(ServerWorld world, PassiveEntity other) {
    BeetleEntity e =
        ((EntityType<? extends BeetleEntity>) this.getType()).create(this.getEntityWorld());
    e.generateGeneticStats(this, (BeetleEntity) other);
    return e;
  }

  @Override
  protected void initDataTracker() {
    super.initDataTracker();
    this.dataTracker.startTracking(CLIMBING, (byte) 0);
    this.dataTracker.startTracking(FLYING, (byte) 0);
    this.dataTracker.startTracking(TARGETING, (byte) 0);
    this.dataTracker.startTracking(BB_SIZE, 15);
    this.dataTracker.startTracking(BB_DAMAGE, 15);
    this.dataTracker.startTracking(BB_SPEED, 15);
    this.dataTracker.startTracking(BB_HEALTH, 15);
  }

  @Nullable
  @Override
  public EntityData initialize(
      ServerWorldAccess world,
      LocalDifficulty difficulty,
      SpawnReason spawnReason,
      @Nullable EntityData entityData,
      @Nullable NbtCompound entityNbt) {

    Random random = world.getRandom();

    double stdev = 2;
    double mean = 15;
    for (BeetleStat beetle_stat : BeetleStat.values()) { // iterate through stats
      int new_value =
          (int) Math.round(random.nextGaussian() * stdev + mean); // apply normal distribution
      this.setStat(beetle_stat, new_value); // apply new stat
    }

    return super.initialize(world, difficulty, spawnReason, entityData, entityNbt);
  }

  @Override
  public void onTrackedDataSet(TrackedData<?> data) {
    if (BB_SIZE.equals(data)) {
      this.calculateDimensions();
      this.setYaw(this.headYaw);
      this.bodyYaw = this.headYaw;
    }

    super.onTrackedDataSet(data);
  }

  @Override
  protected EntityNavigation createNavigation(World world) {
    return new SpiderNavigation(this, world);
  }

  private void switchNavigator(boolean onLand) {
    if (!canFly() || onLand) {
      this.moveControl = new MoveControl(this);
      this.navigation = new SpiderNavigation(this, this.getEntityWorld());
      this.isLandNavigator = true;
      this.navigation.setSpeed(10.0f);
    } else {
      this.moveControl = new FlightMoveControl(this, 6, false);
      this.navigation = new BirdNavigation(this, this.getEntityWorld());
      this.isLandNavigator = false;
      this.navigation.setSpeed(1.0f);
    }
  }

  public boolean canFly() {
    return this.getAttributes().getValue(BeetleAttributes.CAN_FLY) == 1.0;
  }

  @Override
  public boolean isClimbing() {
    return (this.dataTracker.get(CLIMBING) & 1) != 0;
  }

  public boolean isTargeting() {
    return (this.dataTracker.get(TARGETING) & 1) != 0;
  }

  public void setClimbingWall(boolean climbing) {
    byte b = this.dataTracker.get(CLIMBING);
    b = climbing ? (byte) (b | 1) : (byte) (b & 0xFFFFFFFE);
    this.dataTracker.set(CLIMBING, b);
  }

  public void setTargeting(boolean targeting) {
    byte b = this.dataTracker.get(TARGETING);
    b = targeting ? (byte) (b | 1) : (byte) (b & 0xFFFFFFFE);
    this.dataTracker.set(TARGETING, b);
  }

  public boolean isFlying() {
    return (this.dataTracker.get(FLYING) & 1) != 0;
  }

  public void setFlying(boolean flying) {
    if (flying && !canFly()) {
      flying = false;
    }
    byte b = this.dataTracker.get(FLYING);
    b = flying ? (byte) (b | 1) : (byte) (b & 0xFFFFFFFE);
    this.dataTracker.set(FLYING, b);
    this.switchNavigator(!flying);
  }

  @Override
  public void setTarget(@Nullable LivingEntity target) {
    super.setTarget(target);
    this.setTargeting(this.getTarget() != null);
  }

  public boolean isOverWater() {
    BlockPos position = this.getBlockPos();
    while (position.getY() > -64 && this.getWorld().isAir(position)) {
      position = position.down();
    }
    return !this.getWorld().getFluidState(position).isEmpty();
  }

  @Override
  public EntityView method_48926() {
    return this.getWorld();
  }

  public boolean isHealingItem(ItemStack stack) {
    return healing_ingredient.test(stack);
  }

  @Override
  public EntityDimensions getDimensions(EntityPose p) {
    int size = this.getStat(BeetleStat.BB_SIZE) / 10;
    return EntityDimensions.fixed(size, size);
  }

  @Override
  public EntityGroup getGroup() {
    return EntityGroup.ARTHROPOD;
  }

  // -----------
  // -- STATS --
  // -----------

  public static DefaultAttributeContainer.Builder createBeetleAttributes(BeetleType beetle_type) {
    return MobEntity.createMobAttributes()
        .add(EntityAttributes.GENERIC_MAX_HEALTH, 1.0)
        .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.20)
        .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 2.0)
        .add(EntityAttributes.GENERIC_FLYING_SPEED, 0.5)
        .add(BeetleAttributes.CAN_FLY, beetle_type.canFly() ? 1.0 : 0.0)
        .add(BeetleAttributes.ATTACK_TYPE, beetle_type.ability().ordinal());
  }

  public void refreshAttributes() {
    float maxhealthOld = this.getMaxHealth(); // store old health value for post-adjust
    AttributeContainer attributes = this.getAttributes();
    attributes.removeModifiers(current_modifiers); // clear current modifiers
    Multimap<EntityAttribute, EntityAttributeModifier> modbuf = HashMultimap.create();
    // set modifiers
    float health = this.getStat(BeetleStat.BB_HEALTH) / 10f - 1;
    health = health * health + health;
    modbuf.put(
        EntityAttributes.GENERIC_MAX_HEALTH,
        new EntityAttributeModifier(
            EntityAttributes.GENERIC_MAX_HEALTH.getTranslationKey(),
            health,
            EntityAttributeModifier.Operation.MULTIPLY_BASE));
    float speed = this.getStat(BeetleStat.BB_SPEED) / 15f - 1;
    modbuf.put(
        EntityAttributes.GENERIC_MOVEMENT_SPEED,
        new EntityAttributeModifier(
            EntityAttributes.GENERIC_MOVEMENT_SPEED.getTranslationKey(),
            speed,
            EntityAttributeModifier.Operation.MULTIPLY_BASE));
    float damage = this.getStat(BeetleStat.BB_DAMAGE) / 10f - 1;
    modbuf.put(
        EntityAttributes.GENERIC_ATTACK_DAMAGE,
        new EntityAttributeModifier(
            EntityAttributes.GENERIC_ATTACK_DAMAGE.getTranslationKey(),
            damage,
            EntityAttributeModifier.Operation.MULTIPLY_BASE));
    attributes.addTemporaryModifiers(modbuf);

    // heal or lose health according to changes in max health
    float maxhealthNew = this.getMaxHealth();
    if (maxhealthOld < maxhealthNew) {
      this.heal(maxhealthNew - maxhealthOld);
    } else if (this.getHealth() > maxhealthNew) {
      this.setHealth(maxhealthNew);
    }
    this.current_modifiers = modbuf;
  }

  public BeetleStat getHighestStat() {
    double high_val = -1;
    BeetleStat high_stat = null;
    for (BeetleStat beetle_stat : BeetleStat.values()) {
      int compare = this.getStat(beetle_stat);
      if (high_val < compare) {
        high_val = compare;
        high_stat = beetle_stat;
      }
    }
    return high_stat;
  }

  protected void generateGeneticStats(BeetleEntity a, BeetleEntity b) {
    double stdev = 2.0;
    for (BeetleStat beetle_stat : BeetleStat.values()) { // iterate through stats
      double mean =
          (a.getStat(beetle_stat)
              + b.getStat(beetle_stat) / 2.0); // get average of stat between parents
      int new_value =
          (int)
              Math.round(
                  a.getRandom().nextGaussian() * stdev
                      + mean); // apply normal distribution, stdev 2
      this.setStat(beetle_stat, new_value); // apply new stat
    }
  }

  public int getStat(BeetleStat beetle_stat) {
    switch (beetle_stat) {
      case BB_SIZE:
        return this.dataTracker.get(BB_SIZE);
      case BB_SPEED:
        return this.dataTracker.get(BB_SPEED);
      case BB_DAMAGE:
        return this.dataTracker.get(BB_DAMAGE);
      case BB_HEALTH:
        return this.dataTracker.get(BB_HEALTH);
      default:
        return 0;
    }
  }

  public boolean setStat(BeetleStat beetle_stat, int new_value) {
    boolean bl = true;
    if (new_value < 10) {
      new_value = 10;
      bl = false;
    } else if (new_value > 49) {
      new_value = 49;
      bl = false;
    }
    switch (beetle_stat) {
      case BB_SIZE:
        this.dataTracker.set(BB_SIZE, new_value);
        this.calculateDimensions();
        this.refreshPosition();
        return bl;
      case BB_SPEED:
        this.dataTracker.set(BB_SPEED, new_value);
        break;
      case BB_DAMAGE:
        this.dataTracker.set(BB_DAMAGE, new_value);
        break;
      case BB_HEALTH:
        this.dataTracker.set(BB_HEALTH, new_value);
        break;
    }
    this.refreshAttributes();
    return bl;
  }

  public BeetleClass getBeetleClass() {
    return this.beetle_class;
  }

  // ------------
  // -- SOUNDS --
  // ------------
  @Override
  protected SoundEvent getAmbientSound() {
    return null;
  }

  @Override
  protected SoundEvent getHurtSound(DamageSource source) {
    return BBSoundRegistry.BEETLE_HURT;
  }

  @Override
  protected SoundEvent getDeathSound() {
    return BBSoundRegistry.BEETLE_HURT;
  }

  // ---------
  // -- NBT --
  // ---------
  @Override
  public void writeCustomDataToNbt(NbtCompound compound) {
    super.writeCustomDataToNbt(compound);
    for (BeetleStat beetle_stat : BeetleStat.values()) {
      compound.putInt(beetle_stat.getName(), this.getStat(beetle_stat));
    }
    compound.putBoolean("Climbing", this.isClimbing());
    compound.putBoolean("Targeting", this.isTargeting());
    compound.putBoolean("Flying", this.isFlying());
    compound.putInt("TimeFlying", this.timeFlying);
    compound.putInt("TameProgress", tame_progress);
    compound.putInt("Class", this.beetle_class.ordinal());
  }

  public void readCustomDataFromNbt(NbtCompound compound) {
    super.readCustomDataFromNbt(compound);
    for (BeetleStat beetle_stat : BeetleStat.values()) {
      String name = beetle_stat.getName();
      if (compound.contains(name)) {
        this.setStat(beetle_stat, compound.getInt(name));
      }
    }
    if (compound.contains("Climbing")) {
      this.setClimbingWall(compound.getBoolean("Climbing"));
    } else {
      this.setClimbingWall(false);
    }
    if (compound.contains("Targeting")) {
      this.setTargeting(compound.getBoolean("Targeting"));
    } else {
      this.setTargeting(false);
    }
    if (compound.contains("Flying")) {
      this.setFlying(compound.getBoolean("Flying"));
    } else {
      this.setFlying(false);
    }
    if (compound.contains("TimeFlying")) {
      this.timeFlying = compound.getInt("TimeFlying");
    } else {
      this.timeFlying = 5;
    }
    tame_progress = compound.getInt("TameProgress");
    beetle_class = BeetleClass.values()[compound.getInt("Class")];
  }
}
