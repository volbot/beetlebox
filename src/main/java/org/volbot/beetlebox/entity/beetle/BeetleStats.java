package org.volbot.beetlebox.entity.beetle;

public class BeetleStats {
  public enum BeetleClass {
    NONE,
    INFANTRY,
    PROJECTILE;

    public static BeetleClass fromFruit(String fruit_type) {
      switch (fruit_type) {
        case "honey":
          return INFANTRY;
        case "berry":
          return PROJECTILE;
        default:
          return NONE;
      }
    }
  }

  public enum BeetleStat {
    BB_HEALTH("bb_health"),
    BB_SPEED("bb_speed"),
    BB_DAMAGE("bb_damage"),
    BB_SIZE("bb_size");

    private String name;

    BeetleStat(String name) {
      this.name = name;
    }

    public static BeetleStat fromFruit(String fruit_type) {
      switch (fruit_type) {
        case "melon":
          return BB_SIZE;
        case "sugar":
          return BB_SPEED;
        case "cactus":
          return BB_DAMAGE;
        case "apple":
          return BB_HEALTH;
        default:
          return null;
      }
    }

    public static BeetleStat fromName(String name) {
      for (BeetleStat beetle_stat : BeetleStat.values()) {
        if (beetle_stat.getName() == name) {
          return beetle_stat;
        }
      }
      return null;
    }

    public String getName() {
      return name;
    }
  }
}
