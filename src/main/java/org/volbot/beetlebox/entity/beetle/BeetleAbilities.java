package org.volbot.beetlebox.entity.beetle;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.util.math.MathHelper;

public class BeetleAbilities {

  public static enum BeetleAbility {
    FLIP,
    PINCH,
    HEADBUTT;

    public void attack(LivingEntity user, Entity target) {
      if (target instanceof LivingEntity) {
        LivingEntity target2 = (LivingEntity) target;
        switch (this) {
          case FLIP:
            if (user.isOnGround()) { // requires grounded
              target2.takeKnockback(
                  (double) (1F),
                  (double) 0f,
                  (double) (-MathHelper.cos(user.getYaw() * (float) (Math.PI / 180.0))));
            }
            break;
          case PINCH:
            if (user.isOnGround()) { // requires grounded
              target2.setYaw((float) (Math.random() * 2f * Math.PI));
            }
            break;
          case HEADBUTT:
            if (user.isOnGround()) { // requires grounded
              target2.addStatusEffect(
                  new StatusEffectInstance(StatusEffects.SLOWNESS, 50, 2), user);
            }
            break;
        }
      }
    }
  }
}
