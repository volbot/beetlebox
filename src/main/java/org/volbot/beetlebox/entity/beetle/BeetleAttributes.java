package org.volbot.beetlebox.entity.beetle;

import net.minecraft.entity.attribute.ClampedEntityAttribute;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class BeetleAttributes {
  public static final EntityAttribute CAN_FLY =
      Registry.register(
          Registries.ATTRIBUTE,
          new Identifier("beetlebox", "beetle.can_fly"),
          new ClampedEntityAttribute("attribute.name.beetlebox.beetle.can_fly", 1.0, 0.0, 1.0)
              .setTracked(true));
  public static final EntityAttribute ATTACK_TYPE =
      Registry.register(
          Registries.ATTRIBUTE,
          new Identifier("beetlebox", "beetle.attack_type"),
          new ClampedEntityAttribute(
                  "attribute.name.beetlebox.beetle.attack_type",
                  0.0,
                  0.0,
                  BeetleAbilities.BeetleAbility.values().length)
              .setTracked(true));
}
