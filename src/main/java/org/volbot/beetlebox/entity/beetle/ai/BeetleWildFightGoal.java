package org.volbot.beetlebox.entity.beetle.ai;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.UntamedActiveTargetGoal;
import net.minecraft.util.TypeFilter;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

import org.volbot.beetlebox.entity.beetle.BeetleEntity;

public class BeetleWildFightGoal extends UntamedActiveTargetGoal<BeetleEntity> {
    protected int range;

    public BeetleWildFightGoal(BeetleEntity beetle, int range) {
        super(beetle, BeetleEntity.class, true, entity -> entity!=null&&entity instanceof BeetleEntity);
        this.setControls(EnumSet.of(Goal.Control.MOVE));
        this.range = range;
    }

    @Override
    public void stop() {
        super.stop();
        var target = mob.getTarget();
        if(target instanceof BeetleEntity){
            ((BeetleEntity)target).setTarget(null);
            ((BeetleEntity)target).setAttacker(null);
        }
        mob.setTarget(null);
        mob.setAttacker(null);
    }

    @Override
    public boolean canStart() {
        if(!super.canStart()) return false;
        if (mob.getHealth()==mob.getMaxHealth()&&(mob.getTarget() != null && mob.getTarget().isAlive())) {
            //if beetle already has target, do not start
            return false;
        }
        //get up to 3 nearby beetles
        var list = getNearbyBeetles(mob.getWorld(), (BeetleEntity)mob, range, 3);
        if(list.size()>2) {
            //if either beetles has target, do not start
            if(list.get(1).getTarget()!=null || list.get(2).getTarget()!=null) return false;
            BeetleEntity e = list.get(2);
            if(e.getHealth() == e.getMaxHealth()) {
                mob.setTarget(e);
                e.setTarget(mob);
                mob.setAttacker(e);
                e.setAttacker(mob);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean shouldContinue() {
        if(!super.shouldContinue()) return false;
        BeetleEntity target = (BeetleEntity)mob.getTarget();
        return (target!=null&&target.getHealth()==target.getMaxHealth()&&mob.getHealth()==mob.getMaxHealth());
    }

    protected static List<BeetleEntity> getNearbyBeetles(World world, BeetleEntity beetle, int range, int limit) {
        ArrayList<BeetleEntity> list = new ArrayList<>();
        beetle.getWorld().collectEntitiesByType(
            TypeFilter.instanceOf(BeetleEntity.class),
            Box.of(beetle.getPos(), range, range, range),
            entity -> entity!=null&&entity instanceof BeetleEntity && entity.getHealth()>=entity.getMaxHealth(), list, limit);
        return list;
    }
}
