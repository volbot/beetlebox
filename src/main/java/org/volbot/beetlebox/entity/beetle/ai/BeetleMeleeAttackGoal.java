package org.volbot.beetlebox.entity.beetle.ai;

import java.util.EnumSet;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.pathing.Path;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.util.Hand;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;

public class BeetleMeleeAttackGoal extends Goal {

  protected BeetleEntity beetle;
  protected final double speed;
  protected final boolean pauseWhenMobIdle;
  protected Path path;
  protected double targetX;
  protected double targetY;
  protected double targetZ;
  protected int updateCountdownTicks;
  protected int cooldown;
  protected final int attackIntervalTicks = 20;
  protected long lastUpdateTime;
  protected static final long MAX_ATTACK_TIME = 20L;

  public BeetleMeleeAttackGoal(BeetleEntity beetle, double speed, boolean pauseWhenMobIdle) {
    this.beetle = beetle;
    this.speed = speed;
    this.pauseWhenMobIdle = pauseWhenMobIdle;
    this.setControls(EnumSet.of(Goal.Control.MOVE, Goal.Control.LOOK));
  }

  @Override
  public boolean canStart() {
    long l = this.beetle.getWorld().getTime();
    if (l - this.lastUpdateTime < 20L) {
      return false;
    } else {
      this.lastUpdateTime = l;
      LivingEntity livingEntity = this.beetle.getTarget();
      if (livingEntity == null) {
        return false;
      } else if (!livingEntity.isAlive()) {
        return false;
      } else {
        this.path = this.beetle.getNavigation().findPathTo(livingEntity, 0);
        if (this.path != null) {
          return true;
        } else {
          return this.getSquaredMaxAttackDistance(livingEntity)
              >= this.beetle.squaredDistanceTo(
                  livingEntity.getX(), livingEntity.getY(), livingEntity.getZ());
        }
      }
    }
  }

  @Override
  public boolean shouldContinue() {
    LivingEntity livingEntity = this.beetle.getTarget();
    if (livingEntity == null) {
      return false;
    } else if (!livingEntity.isAlive()) {
      return false;
    } else if (!this.pauseWhenMobIdle) {
      return !this.beetle.getNavigation().isIdle();
    } else if (!this.beetle.isInWalkTargetRange(livingEntity.getBlockPos())) {
      return false;
    } else {
      return !(livingEntity instanceof PlayerEntity)
          || !livingEntity.isSpectator() && !((PlayerEntity) livingEntity).isCreative();
    }
  }

  @Override
  public void start() {
    this.beetle.getNavigation().startMovingAlong(this.path, this.speed);
    this.updateCountdownTicks = 0;
    this.cooldown = 0;
  }

  @Override
  public void stop() {
    LivingEntity livingEntity = this.beetle.getTarget();
    if (!EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR.test(livingEntity)) {
      this.beetle.setTarget(null);
    }

    this.beetle.setAttacking(false);
    this.beetle.getNavigation().stop();
  }

  @Override
  public boolean shouldRunEveryTick() {
    return true;
  }

  @Override
  public void tick() {
    LivingEntity livingEntity = this.beetle.getTarget();
    if (livingEntity != null) {
      this.beetle.getLookControl().lookAt(livingEntity, 30.0F, 30.0F);
      double d = this.beetle.getSquaredDistanceToAttackPosOf(livingEntity);
      this.updateCountdownTicks = Math.max(this.updateCountdownTicks - 1, 0);
      if ((this.pauseWhenMobIdle || this.beetle.getVisibilityCache().canSee(livingEntity))
          && this.updateCountdownTicks <= 0
          && (this.targetX == 0.0 && this.targetY == 0.0 && this.targetZ == 0.0
              || livingEntity.squaredDistanceTo(this.targetX, this.targetY, this.targetZ) >= 1.0
              || this.beetle.getRandom().nextFloat() < 0.05F)) {
        this.targetX = livingEntity.getX();
        this.targetY = livingEntity.getY();
        this.targetZ = livingEntity.getZ();
        this.updateCountdownTicks = 4 + this.beetle.getRandom().nextInt(7);
        if (d > 1024.0) {
          this.updateCountdownTicks += 10;
        } else if (d > 256.0) {
          this.updateCountdownTicks += 5;
        }

        if (!this.beetle.getNavigation().startMovingTo(livingEntity, this.speed)) {
          this.updateCountdownTicks += 15;
        }

        this.updateCountdownTicks = this.getTickCount(this.updateCountdownTicks);
      }

      this.cooldown = Math.max(this.cooldown - 1, 0);
      this.attack(livingEntity, d);
    }
  }

  protected void attack(LivingEntity target, double squaredDistance) {
    double d = this.getSquaredMaxAttackDistance(target);
    if (squaredDistance <= d && this.cooldown <= 0) {
      this.resetCooldown();
      this.beetle.swingHand(Hand.MAIN_HAND);
      this.beetle.tryAttack(target);
      this.beetle.setAttacking(true);
    } else {
      this.beetle.setAttacking(false);
    }
  }

  protected void resetCooldown() {
    this.cooldown = this.getTickCount(20);
  }

  protected boolean isCooledDown() {
    return this.cooldown <= 0;
  }

  protected int getCooldown() {
    return this.cooldown;
  }

  protected int getMaxCooldown() {
    return this.getTickCount(20);
  }

  protected double getSquaredMaxAttackDistance(LivingEntity entity) {
    return (double)
        (this.beetle.getWidth() * 2.5F * this.beetle.getWidth() * 2.5F + entity.getWidth());
  }
}
