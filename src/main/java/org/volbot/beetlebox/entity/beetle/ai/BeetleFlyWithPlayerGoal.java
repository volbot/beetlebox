package org.volbot.beetlebox.entity.beetle.ai;

import java.util.EnumSet;

import org.jetbrains.annotations.Nullable;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;

import org.volbot.beetlebox.entity.beetle.BeetleEntity;

public class BeetleFlyWithPlayerGoal extends Goal {
	private final BeetleEntity beetle;
	private final double speed;
	@Nullable
	private PlayerEntity owner;

	public BeetleFlyWithPlayerGoal(BeetleEntity beetle, double speed) {
		this.beetle = beetle;
		this.speed = speed;
		this.setControls(EnumSet.of(Goal.Control.MOVE, Goal.Control.LOOK));
	}

	@Override
	public boolean canStart() {
		this.owner = (PlayerEntity) this.beetle.getOwner();
		if (this.owner == null) {
			return false;
		}
		return this.owner.isFallFlying() && this.beetle.getTarget() != this.owner;
	}

	@Override
	public boolean shouldContinue() {
		return this.owner != null && this.owner.isFallFlying();
	}

	@Override
	public void start() {
		if (!beetle.isFlying()) {
			beetle.setFlying(true);
		}
	}

	@Override
	public void stop() {
		this.owner = null;
		this.beetle.getNavigation().stop();
	}

	@Override
	public void tick() {
        beetle.setPitch(owner.getPitch());
        beetle.setYaw(owner.getYaw());
        Vec3d owner_velocity = this.owner.getVelocity();
        Vec3d new_velocity = this.beetle.getVelocity();
        new_velocity=new_velocity.relativize(owner_velocity);

        if (this.beetle.squaredDistanceTo(this.owner) > 1) {
            if (this.beetle.squaredDistanceTo(this.owner) > 3) {
                new_velocity = owner_velocity.multiply(1.2);
            } else {
                new_velocity = owner_velocity.multiply(1.1);
            }
        }
        Vec3d owner_displacement = owner.getPos().subtract(beetle.getPos()).normalize().multiply(1.00);
        new_velocity = new_velocity.add(owner_displacement);
        LivingEntity e = this.beetle.getWorld().getClosestEntity(
                BeetleEntity.class, 
                TargetPredicate.createNonAttackable(), 
                beetle, 
                beetle.getX(), 
                beetle.getY(), 
                beetle.getZ(), 
                Box.of(beetle.getPos(), 4, 4, 4)
                );
        if ( e != null ) {
            Vec3d entity_displacement = e.getPos().subtract(this.beetle.getPos()).multiply(0.5);
            //entity_displacement = entity_displacement.multiply(owner_velocity.normalize().negate());
            new_velocity=new_velocity.subtract(entity_displacement);
        }
        this.beetle.setVelocity(new_velocity);
    }
}
