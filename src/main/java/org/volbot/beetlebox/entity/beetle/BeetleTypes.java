package org.volbot.beetlebox.entity.beetle;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.fabricmc.fabric.api.tag.convention.v1.ConventionalBiomeTags;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.ArmorItem.Type;
import net.minecraft.item.Item;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.world.biome.Biome;
import org.volbot.beetlebox.entity.beetle.BeetleAbilities.BeetleAbility;
import org.volbot.beetlebox.item.BeetleProduct;
import org.volbot.beetlebox.item.BeetleSpawnEggItem;
import org.volbot.beetlebox.item.equipment.BeetleArmorItem;
import org.volbot.beetlebox.item.equipment.BeetleArmorTypes.ArmorPattern;
import org.volbot.beetlebox.item.equipment.BeetleArmorTypes.HelmetShape;
import org.volbot.beetlebox.item.equipment.materials.ChitinMaterial;

public class BeetleTypes {

  // TODO: custom biome tags
  public static List<TagKey<Biome>> WARM_HABITATS = List.of(ConventionalBiomeTags.CLIMATE_HOT);
  public static List<TagKey<Biome>> PLAINS_HABITATS = List.of(ConventionalBiomeTags.PLAINS);
  public static List<TagKey<Biome>> RAINFOREST_HABITATS =
      List.of(ConventionalBiomeTags.TREE_JUNGLE);
  public static List<TagKey<Biome>> FOREST_HABITATS = List.of(ConventionalBiomeTags.TREE_DECIDUOUS);
  public static List<TagKey<Biome>> FLORAL_HABITATS = List.of(ConventionalBiomeTags.FLORAL);

  public enum BeetleType {
    A_DICHOTOMA(
        "Japanese Rhinoceros Beetle",
        "Allomyrina dichotoma",
        0x23100e,
        0x180809,
        BeetleAbility.FLIP,
        Stream.of(FOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.FLIP,
        ArmorPattern.STANDARD),
    D_HERCULES(
        "Hercules Beetle",
        "Dynastes hercules",
        0x6d5225,
        0x110f07,
        BeetleAbility.PINCH,
        Stream.of(RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        0.6f,
        HelmetShape.CLAW,
        ArmorPattern.STANDARD),
    D_TITYUS(
        "Eastern Hercules Beetle",
        "Dynastes tityus",
        0x635333,
        0x100f09,
        BeetleAbility.PINCH,
        Stream.of(FOREST_HABITATS, PLAINS_HABITATS, FLORAL_HABITATS),
        true,
        0.6f,
        HelmetShape.CLAW,
        ArmorPattern.STANDARD),
    D_TITANUS(
        "Giant Stag Beetle",
        "Dorcus titanus",
        0x161616,
        0x0e0e0e,
        BeetleAbility.PINCH,
        Stream.of(FOREST_HABITATS, RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.PINCERS,
        ArmorPattern.STANDARD),
    M_ELEPHAS(
        "Elephant Beetle",
        "Megasoma elephas",
        0x5b3613,
        0x220f06,
        BeetleAbility.FLIP,
        Stream.of(RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.FLIP,
        ArmorPattern.STANDARD),
    M_ACTAEON(
        "Actaeon Beetle",
        "Megasoma actaeon",
        0x1e1e1e,
        0x0d0d0d,
        BeetleAbility.FLIP,
        Stream.of(RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.THREE,
        ArmorPattern.STANDARD),
    C_ATLAS(
        "Atlas Beetle",
        "Chalcosoma atlas",
        0x0e1a00,
        0x0b0700,
        BeetleAbility.PINCH,
        Stream.of(RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.THREE,
        ArmorPattern.STANDARD),
    C_NITIDA(
        "Green June Beetle",
        "Cotinis nitida",
        0x081d03,
        0x7b5518,
        BeetleAbility.HEADBUTT,
        Stream.of(FOREST_HABITATS, WARM_HABITATS, PLAINS_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.STANDARD,
        ArmorPattern.STANDARD),
    A_VERRUCOSUS(
        "Blue Death Feigning Beetle",
        "Asbolus verrucosus",
        0x364d5f,
        0x0f191b,
        BeetleAbility.HEADBUTT,
        Stream.of(FOREST_HABITATS, WARM_HABITATS, FLORAL_HABITATS),
        false,
        1.0f,
        HelmetShape.STANDARD,
        ArmorPattern.STANDARD),
    G_ALBOSIGNATUS(
        "Goliath Beetle",
        "Goliathus albosignatus",
        0x101010,
        0xbbbbbb,
        BeetleAbility.HEADBUTT,
        Stream.of(RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.STANDARD,
        ArmorPattern.CUSTOM),
    C_MINUTUS(
        "Small Black Dung Beetle",
        "Copris minutus",
        0x292929,
        0x0c0c0c,
        BeetleAbility.FLIP,
        Stream.of(WARM_HABITATS, PLAINS_HABITATS, FLORAL_HABITATS),
        true,
        1.0f,
        HelmetShape.FLIP,
        ArmorPattern.STANDARD),
    T_VIRIDIAURATA(
        "Green Rhinoceros Beetle",
        "Theodosia viridiaurata",
        0x2d4409,
        0x41201d,
        BeetleAbility.PINCH,
        Stream.of(WARM_HABITATS, RAINFOREST_HABITATS, FLORAL_HABITATS),
        true,
        0.6f,
        HelmetShape.CLAW,
        ArmorPattern.CUSTOM);

    private String name_common;
    private String name_scientific;
    private int color1;
    private int color2;
    private BeetleAbility ability;
    private EntityType<BeetleEntity> entity_type;
    private List<TagKey<Biome>> habitats;
    private boolean can_fly;
    private BeetleProduct elytron;
    private BeetleArmorItem helmet;
    private BeetleArmorItem chestplate;
    private BeetleArmorItem leggings;
    private BeetleArmorItem boots;
    private BeetleSpawnEggItem spawn_egg;
    private HelmetShape helmet_shape;
    private float head_turn_scale;
    private ArmorPattern armor_pattern;

    private BeetleType(
        String name_common,
        String name_scientific,
        int color1,
        int color2,
        BeetleAbility ability,
        Stream<List<TagKey<Biome>>> habitats,
        boolean can_fly,
        float head_turn_scale,
        HelmetShape helmet_shape,
        ArmorPattern armor_pattern) {
      this.name_common = name_common;
      this.name_scientific = name_scientific;
      this.color1 = color1;
      this.color2 = color2;
      this.ability = ability;
      this.habitats = habitats.flatMap(Collection::stream).collect(Collectors.toList());
      this.can_fly = can_fly;
      this.head_turn_scale = head_turn_scale;
      this.helmet_shape = helmet_shape;
      this.entity_type =
          FabricEntityTypeBuilder.createMob()
              .entityFactory(BeetleEntity::new)
              .spawnGroup(SpawnGroup.CREATURE)
              .dimensions(EntityDimensions.fixed(0.4f, 0.4f))
              .build();
      this.elytron = new BeetleProduct(this, new FabricItemSettings());
      this.helmet =
          new BeetleArmorItem(
              this, new ChitinMaterial(this, this.elytron), Type.HELMET, new FabricItemSettings());
      this.chestplate =
          new BeetleArmorItem(
              this,
              new ChitinMaterial(this, this.elytron),
              Type.CHESTPLATE,
              new FabricItemSettings());
      this.leggings =
          new BeetleArmorItem(
              this,
              new ChitinMaterial(this, this.elytron),
              Type.LEGGINGS,
              new FabricItemSettings());
      this.boots =
          new BeetleArmorItem(
              this, new ChitinMaterial(this, this.elytron), Type.BOOTS, new FabricItemSettings());
      this.spawn_egg = new BeetleSpawnEggItem(this, new FabricItemSettings());
      this.armor_pattern = armor_pattern;
    }

    public String nameCommon() {
      return this.name_common;
    }

    public String nameScientificLong() {
      return this.name_scientific;
    }

    public String nameScientificShort() {
      String[] words = this.name_scientific.split(" ");
      return words[0].charAt(0) + ". " + words[1];
    }

    public String id() {
      String[] words = this.name_scientific.split(" ");
      return words[0].toLowerCase().charAt(0) + "_" + words[1];
    }

    public int color1() {
      return color1;
    }

    public int color2() {
      return color2;
    }

    public BeetleAbility ability() {
      return ability;
    }

    public EntityType<BeetleEntity> entityType() {
      return this.entity_type;
    }

    public List<TagKey<Biome>> habitats() {
      return habitats;
    }

    public boolean canFly() {
      return can_fly;
    }

    public ArmorPattern armorPattern() {
      return armor_pattern;
    }

    public float headTurnScale() {
      return head_turn_scale;
    }

    public Item elytron() {
      return elytron;
    }

    public BeetleSpawnEggItem spawnEgg() {
      return this.spawn_egg;
    }

    public BeetleArmorItem[] armorItems() {
      BeetleArmorItem[] val = {helmet, chestplate, leggings, boots};
      return val;
    }

    public HelmetShape helmetShape() {
      return helmet_shape;
    }

    private static final Map<String, BeetleType> BEETLE_TYPE_MAP;

    static {
      Map<String, BeetleType> map = new HashMap<String, BeetleType>();
      for (BeetleType instance : BeetleType.values()) {
        map.put(instance.id(), instance);
      }
      BEETLE_TYPE_MAP = Collections.unmodifiableMap(map);
    }

    public static BeetleType get(String id) {
      return BEETLE_TYPE_MAP.get(id);
    }
  }
}
