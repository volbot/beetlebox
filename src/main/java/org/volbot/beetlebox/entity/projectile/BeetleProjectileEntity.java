package org.volbot.beetlebox.entity.projectile;

import java.util.Optional;
import java.util.UUID;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FlyingItemEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.GameStateChangeS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.volbot.beetlebox.data.damage.BeetleDamageTypes;
import org.volbot.beetlebox.entity.beetle.BeetleAttributes;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.JarUtils;
import org.volbot.beetlebox.registry.BBSoundRegistry;
import org.volbot.beetlebox.registry.BeetleRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BeetleProjectileEntity extends PersistentProjectileEntity implements FlyingItemEntity {

  public BeetleEntity entity;
  public boolean landed = false;
  public boolean unSynced = true;

  public BeetleProjectileEntity(
      EntityType<? extends PersistentProjectileEntity> entityType, World world) {
    super(entityType, world);
  }

  public BeetleProjectileEntity(World world) {
    super(BeetleRegistry.BEETLE_PROJECTILE, world);
  }

  public BeetleProjectileEntity(World world, double x, double y, double z) {
    super(BeetleRegistry.BEETLE_PROJECTILE, x, y, z, world);
  }

  public BeetleProjectileEntity(World world, LivingEntity owner, ItemStack jar_stack) {
    this(world, owner, (LivingEntity) JarUtils.getEntityFromJar(jar_stack, world).get());
  }

  public BeetleProjectileEntity(World world, LivingEntity owner, LivingEntity entity) {
    super(BeetleRegistry.BEETLE_PROJECTILE, owner, world);
    this.entity = (BeetleEntity) entity;
    this.entity.setFlying(true);
    if (this.entity.getAttributes().getValue(BeetleAttributes.CAN_FLY) == 1.0) {
      this.setNoGravity(true);
    }
    if (owner instanceof BeetleEntity) {
      this.setNoClip(true);
    }
    this.sendPacket();
  }

  @Override
  public void onPlayerCollision(PlayerEntity player) {
    World world = this.getWorld();
    boolean discard = false;
    if (!world.isClient) {
      if (this.tryPickup(player)) {
        Optional<Entity> op = JarUtils.getEntityFromJar(this.asItemStack(), world);
        if (op.isEmpty()) return;
        ItemStack jar_stack = JarUtils.takeValidJar(player, (LivingEntity) op.get());
        if (jar_stack.isEmpty()) {
          Entity e = op.get();
          if (world.spawnEntity(e)) {
            world.emitGameEvent(e, GameEvent.ENTITY_PLACE, this.getBlockPos());
            discard = true;
          }
        } else {
          ItemEntity dropped =
              new ItemEntity(
                  this.getWorld(),
                  this.getX(),
                  this.getY() + (double) 0.1f,
                  this.getZ(),
                  this.asItemStack());
          dropped.setPickupDelay(0);
          dropped.setOwner(player.getUuid());
          dropped.setNoGravity(true);
          dropped.setVelocity(player.getPos().subtract(dropped.getPos()).normalize().multiply(0.5));
          discard = this.getWorld().spawnEntity(dropped);
        }
      }
    }
    if (discard) {
      this.discard();
    }
  }

  protected boolean tryPickup(PlayerEntity player) {
    if (!this.landed) return false;
    switch (this.pickupType) {
      case ALLOWED:
        return true;
      case CREATIVE_ONLY:
        return player.getAbilities().creativeMode;
      default:
        return false;
    }
  }

  @Override
  protected ItemStack asItemStack() {
    ItemStack jar_stack = ItemRegistry.BEETLE_JAR.getDefaultStack().copy();
    if (entity != null && ((BeetleJarItem<?>) jar_stack.getItem()).canStore(entity))
      JarUtils.writeJarNbt(entity, jar_stack);

    return jar_stack;
  }

  protected ItemStack asItemStackWithVelocity() {
    ItemStack jar_stack = ItemRegistry.BEETLE_JAR.getDefaultStack().copy();
    if (entity != null && ((BeetleJarItem<?>) jar_stack.getItem()).canStore(entity)) {
      entity.setVelocity(this.getVelocity().multiply(6.0));
      JarUtils.writeJarNbt(entity, jar_stack);
    }

    return jar_stack;
  }

  @Override
  public ItemStack getStack() {
    return asItemStack();
  }

  @Override
  public void tick() {
    super.tick();
    World world = this.getWorld();
    boolean discard = false;
    if (!world.isClient) {
      this.random.skip(1);
      this.setVelocity(this.getVelocity().addRandom(this.random, 0.025f));
      if (landed || this.age > 45) {
        Entity owner = this.getOwner();
        if (this.age < 135
            && (owner != null
                && owner.getUuid() != null
                && entity.getOwnerUuid() != null
                && owner.getUuid().equals(entity.getOwnerUuid()))) {
          this.random.skip(1);
          this.setVelocity(
              this.getVelocity()
                  .add(owner.getPos().subtract(this.getPos()).add(0, 1, 0))
                  .normalize()
                  .multiply(0.5)
                  .addRandom(this.random, 0.075f));
        } else {
          Optional<Entity> op =
              JarUtils.trySpawnFromJar(
                  (ServerWorld) world,
                  this.asItemStackWithVelocity(),
                  this.getBlockPos(),
                  null,
                  null);
          /*
          if (!op.isEmpty()) {
            op.get().setVelocity(this.getVelocity().multiply(6.0));
            ;
          }
          */
          discard = true;
        }
      }
      if (this.age % 8 == 1) {
        this.playSound(BBSoundRegistry.BEETLE_FLY, 1.0F, 1.0F);
      }
      if (unSynced) {
        if (this.entity == null) {
          this.unSynced = false;
        } else {
          this.sendPacket();
        }
      }
    }
    if (discard) {
      this.discard();
    }
  }

  @Override
  protected void onHit(LivingEntity target) {
    super.onHit(target);
    this.onHitSetup();
  }

  @Override
  protected void onBlockHit(BlockHitResult blockHitResult) {
    if (blockHitResult.isInsideBlock()) this.setPosition(this.prevX, this.prevY, this.prevZ);
    World world = this.getWorld();
    var blockstate = world.getBlockState(blockHitResult.getBlockPos());
    SoundEvent block_sound = blockstate.getBlock().getSoundGroup(blockstate).getBreakSound();
    world.playSound(
        null,
        blockHitResult.getPos().x,
        blockHitResult.getPos().y,
        blockHitResult.getPos().z,
        block_sound,
        SoundCategory.PLAYERS,
        1.0F,
        1.0F);
    this.onHitSetup();
  }

  private void onHitSetup() {
    World world = this.getWorld();
    this.setVelocity(this.getVelocity().multiply(-1.0));
    this.setYaw(this.getYaw() + 180.0f);
    this.prevYaw += 180.0f;
    if (!world.isClient) {
      this.landed = true;
      this.sendPacket();
    }
  }

  @Override
  protected void onEntityHit(EntityHitResult entityHitResult) {
    if (this.landed) {
      return;
    }
    DamageSource damageSource;
    Entity entity2;
    Entity entity = entityHitResult.getEntity();
    World world = this.getWorld();
    double entityDamage =
        this.entity.getAttributes().getValue(EntityAttributes.GENERIC_ATTACK_DAMAGE);
    int i = MathHelper.ceil(MathHelper.clamp(entityDamage, 0.0, 10.0));
    if ((entity2 = this.getOwner()) == null) {
      damageSource = BeetleDamageTypes.of(world, BeetleDamageTypes.BEETLE_PROJ);
    } else {
      damageSource = BeetleDamageTypes.of(world, BeetleDamageTypes.BEETLE_PROJ, entity2);
    }
    boolean bl = entity.getType() == EntityType.ENDERMAN;
    if (this.isOnFire() && !bl) {
      entity.setOnFireFor(5);
    }
    if (entity.damage(damageSource, i)) {
      if (bl) {
        return;
      }
      if (entity instanceof LivingEntity) {
        LivingEntity livingEntity = (LivingEntity) entity;
        if (!world.isClient && entity2 instanceof LivingEntity) {
          EnchantmentHelper.onUserDamaged(livingEntity, entity2);
          EnchantmentHelper.onTargetDamaged((LivingEntity) entity2, livingEntity);
        }
        this.onHit(livingEntity);
        if (entity2 != null
            && livingEntity != entity2
            && livingEntity instanceof PlayerEntity
            && entity2 instanceof ServerPlayerEntity
            && !this.isSilent()) {
          ((ServerPlayerEntity) entity2)
              .networkHandler.sendPacket(
                  new GameStateChangeS2CPacket(
                      GameStateChangeS2CPacket.PROJECTILE_HIT_PLAYER,
                      GameStateChangeS2CPacket.DEMO_OPEN_SCREEN));
        }
      }
      world.playSound(
          null,
          entityHitResult.getPos().x,
          entityHitResult.getPos().y,
          entityHitResult.getPos().z,
          BBSoundRegistry.BEETLE_HIT,
          SoundCategory.PLAYERS,
          1.0F,
          1.0F);
    } else {
      this.onHitSetup();
    }
  }

  public void sendPacket() {
    World world = getEntityWorld();
    if (world.getPlayers().isEmpty()) {
      this.unSynced = true;
      return;
    }
    if (!world.isClient && entity != null) {
      ServerPlayerEntity p;
      if (this.getOwner() instanceof PlayerEntity) {
        p = (ServerPlayerEntity) this.getOwner();
      } else {
        p = (ServerPlayerEntity) world.getPlayers().get(0);
      }
      PacketByteBuf buf = PacketByteBufs.create();
      buf.writeBoolean(landed);
      NbtCompound entity_nbt = new NbtCompound();
      JarUtils.writeJarNbt(entity, entity_nbt);
      buf.writeNbt(entity_nbt);
      UUID entity_id = this.getUuid();
      buf.writeUuid(entity_id);
      ServerPlayNetworking.send(p, new Identifier("beetlebox", "beetle_proj_packet"), buf);
    }
  }

  @Override
  public NbtCompound writeNbt(NbtCompound nbt) {
    super.writeNbt(nbt);
    nbt.putBoolean("Landed", this.landed);
    nbt.putBoolean("UnSynced", this.unSynced);
    if (entity != null) {
      NbtCompound entity_nbt = new NbtCompound();
      JarUtils.writeJarNbt(entity, entity_nbt);
      nbt.put("EntityNbt", entity_nbt);
    }
    return nbt;
  }

  @Override
  public void readNbt(NbtCompound nbt) {
    super.readNbt(nbt);
    this.landed = nbt.getBoolean("Landed");
    this.unSynced = nbt.getBoolean("UnSynced");
    if (nbt.contains("EntityNbt")) {
      World world = this.getWorld();
      var opt = JarUtils.readJarNbt(nbt.getCompound("EntityNbt"), world);
      if (opt.isPresent()) entity = ((BeetleEntity) opt.get());
    } else {
      entity = null;
    }
  }
}
