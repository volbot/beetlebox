package org.volbot.beetlebox.entity.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CandleBlock;
import net.minecraft.block.FlowerBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.entity.storage.ContainedEntity;
import org.volbot.beetlebox.entity.storage.IEntityContainer;
import org.volbot.beetlebox.item.Larva;
import org.volbot.beetlebox.item.tools.JarUtils;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class TankBlockEntity extends BlockEntity implements SidedInventory, IEntityContainer {

  public ContainedEntity[] contained = {ContainedEntity.EMPTY, ContainedEntity.EMPTY};
  public Larva larva = null;
  protected DefaultedList<ItemStack> inventory = DefaultedList.ofSize(5, ItemStack.EMPTY);
  public int production_time = 0;
  public PlayerEntity owner;

  private static final int[] TOP_SLOTS = new int[] {0, 1, 2, 3, 4};
  public boolean[] decor = new boolean[] {false, false, false};

  public TankBlockEntity(BlockPos pos, BlockState state) {
    super(BlockRegistry.TANK_BLOCK_ENTITY, pos, state);
  }

  //////////////////
  // TICK METHODS //
  //////////////////
  public static void tick(World world, BlockPos pos, BlockState state, TankBlockEntity te) {
    te.tickBreedingCooldown();
    if (te.breedingSetupValid()) {
      te.breedingTick();
      return;
    }
    if (te.tamingSetupValid()) {
      te.tamingTick();
      return;
    }
  }

  public void tamingTick() {
    this.production_time++;
    if (this.production_time % 10 == 0) this.emitParticle(ParticleTypes.HAPPY_VILLAGER);

    World world = this.getWorld();
    if (world.isClient) return;

    if (this.production_time >= BBConfigRegistry.TANK_TAME_TICKS) this.tamingLevelComplete();
  }

  public void breedingTick() {
    this.production_time++;
    if (this.production_time % 10 == 0) this.emitParticle(ParticleTypes.HEART);

    World world = this.getWorld();
    if (world.isClient) return;

    if (this.production_time >= BBConfigRegistry.TANK_BREED_TICKS) this.breedingComplete();
  }

  public void tamingLevelComplete() {
    ContainedEntity ce = this.getContained(0);
    NbtCompound nbt = ce.getEntityData();

    var opt = JarUtils.getEntityFromContained(ce, world);
    if (opt.isPresent()) {
      Entity entity = opt.get();
      if (entity instanceof BeetleEntity) {
        BeetleEntity beetle = (BeetleEntity) entity;
        if (!beetle.eatJelly(this.getStack(4))) return;
        beetle.writeNbt(nbt);
        beetle.writeCustomDataToNbt(nbt);
      }
    }

    this.setProductionTime(0);

    int tame_progress = nbt.getInt("TameProgress");
    tame_progress = tame_progress + 1;
    nbt.putInt("TameProgress", tame_progress);

    this.setStack(4, ItemStack.EMPTY);

    if (tame_progress >= BBConfigRegistry.TANK_TAME_LEVELS) {
      if (this.owner != null) nbt.putUuid("Owner", this.owner.getUuid());
    }

    ce.setEntityData(nbt);
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public void breedingComplete() {
    ContainedEntity[] e = {this.getContained(0), this.getContained(1)};

    this.setLarva(new Larva(e[0], e[1], world));

    for (int i = 0; i < e.length; i++) {
      NbtCompound nbt = e[i].getEntityData();
      nbt.putInt("Age", 200);
      e[i].setEntityData(nbt);
    }

    this.setProductionTime(0);

    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public void tickBreedingCooldown() {
    for (int i = 0; i < this.getEntityContainerSize(); i++) {
      ContainedEntity ce = this.getContained(i);
      if (ce.isEmpty()) continue;
      NbtCompound nbt = ce.getEntityData();

      int age = nbt.contains("Age") ? nbt.getInt("Age") : -1;

      if (age == 0) return;

      nbt.putInt("Age", age > 0 ? age - 1 : 0);

      ce.setEntityData(nbt);

      markDirty();
      this.getWorld()
          .updateListeners(
              this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
    }
  }

  /////////////////////////
  // TANK-SPECIFIC UTILS //
  /////////////////////////
  public void setOwner(PlayerEntity player) {
    this.owner = player;
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public void setProductionTime(int production_time) {
    this.production_time = production_time;
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public boolean canContainedBreed() {
    ContainedEntity[] e = {this.getContained(0), this.getContained(1)};

    if (0 != e[0].getContainedId().compareTo(e[1].getContainedId())) return false;

    for (int i = 0; i < 2; i++) {
      NbtCompound nbt = e[i].getEntityData();
      if (nbt.contains("Age") && nbt.getInt("Age") != 0) return false;
    }

    return true;
  }

  public boolean breedingSetupValid() {
    boolean bl;
    bl =
        isEntityContainerFull()
            && canContainedBreed()
            && getStack(0).isOf(ItemRegistry.SUBSTRATE)
            && this.larva == null;
    if (!bl) return false;

    boolean hasFlower = false;
    boolean hasCandle = false;
    for (int i = 1; i <= 2; i++) {
      ItemStack stack = getStack(i);
      if (Block.getBlockFromItem(stack.getItem()) instanceof FlowerBlock) hasFlower = true;
      if (Block.getBlockFromItem(stack.getItem()) instanceof CandleBlock) hasCandle = true;
    }

    if (!hasFlower || !hasCandle) return false;

    return true;
  }

  public boolean tamingSetupValid() {
    boolean bl;
    bl =
        !(this.isEntityContainerFull() || this.isEntityContainerEmpty())
            && getContained(0).getEntityData().getInt("TameProgress")
                < BBConfigRegistry.TANK_TAME_LEVELS
            && getStack(0).isOf(ItemRegistry.SUBSTRATE)
            && getStack(4).isOf(ItemRegistry.BEETLE_JELLY);
    if (!bl) return false;

    for (int i = 1; i <= 3; i++) {
      if (!this.isEnrichmentMaterial(getStack(i))) return false;

      for (int j = 1; j <= 3; j++) {
        if (j == i) continue;
        if (getStack(j).isOf(getStack(i).getItem())) return false;
      }
    }

    return true;
  }

  public void emitParticle(ParticleEffect type) {
    double d = world.getRandom().nextGaussian() * 0.02;
    double e = world.getRandom().nextGaussian() * 0.02;
    double f = world.getRandom().nextGaussian() * 0.02;
    world.addParticle(
        type,
        0.5 + this.getPos().getX() + ((2.0 * world.getRandom().nextDouble() - 1.0) / 1.5),
        0.5 + this.getPos().getY() + ((2.0 * world.getRandom().nextDouble() - 1.0) / 1.5),
        0.5 + this.getPos().getZ() + ((2.0 * world.getRandom().nextDouble() - 1.0) / 1.5),
        d,
        e,
        f);
  }

  //////////////////////
  // NBT/PACKET UTILS //
  //////////////////////
  @Override
  public Packet<ClientPlayPacketListener> toUpdatePacket() {
    return BlockEntityUpdateS2CPacket.create(this);
  }

  @Override
  public NbtCompound toInitialChunkDataNbt() {
    return createNbt();
  }

  @Override
  public void writeNbt(NbtCompound nbt) {

    for (int i = 0; i < contained.length; i++) {
      if (!contained[i].isEmpty()) {
        nbt.putString("EntityType" + (i + 1), contained[i].contained_id);
        nbt.putString("EntityName" + (i + 1), contained[i].custom_name);
        if (contained[i].entity_data != null)
          nbt.put("EntityTag" + (i + 1), contained[i].entity_data);
      }
    }

    nbt.putInt("ProdTime", production_time);

    if (larva != null) {
      nbt.putString("LarvaType", larva.type);
      for (BeetleStat beetle_stat : BeetleStat.values()) {
        nbt.putInt("Larva" + beetle_stat.getName(), larva.getStat(beetle_stat));
      }
    }

    nbt.putBoolean("Decor0", decor[0]);
    nbt.putBoolean("Decor1", decor[1]);
    nbt.putBoolean("Decor2", decor[2]);

    Inventories.writeNbt(nbt, this.inventory);
    super.writeNbt(nbt);
  }

  @Override
  public void readNbt(NbtCompound nbt) {
    super.readNbt(nbt);
    if (nbt.contains("EntityType1")) {
      contained[0] =
          new ContainedEntity(
              nbt.getString("EntityType1"),
              nbt.getCompound("EntityTag1"),
              nbt.getString("EntityName1"));
    } else {
      contained[0] = ContainedEntity.EMPTY;
    }
    if (nbt.contains("EntityType2")) {
      contained[1] =
          new ContainedEntity(
              nbt.getString("EntityType2"),
              nbt.getCompound("EntityTag2"),
              nbt.getString("EntityName2"));
    } else {
      contained[1] = ContainedEntity.EMPTY;
    }
    production_time = nbt.getInt("ProdTime");
    if (nbt.contains("LarvaType")) {
      larva = new Larva(nbt.getString("LarvaType"), 10, 10, 10, 10);
      for (BeetleStat beetle_stat : BeetleStat.values()) {
        larva.setStat(beetle_stat, nbt.getInt("Larva" + beetle_stat.getName()));
      }
    } else {
      larva = null;
    }

    decor[0] = nbt.getBoolean("Decor0");
    decor[1] = nbt.getBoolean("Decor1");
    decor[2] = nbt.getBoolean("Decor2");

    inventory = DefaultedList.ofSize(size(), ItemStack.EMPTY);
    Inventories.readNbt(nbt, this.inventory);
  }

  ////////////////////////////
  // CONTAINED ENTITY UTILS //
  ////////////////////////////
  public void setLarva(Larva larva) {
    this.larva = larva;
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public boolean canPushContained() {
    if (this.isEntityContainerFull()) { // full entity slots
      return false;
    } else if (this.getStack(0) == ItemStack.EMPTY) { // no substrate
      return false;
    } else if (!getContained(0).isEmpty()
        && this.inventory.get(3) != ItemStack.EMPTY) { // third item slot used
      return false;
    }
    return true;
  }

  public boolean isEntityContainerEmpty() {
    return contained[0] == ContainedEntity.EMPTY && contained[1] == ContainedEntity.EMPTY;
  }

  public boolean isEntityContainerFull() {
    return contained[0] != ContainedEntity.EMPTY && contained[1] != ContainedEntity.EMPTY;
  }

  public int getEntityContainerSize() {
    if (this.inventory.get(3) != ItemStack.EMPTY) {
      return 1;
    } else {
      return 2;
    }
  }

  public boolean pushContained(ContainedEntity entity) {
    if (contained[0] == ContainedEntity.EMPTY) {
      contained[0] = entity;
    } else if (contained[1] == ContainedEntity.EMPTY) {
      contained[1] = entity;
    } else {
      return false;
    }
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
    return true;
  }

  public ContainedEntity getContained(int slot) {
    return contained[slot];
  }

  public ContainedEntity removeContained(int slot) {
    ContainedEntity e = contained[slot];
    contained[slot] = ContainedEntity.EMPTY;
    return e;
  }

  public void setContained(int slot, ContainedEntity e) {
    contained[slot] = e;
    markDirty();
  }

  public boolean isValid(int slot, ContainedEntity entity) {
    return true;
  }

  public ContainedEntity popContained() {
    ContainedEntity r;
    if (contained[1] != ContainedEntity.EMPTY) {
      r = contained[1];
      contained[1] = ContainedEntity.EMPTY;
      markDirty();
      this.getWorld()
          .updateListeners(
              this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
      return r;
    }
    if (contained[0] != ContainedEntity.EMPTY) {
      r = contained[0];
      contained[0] = ContainedEntity.EMPTY;
      markDirty();
      this.getWorld()
          .updateListeners(
              this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
      return r;
    }
    return ContainedEntity.EMPTY;
  }

  public boolean canPopContained() {
    return !this.isEntityContainerEmpty();
  }

  ////////////////////////////
  // CUSTOM INVENTORY UTILS //
  ////////////////////////////
  public void putTopStack(ItemStack stack) {
    setStack(Math.min(getTopStackId() + 1, 3), stack);
  }

  public ItemStack getTopStack() {
    for (int i = 3; i >= 0; i--) if (getStack(i) != ItemStack.EMPTY) return getStack(i);
    return ItemStack.EMPTY;
  }

  public int getTopStackId() {
    for (int i = 3; i >= 0; i--) if (getStack(i) != ItemStack.EMPTY) return i;
    return -1;
  }

  public boolean isEnrichmentMaterial(ItemStack stack) {
    return stack.isIn(ItemTags.LOGS)
        || stack.isIn(ItemTags.SAPLINGS)
        || stack.isIn(ItemTags.FLOWERS)
        || stack.isOf(Items.RED_MUSHROOM)
        || stack.isOf(Items.BROWN_MUSHROOM)
        || stack.isIn(ItemTags.SAPLINGS);
  }

  public static int getDecorId(ItemStack stack) {
    if (stack.isOf(Items.CHAIN)) return 0;
    if (stack.isOf(Items.VINE)) return 1;
    if (stack.isIn(ItemTags.LEAVES)) return 2;
    return -1;
  }

  public void setDecor(int id, boolean value) {
    decor[id] = value;
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public void flipDecor(int id) {
    decor[id] = !decor[id];
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  public boolean hasDecor() {
    for (int i = 0; i < decor.length; i++) if (decor[i]) return true;
    return false;
  }

  public void clearDecor() {
    for (int i = 0; i < decor.length; i++) decor[i] = false;
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  /////////////////////////
  // INVENTORY OVERRIDES //
  /////////////////////////
  @Override
  public ItemStack getStack(int slot) {
    return this.inventory.get(slot);
  }

  @Override
  public ItemStack removeStack(int slot, int amount) {
    ItemStack stack = Inventories.splitStack(this.inventory, slot, amount);
    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
    return stack;
  }

  @Override
  public ItemStack removeStack(int slot) {
    ItemStack stack = Inventories.removeStack(this.inventory, slot);

    markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);

    return stack;
  }

  @Override
  public void setStack(int slot, ItemStack stack) {
    this.inventory.set(slot, stack);

    if (stack.getCount() > this.getMaxCountPerStack()) stack.setCount(this.getMaxCountPerStack());

    this.markDirty();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  @Override
  public int size() {
    return this.inventory.size();
  }

  @Override
  public boolean isEmpty() {
    for (ItemStack itemStack : this.inventory) {
      if (itemStack.isEmpty()) continue;
      return false;
    }
    return true;
  }

  @Override
  public boolean canPlayerUse(PlayerEntity player) {
    return Inventory.canPlayerUse(this, player);
  }

  @Override
  public void clear() {
    this.inventory.clear();
    this.getWorld()
        .updateListeners(
            this.getPos(), this.getCachedState(), this.getCachedState(), Block.NOTIFY_LISTENERS);
  }

  @Override
  public int[] getAvailableSlots(Direction side) {
    if (side == Direction.UP) return TOP_SLOTS;
    return TOP_SLOTS;
  }

  @Override
  public boolean isValid(int slot, ItemStack stack) {
    if (slot >= this.size()) return false;

    if (stack.isOf(ItemRegistry.SUBSTRATE) && slot == 0 && getStack(4) == ItemStack.EMPTY)
      return true;

    if (getStack(0) == ItemStack.EMPTY) return false;

    if (stack.isOf(ItemRegistry.BEETLE_JELLY) && slot == 4 && getStack(4) == ItemStack.EMPTY)
      return true;

    for (int i = 1; i <= 3; i++)
      if (slot == i
          && getStack(i) == ItemStack.EMPTY
          && (stack.isIn(ItemTags.CANDLES) || this.isEnrichmentMaterial(stack))) return true;

    return false;
  }

  @Override
  public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
    return this.isValid(slot, stack);
  }

  @Override
  public boolean canExtract(int slot, ItemStack stack, Direction dir) {
    return true;
  }
}
