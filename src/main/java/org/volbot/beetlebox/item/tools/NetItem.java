package org.volbot.beetlebox.item.tools;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;

public class NetItem extends Item {

    public NetItem(Settings settings) {
        super(settings.maxCount(1));
    }

    public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {

        ItemStack jar_stack = JarUtils.takeValidJar(user, entity);

        if (!jar_stack.isEmpty()) {

            JarUtils.writeJarNbt(entity, jar_stack);

            if (user.getInventory().getEmptySlot() == -1) {
                user.dropStack(jar_stack);
            } else {
                user.giveItemStack(jar_stack);
            }

            World world=user.getWorld();
            if (!world.isClient) {
                entity.discard();
            }
            return ActionResult.SUCCESS;
        }
        return ActionResult.FAIL;
    }
}
