package org.volbot.beetlebox.item.tools;

import java.util.Optional;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import org.volbot.beetlebox.item.equipment.BeetlepackUtils;
import org.volbot.beetlebox.item.equipment.BeetlepackInventory;
import org.volbot.beetlebox.registry.ItemRegistry;
import net.minecraft.util.TypedActionResult;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.Entity;
import net.minecraft.stat.Stats;
import net.minecraft.item.RangedWeaponItem;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.World;
import net.minecraft.util.UseAction;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;

public class BugslingerItem extends RangedWeaponItem {

    public static final Predicate<ItemStack> BUGSLINGER_PROJECTILES = stack ->  stack.isOf(ItemRegistry.BEETLE_JAR) && stack.getOrCreateNbt().contains("EntityType");


    public BugslingerItem(Settings settings) {
        super(settings.maxCount(1));
    }

    @Override
    public void onStoppedUsing(ItemStack stack, World world, LivingEntity entity, int remainingUseTicks) {
        if(world.isClient || !(entity instanceof PlayerEntity) || !stack.hasNbt()) {
            return;
        }
        NbtCompound nbt = stack.getOrCreateNbt();
        if(!nbt.getBoolean("Loaded")) {
            if(nbt.contains("EntityType")) {
                nbt.putBoolean("Loaded",true);
            }
            return;
        }
        if(!nbt.contains("EntityType")) {
            nbt.putBoolean("Loaded",false);
            return;
        }
        PlayerEntity user = (PlayerEntity)entity;

        var opt = JarUtils.getEntityFromJar(stack, world);
        LivingEntity contained = (LivingEntity)opt.get();
        BeetleProjectileEntity proj = new BeetleProjectileEntity(world, user, contained);
        float speed = (float)(1.5*contained.getAttributes().getValue(EntityAttributes.GENERIC_MOVEMENT_SPEED));
        proj.setVelocity(
                user, 
                user.getPitch(), 
                user.getYaw(), 
                0.0F, 
                speed, 
                1.0F
                );

        proj.pickupType = PersistentProjectileEntity.PickupPermission.ALLOWED;
        
        world.spawnEntity(proj);

        JarUtils.clearJarNbt(stack);
        nbt.putBoolean("Loaded",false);
        nbt.putInt("BPIndex",0);

        user.incrementStat(Stats.USED.getOrCreateStat(this));
        return;
    }
	
    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(user);
        if(stack.getOrCreateNbt().contains("EntityType")) {
            user.setCurrentHand(hand);
            return TypedActionResult.consume(stack);
        }
        if (!bp.isEmpty()) {
            BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
            for (int i = 0; i < bp_inv.size(); i++) {
                ItemStack jar = bp_inv.getStack(i);
                if(BUGSLINGER_PROJECTILES.test(jar)){
                        JarUtils.cloneJarNbt(jar,stack);
                        JarUtils.clearJarNbt(jar);
                        user.setCurrentHand(hand);
                        stack.getOrCreateNbt().putBoolean("Loaded",true);
                        stack.getOrCreateNbt().putInt("BPIndex",i);
                        return TypedActionResult.consume(stack);
                }
            }
        }
        PlayerInventory player_inv = user.getInventory();
        for (int i = 0; i < user.getInventory().size(); i++) {
            ItemStack jar = player_inv.getStack(i);
            if(BUGSLINGER_PROJECTILES.test(jar)){
                JarUtils.cloneJarNbt(jar,stack);
                JarUtils.clearJarNbt(jar);
                user.setCurrentHand(hand);
                stack.getOrCreateNbt().putBoolean("Loaded",false);
                stack.getOrCreateNbt().putInt("BPIndex",-1);
                return TypedActionResult.success(stack);
            }
        }
        return TypedActionResult.pass(stack);
    }

    public static Optional<Entity> getContained(ItemStack stack, World world) {
        if (stack.isOf(ItemRegistry.BUGSLINGER)) {
            NbtCompound nbt = stack.getOrCreateNbt();
            if(nbt.contains("EntityType")) {
                return JarUtils.getEntityFromJar(stack, world);
            }
        }
        return Optional.empty();
    }

    public static void setContained(ItemStack stack, LivingEntity entity) {
        JarUtils.writeJarNbt(entity, stack);
    }

    @Override
    public Predicate<ItemStack> getProjectiles() {
        return BUGSLINGER_PROJECTILES;
    }

    @Override
    public int getRange() {
        return 15;
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.BOW;
    }

    @Override
    public int getMaxUseTime(ItemStack stack) {
        return 7200;
    }

    public static float getModelState(ItemStack stack, @Nullable PlayerEntity user) {
        if (user==null || !stack.hasNbt())
            return 0.0f;

        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(user);
        if(stack.getOrCreateNbt().contains("EntityType")) {
            if(stack.getOrCreateNbt().getBoolean("Loaded")) 
                return 0.3F; 
            return 0.2F;
        }
        if (!bp.isEmpty()) {
            BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
            for (int i = 0; i < bp_inv.size(); i++) {
                ItemStack jar = bp_inv.getStack(i);
                if (jar.getItem() instanceof BeetleJarItem) {
                    if(jar.hasNbt() && jar.getNbt().contains("EntityType")) {
                        return 0.3F;
                    }   
                }
            }
        }
        PlayerInventory player_inv = user.getInventory();
        for (int i = 0; i < user.getInventory().size(); i++) {
            ItemStack jar = player_inv.getStack(i);
            if (jar.getItem() instanceof BeetleJarItem) {
                if(jar.hasNbt()&&jar.getNbt().contains("EntityType")) {
                    return 0.2F;
                }
            }
        }
        return 0.1F;
    }

}
