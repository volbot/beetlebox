package org.volbot.beetlebox.item.tools;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.jetbrains.annotations.Nullable;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.storage.ContainedEntity;
import org.volbot.beetlebox.item.equipment.BeetlepackInventory;
import org.volbot.beetlebox.item.equipment.BeetlepackUtils;
import org.volbot.beetlebox.registry.ItemRegistry;

public class JarUtils {

  public static final Predicate<ItemStack> JAR_PREDICATE =
      stack -> stack.isOf(ItemRegistry.BEETLE_JAR) && !stack.hasNbt();
  public static final Predicate<ItemStack> LEG_JAR_PREDICATE =
      stack -> stack.isOf(ItemRegistry.LEG_BEETLE_JAR) && !stack.hasNbt();

  public static void clearJarNbt(ItemStack stack) {
    NbtCompound nbt = stack.getOrCreateNbt();
    nbt.remove("EntityType");
    nbt.remove("EntityTag");
    nbt.remove("EntityName");
    if (nbt.isEmpty()) stack.setNbt(null);
  }

  public static void cloneJarNbt(ItemStack source, ItemStack target) {

    if (!source.hasNbt()) {
      target.setNbt(null);
      return;
    }
    NbtCompound nbt1 = source.getNbt();
    NbtCompound nbt2 = target.getOrCreateNbt().copy();

    String entity_type = nbt1.getString("EntityType");
    nbt2.putString("EntityType", entity_type);

    NbtCompound entity_tag = nbt1.getCompound("EntityTag");
    nbt2.put("EntityTag", entity_tag);

    if (nbt1.contains("EntityName")) {
      String entity_name = nbt1.getString("EntityName");
      nbt2.putString("EntityName", entity_name);
    }

    target.setNbt(nbt2);
  }

  public static void writeJarNbt(LivingEntity entity, ItemStack stack) {
    entity.setOnGround(true);
    if (entity instanceof BeetleEntity) {
      ((BeetleEntity) entity).setFlying(false);
    }

    NbtCompound nbt = stack.getOrCreateNbt().copy();

    nbt.putString("EntityType", EntityType.getId(entity.getType()).toString());

    NbtCompound tag = new NbtCompound();
    entity.writeNbt(tag);
    entity.writeCustomDataToNbt(tag);
    nbt.put("EntityTag", tag);

    Text custom_name = entity.getCustomName();
    if (custom_name != null && !custom_name.getString().isEmpty()) {
      nbt.putString("EntityName", custom_name.getString());
    }

    stack.setNbt(nbt);
  }

  public static void writeJarNbt(LivingEntity entity, NbtCompound nbt) {
    nbt.putString("EntityType", EntityType.getId(entity.getType()).toString());

    NbtCompound tag = new NbtCompound();
    entity.writeNbt(tag);
    entity.writeCustomDataToNbt(tag);
    nbt.put("EntityTag", tag);

    Text custom_name = entity.getCustomName();
    if (custom_name != null && !custom_name.getString().isEmpty()) {
      nbt.putString("EntityName", custom_name.getString());
    }
  }

  public static Optional<Entity> readJarNbt(NbtCompound nbt, World world) {
    if (nbt.contains("EntityType")) {
      EntityType<? extends Entity> entityType =
          EntityType.get(nbt.getString("EntityType")).orElse(null);
      if (entityType != null) {
        Entity entity = entityType.create(world);

        if (entity != null) {
          entity.readNbt(nbt.getCompound("EntityTag"));
          if (entity instanceof LivingEntity) {
            ((LivingEntity) entity).readCustomDataFromNbt(nbt.getCompound("EntityTag"));
          }

          if (nbt.contains("EntityName")) {
            entity.setCustomName(Text.of(nbt.getString("EntityName")));
          }

          return Optional.of(entity);
        }
      }
    }
    return Optional.empty();
  }

  public static Optional<Entity> getEntityFromJar(ItemStack stack, World world) {
    if (!stack.hasNbt()) return Optional.empty();

    NbtCompound nbt = stack.getNbt();
    return readJarNbt(nbt, world);
  }

  public static Optional<Entity> getEntityFromContained(ContainedEntity ce, World world) {
    EntityType<? extends Entity> entityType = EntityType.get(ce.getContainedId()).orElse(null);
    if (entityType != null) {
      Entity entity = entityType.create(world);
      if (entity != null) {

        entity.readNbt(ce.getEntityData());
        if (entity instanceof LivingEntity)
          ((LivingEntity) entity).readCustomDataFromNbt(ce.getEntityData());

        if (!ce.getCustomName().isEmpty()) entity.setCustomName(Text.of(ce.getCustomName()));

        return Optional.of(entity);
      }
    }
    return Optional.empty();
  }

  // PASSES BY VALUE, NOT BY REFERENCE!!
  // use takeValidJar() for single decrement and returns
  public static ItemStack getValidJar(PlayerEntity player, LivingEntity entity) {
    Predicate<ItemStack> predicate;
    ItemStack fallback;
    if (entity instanceof BeetleEntity) {
      predicate = JAR_PREDICATE;
      fallback = new ItemStack(ItemRegistry.BEETLE_JAR);
    } else {
      predicate = LEG_JAR_PREDICATE;
      fallback = new ItemStack(ItemRegistry.LEG_BEETLE_JAR);
    }

    ItemStack bp_stack = BeetlepackUtils.getBeetlepackOnPlayer(player);
    if (!bp_stack.isEmpty()) {
      BeetlepackInventory bp_inv = new BeetlepackInventory(bp_stack);
      for (int i = 0; i < bp_inv.size(); ++i) {
        ItemStack itemStack2 = bp_inv.getStack(i);
        if (predicate.test(itemStack2)) {
          return itemStack2;
        }
      }
    }

    var inv = player.getInventory();
    for (int i = 0; i < inv.size(); ++i) {
      ItemStack itemStack2 = inv.getStack(i);
      if (predicate.test(itemStack2)) {
        return itemStack2;
      }
    }

    return player.isCreative() ? fallback : ItemStack.EMPTY;
  }

  public static ItemStack takeValidJar(PlayerEntity player, LivingEntity entity) {
    Predicate<ItemStack> predicate;
    ItemStack fallback;
    if (entity instanceof BeetleEntity) {
      predicate = JAR_PREDICATE;
      fallback = new ItemStack(ItemRegistry.BEETLE_JAR);
    } else {
      predicate = LEG_JAR_PREDICATE;
      fallback = new ItemStack(ItemRegistry.LEG_BEETLE_JAR);
    }

    ItemStack bp_stack = BeetlepackUtils.getBeetlepackOnPlayer(player);
    if (!bp_stack.isEmpty()) {
      BeetlepackInventory bp_inv = new BeetlepackInventory(bp_stack);
      for (int i = 0; i < bp_inv.size(); ++i) {
        ItemStack itemStack2 = bp_inv.getStack(i);
        if (predicate.test(itemStack2)) {
          ItemStack val = itemStack2.copyWithCount(1);
          itemStack2.decrement(1);
          bp_inv.markDirty();
          return val;
        }
      }
    }

    var inv = player.getInventory();
    for (int i = 0; i < inv.size(); ++i) {
      ItemStack itemStack2 = inv.getStack(i);
      if (predicate.test(itemStack2)) {
        ItemStack val = itemStack2.copyWithCount(1);
        itemStack2.decrement(1);
        return val;
      }
    }

    return player.isCreative() ? fallback : ItemStack.EMPTY;
  }

  public static Optional<Entity> trySpawnFromJar(ItemUsageContext context) {
    return trySpawnFromJar(
        (ServerWorld) context.getWorld(),
        context.getStack(),
        context.getBlockPos(),
        context.getPlayer(),
        context.getSide());
  }

  public static Optional<Entity> trySpawnFromJar(
      ServerWorld world,
      ItemStack stack,
      BlockPos pos,
      @Nullable PlayerEntity user,
      @Nullable Direction dir) {

    BlockPos pos2 = pos;
    boolean invertY = false;

    if (dir != null) {
      pos2 =
          world.getBlockState(pos).getCollisionShape(world, pos).isEmpty() ? pos : pos.offset(dir);

      invertY = !Objects.equals(pos, pos2) && dir == Direction.UP;
    }

    return trySpawnFromJar(world, stack, pos2, user, invertY);
  }

  public static Optional<Entity> trySpawnFromJar(
      ServerWorld world,
      ItemStack stack,
      BlockPos pos,
      @Nullable PlayerEntity user,
      boolean invertY) {

    if (!stack.hasNbt()) {
      return Optional.empty();
    }
    NbtCompound nbt = stack.getNbt();

    if (nbt.contains("EntityType")) {
      EntityType<? extends Entity> entityType =
          EntityType.get(nbt.getString("EntityType")).orElse(null);
      if (entityType != null) {
        var entity =
            entityType.create(
                world,
                null,
                user == null ? null : EntityType.copier(world, stack, user),
                pos,
                SpawnReason.SPAWN_EGG,
                true,
                invertY);

        if (entity != null) {
          entity.readNbt(nbt.getCompound("EntityTag"));
          if (entity instanceof LivingEntity) {
            ((LivingEntity) entity).readCustomDataFromNbt(nbt.getCompound("EntityTag"));
          }

          if (nbt.contains("EntityName")) {
            entity.setCustomName(Text.of(nbt.getString("EntityName")));
          }

          entity.refreshPositionAndAngles(pos, 0, 0);
          if (!world.isClient) {

            world.spawnEntityAndPassengers(entity);
          }

          JarUtils.clearJarNbt(stack);

          world.emitGameEvent(entity, GameEvent.ENTITY_PLACE, pos);
          return Optional.of(entity);
        }
      }
    }
    return Optional.empty();
  }

  public static ContainedEntity getContained(ItemStack stack) {
    NbtCompound nbt = stack.getNbt();
    if (!nbt.contains("EntityType")) {
      return null;
    }
    String contained_id = nbt.getString("EntityType");
    NbtCompound entity_data = nbt.getCompound("EntityTag");
    String custom_name = "";
    if (nbt.contains("EntityName")) {
      custom_name = nbt.getString("EntityName");
    }
    return new ContainedEntity(contained_id, entity_data, custom_name);
  }
}
