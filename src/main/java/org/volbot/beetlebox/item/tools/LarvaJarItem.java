package org.volbot.beetlebox.item.tools;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import net.minecraft.block.FluidBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.World;
import org.volbot.beetlebox.registry.ItemRegistry;
import org.volbot.beetlebox.config.BBConfigRegistry;

public class LarvaJarItem extends Item {

    public LarvaJarItem(Settings settings) {
        super(settings);
    }

    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        if (stack.getOrCreateNbt().contains("EntityType") && entity instanceof LivingEntity) {
            incrementJarTime(stack);
        }
    }

    public static void incrementJarTime(ItemStack stack) {
        incrementJarTime(stack, BBConfigRegistry.LARVA_MULTIPLIER_DEFAULT);
    }

    public static void incrementJarTime(ItemStack stack, int amount) {
        NbtCompound nbt = stack.getOrCreateNbt();
        if (!nbt.contains("GrowingTime")) {
            nbt.putInt("GrowingTime", 0);
        }
        int growing_time = nbt.getInt("GrowingTime");
        if (growing_time >= BBConfigRegistry.LARVA_BASE_TICKS) {
            return;
        }
        growing_time += amount;
        nbt.putInt("GrowingTime", growing_time);
        stack.setNbt(nbt);
    }

    public static void awardHusks(PlayerEntity user) {

        ItemStack husk_stack = ItemRegistry.BEETLE_HUSK.getDefaultStack();
        husk_stack.setCount(user.getRandom().nextBetween(BBConfigRegistry.LARVA_HUSKS_MIN,BBConfigRegistry.LARVA_HUSKS_MAX));
        if(user.getInventory().getEmptySlot() == -1) {
            user.dropStack(husk_stack);
        } else {
            user.giveItemStack(husk_stack);
        }
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        if (!(world instanceof ServerWorld)) {
            return ActionResult.SUCCESS;
        } else {
            ItemStack stack = context.getStack();
            NbtCompound nbt = stack.getOrCreateNbt(); 
            if (nbt.getInt("GrowingTime") >= BBConfigRegistry.LARVA_BASE_TICKS) {
                if (JarUtils.trySpawnFromJar(context).isPresent()) {
                    awardHusks(context.getPlayer());
                    JarUtils.clearJarNbt(stack);
                    nbt.putInt("GrowingTime",0);
                    stack.setNbt(nbt);
                    return ActionResult.CONSUME;
                }
            }
            return ActionResult.PASS;
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        HitResult hitResult = raycast(world, user, RaycastContext.FluidHandling.SOURCE_ONLY);
        if (hitResult.getType() != HitResult.Type.BLOCK) {
            return TypedActionResult.pass(stack);
        } else if (!(world instanceof ServerWorld)) {
            return TypedActionResult.success(stack);
        } else {
            BlockHitResult blockHitResult = (BlockHitResult)hitResult;
            BlockPos pos = blockHitResult.getBlockPos();
            if (!(world.getBlockState(pos).getBlock() instanceof FluidBlock)) {
                return TypedActionResult.pass(stack);
            } else if (world.canPlayerModifyAt(user, pos) && user.canPlaceOn(pos, blockHitResult.getSide(), stack)) {
                NbtCompound nbt = stack.getOrCreateNbt();
                if (nbt.getInt("GrowingTime") >= BBConfigRegistry.LARVA_BASE_TICKS) {
                    if(JarUtils.trySpawnFromJar((ServerWorld)world, stack, pos, user, false).isPresent()) {
                        awardHusks(user);
                        JarUtils.clearJarNbt(stack);
                        nbt.putInt("GrowingTime",0);
                        stack.setNbt(nbt);
                        return TypedActionResult.consume(stack);
                    }
                }
            }
            return TypedActionResult.pass(stack);
        }
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        NbtCompound nbt = stack.getNbt();
        if (nbt != null) {
            EntityType<?> e = EntityType.get(nbt.getString("EntityType")).orElse(null);
            if (e != null) {
                tooltip.add(Text.literal("Contained: ").append(e.getName()).formatted(Formatting.GRAY));
                int growing_time = nbt.getInt("GrowingTime");
                if (growing_time < BBConfigRegistry.LARVA_BASE_TICKS) {
                    tooltip.add(Text.literal("Growing... (").formatted(Formatting.GRAY)
                            .append(Text.literal(Math.round(100 * growing_time / BBConfigRegistry.LARVA_BASE_TICKS) + "%").formatted(Formatting.WHITE))
                            .append(Text.literal(")").formatted(Formatting.GRAY)));
                } else {
                    tooltip.add(Text.literal("Ready to emerge").formatted(Formatting.GRAY));
                }
                return;
            }
        }
        tooltip.add(Text.literal("Contained: None").formatted(Formatting.GRAY));
    }
}
