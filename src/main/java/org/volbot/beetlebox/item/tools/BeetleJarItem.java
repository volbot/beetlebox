package org.volbot.beetlebox.item.tools;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import net.minecraft.block.FluidBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.World;
import org.volbot.beetlebox.entity.storage.ContainedEntity;

public class BeetleJarItem<T extends LivingEntity> extends Item {

    Class<T> clazz;

    public BeetleJarItem(Settings settings, Class<T> clazz) {
        super(settings);
        this.clazz = clazz;
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        if (!(world instanceof ServerWorld)) {
            return ActionResult.SUCCESS;
        } else {
            if(JarUtils.trySpawnFromJar(context).isPresent()) {
                return ActionResult.CONSUME;
            }
            return ActionResult.PASS;
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        HitResult hitResult = raycast(world, user, RaycastContext.FluidHandling.SOURCE_ONLY);
        if (hitResult.getType() != HitResult.Type.BLOCK) {
            return TypedActionResult.pass(stack);
        } else if (!(world instanceof ServerWorld)) {
            return TypedActionResult.success(stack);
        } else {
            BlockHitResult blockHitResult = (BlockHitResult)hitResult;
            BlockPos pos = blockHitResult.getBlockPos();
            if (!(world.getBlockState(pos).getBlock() instanceof FluidBlock)) {
                return TypedActionResult.pass(stack);
            } else if (world.canPlayerModifyAt(user, pos) && user.canPlaceOn(pos, blockHitResult.getSide(), stack)) {
                if (JarUtils.trySpawnFromJar((ServerWorld)world, stack, pos, user, false).isPresent()) {
                        return TypedActionResult.consume(stack);
                }
                return TypedActionResult.pass(stack);
            } else {
                return TypedActionResult.fail(stack);
            }
        }
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        NbtCompound nbt = stack.getNbt();
        if (nbt != null) {
            EntityType<?> e = EntityType.get(nbt.getString("EntityType")).orElse(null);
            if (e != null) {
                tooltip.add(Text.literal("Contained: ").append(e.getName()).formatted(Formatting.GRAY));
                return;
            }
        }
        tooltip.add(Text.literal("Contained: None").formatted(Formatting.GRAY));
    }

    @Override
    public ItemStack getDefaultStack() {
        return new ItemStack(this);
    }

    public boolean canStore(Entity entity) {
        return clazz.isAssignableFrom(entity.getClass());
    }

	public static ContainedEntity getContained(ItemStack stack) {
		NbtCompound nbt = stack.getNbt();
		if (!nbt.contains("EntityType")) {
			return ContainedEntity.EMPTY;
		}
		String contained_id = nbt.getString("EntityType");
		NbtCompound entity_data = nbt.getCompound("EntityTag");
        if(entity_data==null) { entity_data = new NbtCompound(); }
		String custom_name = "";
		if (nbt.contains("EntityName")) {
			custom_name = nbt.getString("EntityName");
		}
		return new ContainedEntity(contained_id, entity_data, custom_name);
	}
}
