package org.volbot.beetlebox.item;

import java.util.List;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

public class BeetleSpawnEggItem extends SpawnEggItem {

  BeetleType beetle_type;

  public BeetleSpawnEggItem(BeetleType beetle_type, Item.Settings settings) {
    super(beetle_type.entityType(), beetle_type.color1(), beetle_type.color2(), settings);
    this.beetle_type = beetle_type;
  }

  @Override
  public void appendTooltip(
      ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
    BeetleProductUtils.appendBeetleNameTooltip(stack, world, tooltip, context, beetle_type);
  }
}
