package org.volbot.beetlebox.item;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.World;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.entity.storage.ContainedEntity;

public class Larva {

  public String type;

  protected Map<BeetleStat, Integer> stats = new HashMap<>();

  public Larva(String type, int size, int damage, int speed, int maxhealth) {
    this.type = type;
    this.stats.put(BeetleStat.BB_SIZE, size);
    this.stats.put(BeetleStat.BB_DAMAGE, damage);
    this.stats.put(BeetleStat.BB_SPEED, speed);
    this.stats.put(BeetleStat.BB_HEALTH, maxhealth);
  }

  public Larva(LivingEntity a, LivingEntity b) {
    this.type = EntityType.getId(a.getType()).toString();
    if (a instanceof BeetleEntity) {
      generateGeneticStats((BeetleEntity) a, (BeetleEntity) b);
    }
  }

  public Larva(ContainedEntity a_contained, ContainedEntity b_contained, World world) {
    EntityType<?> a_type = EntityType.get(a_contained.getContainedId()).orElse(null);
    LivingEntity a = (LivingEntity) a_type.create(world);
    a.readNbt(a_contained.getEntityData());
    a.readCustomDataFromNbt(a_contained.getEntityData());

    this.type = EntityType.getId(a.getType()).toString();

    if (a instanceof BeetleEntity) {
      EntityType<?> b_type = EntityType.get(b_contained.getContainedId()).orElse(null);
      LivingEntity b = (LivingEntity) b_type.create(world);
      b.readNbt(b_contained.getEntityData());
      b.readCustomDataFromNbt(b_contained.getEntityData());

      generateGeneticStats((BeetleEntity) a, (BeetleEntity) b);
    }
  }

  protected void generateGeneticStats(BeetleEntity a, BeetleEntity b) {
    for (BeetleStat beetle_stat : BeetleStat.values()) {
      // randomly alternate between parents for each stat to be inherited
      this.setStat(beetle_stat, (a.getRandom().nextBoolean() ? a : b).getStat(beetle_stat));
    }
  }

  public Map<BeetleStat, Integer> getStats() {
    return this.stats;
  }

  public int getStat(BeetleStat beetle_stat) {
    return this.stats.get(beetle_stat);
  }

  public void setStat(BeetleStat beetle_stat, int new_value) {
    this.stats.replace(beetle_stat, new_value);
  }
}
