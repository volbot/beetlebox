package org.volbot.beetlebox.item;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.minecraft.text.Style;
import net.minecraft.world.World;

public class BeetleJelly extends Item {

	public BeetleJelly(Settings settings) {
		super(settings);
	}
	
	@Override
	public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
		NbtCompound nbt = stack.getNbt();
		if (nbt == null) {
			return;
		}
		if(!nbt.contains("FruitType")) {
			return;
        }
        String fruit = nbt.getString("FruitType");
        String fruitstring = " (Unknown Effect)";
        if(nbt.contains("Level") && nbt.contains("Increase")) {
            switch(fruit) {
                case "apple":
                    fruitstring = " (Max Health)";
                    break;
                case "melon":
                    fruitstring = " (Size)";
                    break;
                case "sugar":
                    fruitstring = " (Speed)";
                    break;
                case "cactus":
                    fruitstring = " (Damage)";
                    break;
                case "":
                    return;
            }	
            
            tooltip.addAll(Text.of(fruit.substring(0, 1).toUpperCase() + fruit.substring(1) + fruitstring).getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
            int level = nbt.getInt("Level");
            boolean increase = nbt.getBoolean("Increase");
            String incstring = increase?"Increase":"Decrease";
            tooltip.addAll(Text.of("Level "+level+" "+incstring).getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
        } else {
            tooltip.addAll(Text.of(fruit.substring(0, 1).toUpperCase() + fruit.substring(1) + " (Class)").getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
            switch(fruit) {
                case "honey":
                    tooltip.addAll(Text.of("Infantry").getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
                    break;
                case "berry":
                    tooltip.addAll(Text.of("Projectile").getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
                    break;
            }
        }
    }
}
