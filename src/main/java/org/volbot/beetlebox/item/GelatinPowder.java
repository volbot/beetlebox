package org.volbot.beetlebox.item;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

public class GelatinPowder extends Item {

	public GelatinPowder(Settings settings) {
		super(settings);
	}

	@Override
	public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
		tooltip.addAll(Text.of("Obtained by boiling proteins").getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
		tooltip.addAll(Text.of("Beef, Pork, Bones, Leather, or Kelp").getWithStyle(Style.EMPTY.withItalic(true).withColor(TextColor.fromFormatting(Formatting.DARK_GRAY))));
	}
}
