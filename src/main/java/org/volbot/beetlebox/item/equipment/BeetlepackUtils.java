package org.volbot.beetlebox.item.equipment;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.World;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleClass;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.JarUtils;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BeetlepackUtils {

  public static void deployBeetles(PlayerEntity user, BeetleDeployReason reason) {
    World world = user.getWorld();
    if (world.isClient) {
      return;
    }
    ItemStack bp = getBeetlepackOnPlayer(user);
    if (bp.isOf(ItemRegistry.BEETLEPACK)) {
      NbtCompound bp_nbt = bp.getOrCreateNbt();
      if (!bp_nbt.contains(reason.toString() + "Spawn")) {
        BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
        ArrayList<UUID> spawned_uuids = new ArrayList<UUID>();
        for (int i = 0; i < bp_inv.size(); i++) {
          ItemStack jar = bp_inv.getStack(i);
          if (jar.getItem() instanceof BeetleJarItem) {
            switch (reason) {
              case COMBAT:
                if (!bp_nbt.contains("ToggleAttack")) {
                  bp_nbt.putBoolean(
                      "ToggleAttack", BBConfigRegistry.BEETLEPACK_TOGGLE_DEPLOY_ATTACK);
                }
                if (!bp_nbt.getBoolean("ToggleAttack")) {
                  return;
                }
                NbtCompound entity_data = jar.getOrCreateNbt().getCompound("EntityTag");
                switch (BeetleClass.values()[entity_data.getInt("Class")]) {
                  case INFANTRY:
                    Optional<Entity> opt =
                        JarUtils.trySpawnFromJar(
                            (ServerWorld) world, jar, user.getBlockPos(), user, null);
                    if (opt.isPresent()) {
                      Entity e = opt.get();
                      if (e instanceof TameableEntity && ((TameableEntity) e).isOwner(user)) {
                        spawned_uuids.add(e.getUuid());
                      }
                    }
                    break;
                  case PROJECTILE:
                    // fire beetle as projectile
                    BeetleProjectileEntity proj = new BeetleProjectileEntity(world, user, jar);
                    Entity target = user.getAttacker();
                    proj.setVelocity(
                        target
                            .getBoundingBox()
                            .getCenter()
                            .subtract(user.getEyePos())
                            .normalize()
                            .multiply(0.5f));
                    proj.pickupType = PersistentProjectileEntity.PickupPermission.ALLOWED;
                    if (world.spawnEntity(proj)) {
                      jar.removeSubNbt("EntityType");
                      jar.removeSubNbt("EntityTag");
                      jar.removeSubNbt("EntityName");
                    }
                    break;
                  default:
                    break;
                }
                break;
              default:
                if (!bp_nbt.contains("ToggleFlight")) {
                  bp_nbt.putBoolean(
                      "ToggleFlight", BBConfigRegistry.BEETLEPACK_TOGGLE_DEPLOY_FLIGHT);
                }
                if (!bp_nbt.getBoolean("ToggleFlight")) {
                  return;
                }
                Optional<Entity> opt =
                    JarUtils.trySpawnFromJar(
                        (ServerWorld) world, jar, user.getBlockPos(), user, null);
                if (opt.isPresent()) {
                  spawned_uuids.add(opt.get().getUuid());
                }
                break;
            }
          }
        }
        if (!spawned_uuids.isEmpty()) {
          NbtCompound uuid_nbt = new NbtCompound();
          int i = 0;
          for (UUID uuid : spawned_uuids) {
            uuid_nbt.putUuid(reason + "Spawn" + i, uuid);
            i++;
          }
          bp_nbt.put(reason + "Spawn", uuid_nbt);
        }
        bp_inv.markDirty();
      }
    }
  }

  public static void recallBeetles(PlayerEntity user) {
    ItemStack bp = getBeetlepackOnPlayer(user);
    if (bp.isOf(ItemRegistry.BEETLEPACK)) {
      NbtCompound bp_nbt = bp.getOrCreateNbt();

      World world = user.getWorld();
      for (BeetleDeployReason reason : BeetleDeployReason.values()) {
        if (bp_nbt.contains(reason.toString() + "Spawn")) {
          NbtCompound flight_spawn = bp_nbt.getCompound(reason + "Spawn");
          ArrayList<UUID> spawned_uuids = new ArrayList<UUID>();
          int i = 0;
          while (flight_spawn.contains(reason + "Spawn" + i)) {
            spawned_uuids.add(flight_spawn.getUuid(reason + "Spawn" + i));
            i++;
          }
          i = -1;
          for (UUID uuid : spawned_uuids) {
            Entity entity = user.getWorld().getEntityLookup().get(uuid);
            if (entity == null) {
              continue;
            }
            BeetleProjectileEntity proj =
                new BeetleProjectileEntity(world, user, (LivingEntity) entity);
            proj.setVelocity(
                user.getBoundingBox()
                    .getCenter()
                    .subtract(proj.getPos())
                    .normalize()
                    .multiply(0.5));
            proj.pickupType = PersistentProjectileEntity.PickupPermission.ALLOWED;
            proj.landed = true;
            world.spawnEntity(proj);
            entity.discard();
          }
        }
        bp_nbt.remove(reason + "Spawn");
      }
    }
  }

  public enum BeetleDeployReason {
    FLIGHT,
    COMBAT
  }

  public static ItemStack getBeetlepackOnPlayer(PlayerEntity player) {
    ItemStack beetlepack = ItemStack.EMPTY;
    PlayerInventory inv = player.getInventory();
    ItemStack chest_stack = inv.getArmorStack(EquipmentSlot.CHEST.getEntitySlotId());
    if (chest_stack.isOf(ItemRegistry.BEETLEPACK)) {
      beetlepack = chest_stack;
    } else if (!(beetlepack.isOf(ItemRegistry.BEETLEPACK))) {
      if (FabricLoader.getInstance().isModLoaded("trinkets")) {
        ItemStack back_stack =
            org.volbot.beetlebox.compat.trinkets.BeetlepackTrinket.getBackStack(player);
        if (back_stack.isOf(ItemRegistry.BEETLEPACK)) {
          beetlepack = back_stack;
        }
      }
    }
    return beetlepack;
  }
}
