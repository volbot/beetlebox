package org.volbot.beetlebox.item.equipment;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterials;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import org.volbot.beetlebox.client.render.gui.BeetlepackScreenHandler;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.item.tools.LarvaJarItem;

public class BeetlepackItem extends ArmorItem implements ExtendedScreenHandlerFactory {

    protected static ArmorItem.Type equipment_type = Type.CHESTPLATE;

    public BeetlepackItem(Settings settings) {
        super(ArmorMaterials.LEATHER, Type.CHESTPLATE, settings.maxDamage(-1));
    }

    public void inventoryTick(ItemStack bp, World world, Entity entity, int slot, boolean selected) {
        BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
        for (int i = 0; i < bp_inv.size(); i++) {
            ItemStack stack = bp_inv.getStack(i);
            if (!stack.isEmpty())
                if (stack.getItem() instanceof LarvaJarItem) {
                    LarvaJarItem.incrementJarTime(stack, BBConfigRegistry.LARVA_MULTIPLIER_BEETLEPACK);
                }
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        if (!user.isSneaking()) { 
            return super.use(world, user, hand);
        }
        ItemStack stack = user.getStackInHand(hand);
        stack.getOrCreateNbt().putBoolean("Open", true);
        user.openHandledScreen(this);
        return TypedActionResult.consume(stack);
    }

    @Override
    public Text getDisplayName() {
        return Text.of("Beetlepack");
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        return new BeetlepackScreenHandler(syncId, playerInventory);
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {

    }

}
