package org.volbot.beetlebox.item.equipment;

public class BeetleArmorTypes {

  public enum ArmorPattern {
    STANDARD,
    CUSTOM;
  }

  public enum HelmetShape {
    STANDARD,
    CLAW,
    FLIP,
    PINCERS,
    THREE;

    public String texSuffix() {
      switch (this) {
        case CLAW:
          return "_claw";
        case FLIP:
          return "_flip";
        case PINCERS:
          return "_pincers";
        case THREE:
          return "_three";
        default:
          return "";
      }
    }
  }
}
