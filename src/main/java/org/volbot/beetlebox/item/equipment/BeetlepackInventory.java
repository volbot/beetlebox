package org.volbot.beetlebox.item.equipment;

import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.text.Text;
import net.minecraft.util.Nameable;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;

public class BeetlepackInventory implements Inventory, Nameable {

    public static int INVENTORY_SIZE = 6;

    public ItemStack stack;
    private DefaultedList<ItemStack> inv = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);

    public BeetlepackInventory(ItemStack itemStack) {
        stack=itemStack;
        inv = DefaultedList.ofSize(6, ItemStack.EMPTY);
        Inventories.readNbt(stack.getOrCreateNbt().getCompound("Inventory"), inv);
    }
    
	@Override
	public void markDirty() {
        NbtCompound inv_nbt = new NbtCompound();
        Inventories.writeNbt(inv_nbt, inv);
        stack.getOrCreateNbt().put("Inventory", inv_nbt);
    }
	
    @Override
	public Text getName() {
		return Text.translatable("beetlebox.container.beetlepack");
	}

    @Override
	public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }
	
    @Override
	public void setStack(int slot, ItemStack itemStack) {
		if (slot < inv.size()) {
			inv.set(slot, itemStack);
		}
	}
	
    @Override
	public ItemStack removeStack(int slot) {
		if (slot < inv.size()) {
            ItemStack val = inv.get(slot).copyAndEmpty();
            return val;
        }
        return ItemStack.EMPTY;
	}

    @Override
	public ItemStack removeStack(int slot, int amount) {
		return !(inv.get(slot)).isEmpty() ? Inventories.splitStack(inv, slot, amount) : ItemStack.EMPTY;
	}

    @Override
    public ItemStack getStack(int slot) {
		return inv.get(slot);
    }

	@Override
	public boolean isEmpty() {
		for(ItemStack itemStack : inv) {
			if (!itemStack.isEmpty()) {
				return false;
			}
		}
        return true;
    }

    @Override
    public int size() {
        return inv.size();
    }

	@Override
	public void clear() {
		inv.clear();
	}

    @Override
    public void onClose(PlayerEntity player) {
        this.markDirty();

		this.stack.getOrCreateNbt().putBoolean("Open", false);
    }

	public NbtList writeNbt(NbtList nbtList) {
		for(int i = 0; i < this.inv.size(); ++i) {
			if (!this.inv.get(i).isEmpty()) {
				NbtCompound nbtCompound = new NbtCompound();
				nbtCompound.putByte("Slot", (byte)i);
				this.inv.get(i).writeNbt(nbtCompound);
				nbtList.add(nbtCompound);
			}
		}
        return nbtList;
    }
	
    public void readNbt(NbtList nbtList) {
		this.inv.clear();

		for(int i = 0; i < nbtList.size(); ++i) {
			NbtCompound nbtCompound = nbtList.getCompound(i);
			int j = nbtCompound.getByte("Slot") & 255;
			ItemStack itemStack = ItemStack.fromNbt(nbtCompound);
			if (!itemStack.isEmpty()) {
				if (j >= 0 && j < this.inv.size()) {
					this.inv.set(j, itemStack);
				}
			}
		}
	}
}
