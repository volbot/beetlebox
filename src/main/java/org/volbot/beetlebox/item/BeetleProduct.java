package org.volbot.beetlebox.item;

import java.util.List;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

public class BeetleProduct extends Item {

  BeetleType beetle_type;

  public BeetleProduct(BeetleType beetle_type, Settings settings) {
    super(settings);
    this.beetle_type = beetle_type;
  }

  @Override
  public void appendTooltip(
      ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
    BeetleProductUtils.appendBeetleNameTooltip(stack, world, tooltip, context, beetle_type);
  }
}
