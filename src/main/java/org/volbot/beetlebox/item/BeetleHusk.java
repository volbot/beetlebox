package org.volbot.beetlebox.item;

import java.util.List;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class BeetleHusk extends Item {

  public BeetleHusk(Settings settings) {
    super(settings);
  }

  @Override
  public void appendTooltip(
      ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
    tooltip.addAll(
        Text.of("Obtained by hatching larvae")
            .getWithStyle(Style.EMPTY.withColor(TextColor.fromFormatting(Formatting.GRAY))));
  }
}
