package org.volbot.beetlebox.item;

import java.awt.Color;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import org.volbot.beetlebox.registry.ItemRegistry;

public class FruitSyrup extends Item {

    public FruitSyrup(Settings settings) {
        super(settings);
    }

    public static int getColor(ItemStack stack) {
        NbtCompound nbt = stack.getOrCreateNbt();
        String fruit = nbt.getString("FruitType");
        int level = nbt.getInt("Level");
        int r, g, b;
        if(stack.isOf(ItemRegistry.APPLE_SYRUP) || fruit.equals("apple")) {
            r = 221;
            g = 21;
            b = 51;
        } else if(stack.isOf(ItemRegistry.MELON_SYRUP) || fruit.equals("melon")) {
            r = 77;
            g = 221;
            b = 77;
        } else if(stack.isOf(ItemRegistry.BERRY_SYRUP) || fruit.equals("berry")) {
            r = 214;
            g = 28;
            b = 86;
        } else if(stack.isOf(ItemRegistry.SUGAR_SYRUP) || fruit.equals("sugar")) {
            r = 175;
            g = 110;
            b = 77;
        } else if(stack.isOf(ItemRegistry.CACTUS_SYRUP) || fruit.equals("cactus")) {
            r = 175;
            g = 167;
            b = 123;
        } else if(stack.isOf(ItemRegistry.HONEY_SYRUP) || fruit.equals("honey")) {
            r = 235;
            g = 188;
            b = 78;
        } else {
            r = 255;
            g = 255;
            b = 255;
        }
        if(level>0){
            double intensity = (4-level)/4.;
            r+=((255-r)*intensity);
            g+=((255-g)*intensity);
            b+=((255-b)*intensity);
        }
        return (new Color(r, g, b)).getRGB();
    }


}
