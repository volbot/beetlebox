package org.volbot.beetlebox.item;

import java.util.List;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

public class BeetleProductUtils {

  public static void appendBeetleNameTooltip(
      ItemStack stack,
      @Nullable World world,
      List<Text> tooltip,
      TooltipContext context,
      BeetleType beetle_type) {
    tooltip.addAll(
        Text.of(beetle_type.nameScientificShort())
            .getWithStyle(
                Style.EMPTY.withItalic(true).withColor(TextColor.fromFormatting(Formatting.GRAY))));
  }
}
