package org.volbot.beetlebox.network;

import net.minecraft.nbt.NbtCompound;
import org.volbot.beetlebox.item.equipment.BeetlepackItem;
import org.volbot.beetlebox.item.equipment.BeetlepackUtils;
import org.volbot.beetlebox.item.equipment.BeetleArmorAbilities;
import net.minecraft.text.Text;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.util.Identifier;
import net.minecraft.item.ItemStack;

public class BBPacketHandler {
    public static void registerKeybindReceivers() {
        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetle_elytraboost_packet"), 
                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        BeetleArmorAbilities.elytra_boost(player); 
                    });
                });
        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetle_togglewallclimb_packet"), 
                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        BeetleArmorAbilities.toggle_wallclimb(player);
                    });
                });
        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetlepack_toggleattack_packet"), 
                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
                        if(bp.isEmpty()) { return; }
                        NbtCompound bp_nbt = bp.getOrCreateNbt();

                        boolean current = bp_nbt.getBoolean("ToggleAttack");
                        bp_nbt.putBoolean("ToggleAttack", !current);

                        player.sendMessage(Text.literal("Beetlepack: Combat Deployment "+(current?"Enabled":"Disabled")), true);
                    });
                });
        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetlepack_toggleflight_packet"), 
                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
                        if(bp.isEmpty()) { return; }
                        NbtCompound bp_nbt = bp.getOrCreateNbt();

                        boolean current = bp_nbt.getBoolean("ToggleFlight");
                        bp_nbt.putBoolean("ToggleFlight", !current);

                        player.sendMessage(Text.literal("Beetlepack: Flight Deployment "+(current?"Enabled":"Disabled")), true);
                    });
                });
        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetlepack_toggleintake_packet"), 
                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
                        if(bp.isEmpty()) { return; }
                        NbtCompound bp_nbt = bp.getOrCreateNbt();

                        boolean current = bp_nbt.getBoolean("ToggleIntake");
                        bp_nbt.putBoolean("ToggleIntake", !current);

                        player.sendMessage(Text.literal("Beetlepack: Inventory Handling "+(current?"Enabled":"Disabled")), true);
                    });
                });

        ServerPlayNetworking.registerGlobalReceiver(
                new Identifier("beetlebox", "beetlepack_open_packet"), 

                (server, player, handler, buf, responseSender) -> {
                    server.execute(() -> { 
                        ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
                        if(bp.isEmpty()) {
                            return;
                        }
                        player.openHandledScreen((BeetlepackItem)bp.getItem());
                    });
                });
    }
}
