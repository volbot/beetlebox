package org.volbot.beetlebox;

import net.fabricmc.loader.api.FabricLoader;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.StrippableBlockRegistry;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.data.JsonDataGenerator;
import org.volbot.beetlebox.registry.BeetleFlammableBlockRegistry;
import org.volbot.beetlebox.registry.BeetleRegistry;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.DataRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;
import org.volbot.beetlebox.registry.ItemGroupRegistry;
import org.volbot.beetlebox.registry.BBSoundRegistry;
import org.volbot.beetlebox.network.BBPacketHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeetleBoxMod implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("beetlebox");
    
	@Override
	public void onInitialize() {
		
		LOGGER.info("BEETLEBOX HAS HATCHED!");
        
        BBConfigRegistry.registerConfigs();
		
        LOGGER.info("BEETLEBOX: Wow, nice setup you've got here!");

        if (FabricLoader.getInstance().isModLoaded("trinkets")) {
		    LOGGER.info("BEETLEBOX: Trinkets looks just *gorgeous* on your modlist!");
            org.volbot.beetlebox.compat.trinkets.BeetlepackTrinket.init();
        } else {
		    LOGGER.info("BEETLEBOX: Though, it'd be nicer with Trinkets installed...");
        }

		LOGGER.info("BEETLEBOX: I'll just move a couple things in to spruce the place up.");
        BBSoundRegistry.register();
        ItemRegistry.register();
        BlockRegistry.register();
        BeetleRegistry.register();
        ItemGroupRegistry.register();
        DataRegistry.register();
		
		LOGGER.info("BEETLEBOX: This goes here... that goes there...");
        JsonDataGenerator.generateModWorldGen();
        BeetleFlammableBlockRegistry.registerFlammableBlocks();
        StrippableBlockRegistry.register(BlockRegistry.ASH_LOG, BlockRegistry.ASH_LOG_STRIPPED);
        StrippableBlockRegistry.register(BlockRegistry.ASH_WOOD, BlockRegistry.ASH_WOOD_STRIPPED);        
        
        BBPacketHandler.registerKeybindReceivers();
        LOGGER.info("BEETLEBOX: All done! Doesn't it feel brighter in here already?");
        LOGGER.info("BEETLEBOX: Just don't go looking for your catalytic converter...");
    }
}
