package org.volbot.beetlebox.client.registry;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.minecraft.client.color.world.FoliageColors;
import net.minecraft.client.render.RenderLayer;
import org.volbot.beetlebox.client.render.block.entity.*;
import org.volbot.beetlebox.registry.BlockRegistry;

@SuppressWarnings("deprecation")
@Environment(EnvType.CLIENT)
public class BBBlockModels {

  public static void register() {
    registerBlockModels();
    registerBlockEntities();
    registerDynamicColors();
  }

  public static void registerBlockModels() {
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.TANK, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.ASH_LEAVES, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.ASH_SAPLING, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.ASH_TRAPDOOR, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.ASH_DOOR, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.IMMIGRATOR, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.EMIGRATOR, RenderLayer.getCutout());
    BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.INCUBATOR, RenderLayer.getCutout());
  }

  public static void registerBlockEntities() {
    BlockEntityRendererRegistry.register(
        BlockRegistry.TANK_BLOCK_ENTITY, TankBlockEntityRenderer::new);
    BlockEntityRendererRegistry.register(
        BlockRegistry.BOILER_BLOCK_ENTITY, BoilerBlockEntityRenderer::new);
    BlockEntityRendererRegistry.register(
        BlockRegistry.INCUBATOR_BLOCK_ENTITY, IncubatorBlockEntityRenderer::new);
  }

  public static void registerDynamicColors() {
    // register colorproviders
    ColorProviderRegistry.BLOCK.register(
        (state, world, pos, tintIndex) -> {
          return FoliageColors.getBirchColor();
        },
        BlockRegistry.ASH_LEAVES);
  }
}
