package org.volbot.beetlebox.client.registry;

import java.util.HashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ArmorRenderer;
import net.fabricmc.fabric.api.client.rendering.v1.LivingEntityFeatureRendererRegistrationCallback;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.LivingEntity;
import org.volbot.beetlebox.client.model.armor.BeetlepackModel;
import org.volbot.beetlebox.client.model.armor.beetle.*;
import org.volbot.beetlebox.client.render.armor.BeetlepackRenderer;
import org.volbot.beetlebox.client.render.armor.beetle.BeetleArmorRenderer;
import org.volbot.beetlebox.client.render.armor.beetle.BeetleElytraFeatureRenderer;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.item.equipment.BeetleArmorItem;
import org.volbot.beetlebox.registry.ItemRegistry;

@SuppressWarnings("deprecation")
@Environment(EnvType.CLIENT)
public class BBArmorModels {

  public static HashMap<String, BeetleArmorEntityModel<?>> beetle_helmets = new HashMap<>();

  public static void register() {
    registerHelmetModels();
    registerBeetlepackModels();
    registerBeetleElytraModels();
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static void registerHelmetModels() {
    // create hashmap for helmet models
    beetle_helmets.put(BeetleType.A_DICHOTOMA.id(), new JRBHelmetModel<>());
    beetle_helmets.put(BeetleType.D_HERCULES.id(), new HercHelmetModel<>());
    beetle_helmets.put(BeetleType.D_TITANUS.id(), new TitanHelmetModel<>());
    beetle_helmets.put(BeetleType.C_ATLAS.id(), new AtlasHelmetModel<>());
    beetle_helmets.put(BeetleType.M_ELEPHAS.id(), new ElephantHelmetModel<>());
    beetle_helmets.put(BeetleType.D_TITYUS.id(), new TityusHelmetModel<>());
    beetle_helmets.put(BeetleType.M_ACTAEON.id(), new ActaeonHelmetModel<>());
    beetle_helmets.put(BeetleType.C_MINUTUS.id(), new BlackDungHelmetModel<>());
    beetle_helmets.put(BeetleType.T_VIRIDIAURATA.id(), new GreenRhinoHelmetModel<>());

    // register renderers
    for (BeetleType beetle_type : BeetleType.values()) {
      BeetleArmorItem[] armorItems = beetle_type.armorItems();
      BeetleArmorEntityModel model = beetle_helmets.get(beetle_type.id());
      if (model == null) {
        model = new StandardHelmetModel<>(beetle_type.id());
      }
      ArmorRenderer.register(new BeetleArmorRenderer(model, beetle_type), armorItems[0]);
      ArmorRenderer.register(
          new BeetleArmorRenderer(
              new BeetleArmorEntityModel<LivingEntity>(beetle_type.id(), false), beetle_type),
          armorItems[1]);
      ArmorRenderer.register(
          new BeetleArmorRenderer(
              new BeetleArmorEntityModel<LivingEntity>(beetle_type.id(), false), beetle_type),
          armorItems[2]);
      ArmorRenderer.register(
          new BeetleArmorRenderer(
              new BeetleArmorEntityModel<LivingEntity>(beetle_type.id(), true), beetle_type),
          armorItems[3]);
    }
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static void registerBeetlepackModels() {
    // register renderer
    ArmorRenderer.register(
        new BeetlepackRenderer(new BeetlepackModel<>()), ItemRegistry.BEETLEPACK);
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static void registerBeetleElytraModels() {
    // register feature renderer
    LivingEntityFeatureRendererRegistrationCallback.EVENT.register(
        (entityType, entityRenderer, registrationHelper, context) -> {
          if (entityRenderer.getModel() instanceof BipedEntityModel) {
            registrationHelper.register(
                new BeetleElytraFeatureRenderer(entityRenderer, context.getModelLoader()));
          }
        });
  }
}
