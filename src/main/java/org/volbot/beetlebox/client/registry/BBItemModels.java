package org.volbot.beetlebox.client.registry;

import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.minecraft.client.color.world.FoliageColors;
import net.minecraft.client.item.ModelPredicateProviderRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.data.recipe.BeetleRecipeGenerator;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.item.FruitSyrup;
import org.volbot.beetlebox.item.tools.BugslingerItem;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BBItemModels {

  public static void register() {
    registerItemModels();
    registerDynamicColors();
  }

  public static void registerItemModels() {
    // register bugslinger states for texturing the green dot
    ModelPredicateProviderRegistry.register(
        ItemRegistry.BUGSLINGER,
        new Identifier("state"),
        (itemStack, clientWorld, livingEntity, whatever) -> {
          if (livingEntity == null || !(livingEntity instanceof PlayerEntity)) {
            return 0F;
          }
          return BugslingerItem.getModelState(itemStack, (PlayerEntity) livingEntity);
        });

    // register filled state for usage in jar textures
    ModelPredicateProviderRegistry.register(
        ItemRegistry.BEETLE_JAR,
        new Identifier("full"),
        (itemStack, clientWorld, livingEntity, whatever) -> {
          if (livingEntity == null || itemStack.getNbt() == null) {
            return 0F;
          }
          return itemStack.getNbt().contains("EntityType") ? 1F : 0F;
        });
    ModelPredicateProviderRegistry.register(
        ItemRegistry.LEG_BEETLE_JAR,
        new Identifier("full"),
        (itemStack, clientWorld, livingEntity, whatever) -> {
          if (livingEntity == null || itemStack.getNbt() == null) {
            return 0F;
          }
          return itemStack.getNbt().contains("EntityType") ? 1F : 0F;
        });
    ModelPredicateProviderRegistry.register(
        ItemRegistry.LARVA_JAR,
        new Identifier("full"),
        (itemStack, clientWorld, livingEntity, whatever) -> {
          if (livingEntity == null || itemStack.getNbt() == null) {
            return 0F;
          }
          return itemStack.getNbt().contains("EntityType") ? 1F : 0F;
        });
  }

  public static void registerDynamicColors() {
    // register colorproviders
    ColorProviderRegistry.ITEM.register(
        (stack, tintIndex) -> {
          return FruitSyrup.getColor(stack);
        },
        ItemRegistry.BEETLE_JELLY);
    ColorProviderRegistry.ITEM.register(
        (stack, tintIndex) -> {
          return FoliageColors.getBirchColor();
        },
        BlockRegistry.ASH_LEAVES);

    for (BeetleType beetle_type : BeetleType.values()) {
      ColorProviderRegistry.ITEM.register(
          (stack, tintIndex) -> {
            return beetle_type.color1();
          },
          beetle_type.elytron());
      Item[] armor = beetle_type.armorItems();
      for (int i = 0; i < 4; i++) {
        ColorProviderRegistry.ITEM.register(
            (stack, tintIndex) -> {
              return beetle_type.color1();
            },
            armor[i]);
      }
    }

    // register each of the syrups
    for (Item i : BeetleRecipeGenerator.stat_syrups) {
      ColorProviderRegistry.ITEM.register(
          (stack, tintIndex) -> {
            return FruitSyrup.getColor(stack);
          },
          i);
    }
    for (Item i : BeetleRecipeGenerator.misc_syrups) {
      ColorProviderRegistry.ITEM.register(
          (stack, tintIndex) -> {
            return FruitSyrup.getColor(stack);
          },
          i);
    }
  }
}
