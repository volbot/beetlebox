package org.volbot.beetlebox.client.registry;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.client.model.entity.beetle.BeetleEntityModel;
import org.volbot.beetlebox.client.render.entity.beetle.BeetleEntityRenderer;
import org.volbot.beetlebox.client.render.entity.projectile.BeetleProjectileEntityRenderer;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.registry.BeetleRegistry;

@Environment(EnvType.CLIENT)
public class BBEntityModels {

  public static void register() {
    registerBeetleModels();
    registerBeetleProjectileModel();
  }

  public static void registerBeetleModels() {
    for (BeetleType beetle_type : BeetleType.values()) {
      registerBeetleRenderer(beetle_type);
    }
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static void registerBeetleRenderer(BeetleType beetle_type) {
    Identifier beetle_id = new Identifier("beetlebox", beetle_type.id());
    Identifier texture =
        new Identifier("beetlebox", "textures/entity/beetle/" + beetle_type.id() + ".png");
    EntityModelLayer model_layer = new EntityModelLayer(beetle_id, "main");
    EntityRendererRegistry.register(
        beetle_type.entityType(),
        (var1) ->
            new BeetleEntityRenderer(var1, texture, model_layer, beetle_type.headTurnScale()));
    EntityModelLayerRegistry.registerModelLayer(
        model_layer, () -> BeetleEntityModel.getTexturedModelData(beetle_type));
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  public static void registerBeetleProjectileModel() {
    EntityRendererRegistry.register(
        BeetleRegistry.BEETLE_PROJECTILE, (context) -> new BeetleProjectileEntityRenderer(context));
  }
}
