package org.volbot.beetlebox.client.model.armor.beetle;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.ModelData;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.entity.model.ArmorEntityModel;
import net.minecraft.entity.LivingEntity;

@Environment(EnvType.CLIENT)
public class BeetleArmorEntityModel<T extends LivingEntity> extends ArmorEntityModel<T> {

  private final String name;

  public BeetleArmorEntityModel(ModelPart part, String name) {
    super(part);
    this.name = name;
  }

  public BeetleArmorEntityModel(String name) {
    this(name, false);
  }

  public BeetleArmorEntityModel(String name, boolean outerModel) {
    super(BeetleArmorEntityModel.getTexturedModelData(outerModel).createModel());
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public static TexturedModelData getTexturedModelData(boolean outerModel) {
    ModelData modelData = ArmorEntityModel.getModelData(new Dilation(outerModel ? 1.4F : 1F));
    return TexturedModelData.of(modelData, 64, 32);
  }
}
