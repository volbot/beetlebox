package org.volbot.beetlebox.client.model.entity.beetle;

import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.ModelPartBuilder;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.ModelTransform;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

public class VariantModels {

  public static ModelPartData apply_variant(ModelPartData modelPartData, BeetleType beetle_type) {
    ModelPartData thorax = modelPartData.getChild("Body").getChild("Thorax");
    ModelPartData thoraxAttach = thorax.getChild("ThoraxAttach");
    // ThoraxAttach
    ModelPartData cube;
    switch (beetle_type) {
      case A_DICHOTOMA:
        cube =
            thoraxAttach.addChild(
                "cube_r777",
                ModelPartBuilder.create()
                    .uv(47, 52)
                    .cuboid(-0.75F, -1.25F, 1.0F, 2.0F, 2.0F, 2.0F, new Dilation(0.0F)),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(58, 57)
                .cuboid(0.0F, -2.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(0.0F, 0.0F, 0.0F, 0.2618F, -0.5236F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(58, 57)
                .mirrored()
                .cuboid(-1.0F, -2.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.5F, 0.0F, 0.0F, 0.2618F, 0.5236F, 0.0F));

        cube.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(56, 52)
                .cuboid(-1.0F, -2.0F, -2.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.75F, 0.25F, 1.5F, -0.3927F, 0.0F, 0.0F));
        break;
      case C_ATLAS:
        cube =
            thoraxAttach.addChild(
                "cube_r777",
                ModelPartBuilder.create()
                    .uv(58, 61)
                    .cuboid(-0.25F, -0.1808F, -0.9146F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));
        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(43, 57)
                .mirrored()
                .cuboid(-1.5F, -0.5F, -2.75F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-2.2519F, -0.25F, 1.0F, -0.2618F, 0.4363F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(43, 57)
                .mirrored()
                .cuboid(-1.0F, -0.25F, -6.25F, 1.0F, 1.0F, 6.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.7519F, -0.75F, -0.25F, 0.0873F, -0.3624F, 0.0F));

        cube.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(43, 57)
                .cuboid(0.0F, -0.25F, -6.25F, 1.0F, 1.0F, 6.0F, new Dilation(0.0F)),
            ModelTransform.of(4.25F, -0.75F, -0.25F, 0.0873F, 0.3624F, 0.0F));

        cube.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(43, 57)
                .mirrored()
                .cuboid(-0.5F, -0.5F, -2.75F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(2.75F, -0.25F, 1.0F, -0.2618F, -0.4363F, 0.0F));

        cube.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(47, 50)
                .cuboid(-0.5F, -1.75F, -0.5F, 1.0F, 3.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.25F, 1.6386F, 1.036F, -0.3054F, 0.0F, 0.0F));
        break;
      case M_ACTAEON:
        cube =
            thoraxAttach.addChild(
                "object_r777",
                ModelPartBuilder.create(),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));
        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(45, 50)
                .mirrored()
                .cuboid(-1.5F, -0.5F, -1.75F, 2.0F, 2.0F, 4.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(44, 58)
                .cuboid(3.5019F, -0.5F, -1.75F, 2.0F, 2.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.7519F, 0.75F, 1.0F, -0.2618F, 0.0F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(56, 52)
                .mirrored()
                .cuboid(-1.0F, -0.25F, -3.25F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(56, 52)
                .cuboid(4.0019F, -0.25F, -3.25F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.7519F, 0.45F, 0.0F, 0.1745F, 0.0F, 0.0F));
        break;
      case M_ELEPHAS:
        cube =
            thoraxAttach.addChild(
                "ThoraxAttach",
                ModelPartBuilder.create(),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));
        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(58, 61)
                .cuboid(0.5F, -0.2F, -2.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(2.25F, 0.85F, 1.0F, 0.0F, -0.6981F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(46, 51)
                .cuboid(0.0F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(2.25F, 0.75F, 1.0F, -0.1309F, -0.6981F, 0.0F));

        cube.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(46, 51)
                .mirrored()
                .cuboid(-2.0F, -0.5F, -0.75F, 2.0F, 2.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.7519F, 0.75F, 1.0F, -0.1309F, 0.6981F, 0.0F));

        cube.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(58, 61)
                .mirrored()
                .cuboid(-1.5F, -0.2F, -2.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.7519F, 0.85F, 1.0F, 0.0F, 0.6981F, 0.0F));
        break;
      case D_HERCULES:
        cube =
            thoraxAttach.addChild(
                "cube_r777",
                ModelPartBuilder.create()
                    .uv(44, 50)
                    .cuboid(-2.25F, -1.0F, -0.5F, 5.0F, 3.0F, 4.0F, new Dilation(0.0F)),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(43, 57)
                .cuboid(0.0F, -3.25F, -12.5F, 1.0F, 1.0F, 6.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.25F, -1.4F, 1.0F, 0.2182F, 0.0F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(46, 38)
                .cuboid(-1.5F, -1.0F, -3.5F, 4.0F, 3.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.25F, -0.3484F, 1.5691F, -0.3491F, 0.0F, 0.0F));

        cube.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(43, 57)
                .cuboid(-0.5F, -1.0F, -8.5F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.25F, -0.15F, 1.75F, -0.3054F, 0.0F, 0.0F));
        break;
      case D_TITYUS:
        cube =
            thoraxAttach.addChild(
                "cube_r777",
                ModelPartBuilder.create(),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

        cube.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(34, 52)
                .cuboid(0.0F, -3.25F, -8.5F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.25F, 0.6F, 4.75F, 0.1745F, 0.0F, 0.0F));

        cube.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(47, 38)
                .cuboid(-1.5F, -1.0F, -3.5F, 3.0F, 3.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.25F, 0.1516F, 3.0691F, -0.2182F, 0.0F, 0.0F));

        cube.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(46, 51)
                .cuboid(-0.5F, -1.0F, -6.5F, 2.0F, 2.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.25F, 1.35F, 3.75F, -0.3927F, 0.0F, 0.0F));
        break;
      case T_VIRIDIAURATA:
        cube =
            thoraxAttach.addChild(
                "ThoraxAttach_r777",
                ModelPartBuilder.create()
                    .uv(47, 39)
                    .cuboid(-1.251F, -1.0F, -0.5F, 3.0F, 3.0F, 4.0F, new Dilation(0.0F)),
                ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

        cube.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(59, 62)
                .cuboid(-0.5F, -0.5F, 1.5F, 1.0F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.25F, 1.7145F, -6.6892F, 1.0647F, 0.0F, 0.0F));

        cube.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(33, 51)
                .cuboid(-0.5F, 0.0F, -14.0F, 1.0F, 1.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(0.25F, -2.7838F, 7.9317F, 0.1484F, 0.0F, 0.0F));

        cube.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(28, 58)
                .cuboid(-1.0F, -1.25F, -0.5F, 2.0F, 2.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(0.25F, -0.5386F, -2.064F, -0.2007F, 0.0F, 0.0F));
        break;
      default:
        break;
    }
    // Horn
    ModelPartData horn;
    switch (beetle_type) {
      case A_DICHOTOMA:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(58, 61)
                .cuboid(1.0F, 1.2F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.249F, -3.25F, -7.2064F, -0.2618F, -0.9163F, 0.0F));

        horn.addChild(
            "cube_r21",
            ModelPartBuilder.create()
                .uv(58, 61)
                .cuboid(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.249F, -3.25F, -6.9564F, -0.2618F, -0.5236F, 0.0F));

        horn.addChild(
            "cube_r22",
            ModelPartBuilder.create()
                .uv(58, 61)
                .mirrored()
                .cuboid(-1.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.251F, -3.25F, -6.9564F, -0.2618F, 0.5236F, 0.0F));

        horn.addChild(
            "cube_r23",
            ModelPartBuilder.create()
                .uv(58, 61)
                .mirrored()
                .cuboid(-2.0F, 1.2F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.251F, -3.25F, -7.2064F, -0.2618F, 0.9163F, 0.0F));

        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(42, 57)
                .cuboid(-1.0F, -1.0F, -12.0F, 2.0F, 1.0F, 6.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 1.5F, 5.2936F, -0.2182F, 0.0F, 0.0F));
        break;
      case C_ATLAS:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(58, 57)
                .cuboid(-0.5F, -1.0F, -11.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 7.5863F, -0.4448F, -1.0908F, 0.0F, 0.0F));

        horn.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(58, 57)
                .cuboid(-0.5F, -1.0F, -11.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 3.3655F, 5.7361F, -0.3491F, 0.0F, 0.0F));

        horn.addChild(
            "head_r4",
            ModelPartBuilder.create()
                .uv(45, 60)
                .cuboid(-1.0F, -0.25F, -1.5F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, 0.3054F, 0.0F, 0.0F));
        break;
      case M_ACTAEON:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(58, 61)
                .cuboid(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.249F, -2.65F, -6.7564F, -0.2618F, -0.5236F, 0.0F));

        horn.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(58, 61)
                .mirrored()
                .cuboid(-1.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.249F, -2.65F, -6.7564F, -0.2618F, 0.5236F, 0.0F));

        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(45, 60)
                .cuboid(-0.5F, -1.0F, -12.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 2.1F, 5.4936F, -0.2182F, 0.0F, 0.0F));

        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(46, 60)
                .cuboid(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, -0.3054F, 0.0F, 0.0F));

        horn.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(45, 60)
                .cuboid(-1.0F, -0.25F, -1.5F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, 0.0436F, 0.0F, 0.0F));
        break;
      case M_ELEPHAS:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "cube_r21",
            ModelPartBuilder.create()
                .uv(58, 57)
                .cuboid(0.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.249F, -2.65F, -5.5064F, -0.8498F, -0.4478F, 0.281F));

        horn.addChild(
            "cube_r22",
            ModelPartBuilder.create()
                .uv(58, 57)
                .mirrored()
                .cuboid(-1.0F, 1.0F, -1.0F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.249F, -2.65F, -5.5064F, -0.8498F, 0.4478F, -0.281F));

        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(56, 52)
                .cuboid(-0.5F, -1.0F, -12.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 2.1F, 5.4936F, -0.2182F, 0.0F, 0.0F));

        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(46, 59)
                .cuboid(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, -0.3054F, 0.0F, 0.0F));

        horn.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(54, 47)
                .cuboid(-1.0F, -0.25F, -1.5F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, 0.0436F, 0.0F, 0.0F));
        break;
      case D_HERCULES:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(35, 53)
                .cuboid(-0.5F, -0.5F, 0.5F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -3.0322F, -7.9828F, -0.7592F, 0.0F, 0.0F));

        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(33, 51)
                .cuboid(-0.5F, -1.0F, -14.0F, 1.0F, 1.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 4.6338F, 6.3253F, -0.4102F, 0.0F, 0.0F));

        horn.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(27, 57)
                .cuboid(-1.0F, -0.75F, -1.5F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.6114F, -1.9204F, 0.2007F, 0.0F, 0.0F));
        break;
      case D_TITYUS:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(45, 59)
                .cuboid(-0.5F, -1.0F, -12.0F, 1.0F, 1.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, 3.6338F, 6.5753F, -0.3665F, 0.0F, 0.0F));

        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(43, 57)
                .cuboid(-1.0F, -0.75F, -0.5F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.001F, -0.3614F, -1.6704F, 0.5061F, 0.0F, 0.0F));
        break;
      case C_MINUTUS:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(48, 61)
                .mirrored()
                .cuboid(-0.5F, -0.5F, -2.0F, 1.0F, 2.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(1.0607F, -3.2892F, -0.8787F, 0.1719F, 0.778F, 0.1213F));

        horn.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(54, 60)
                .cuboid(-0.5F, -0.5F, -1.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(1.0F, 0.299F, -0.2944F, 0.1137F, 0.6949F, 0.073F));

        horn.addChild(
            "head_r3",
            ModelPartBuilder.create()
                .uv(54, 60)
                .mirrored()
                .cuboid(-1.5F, -0.5F, -1.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.0F, 0.299F, -0.2944F, 0.1137F, -0.6949F, -0.073F));

        horn.addChild(
            "head_r4",
            ModelPartBuilder.create()
                .uv(38, 53)
                .mirrored()
                .cuboid(-0.5F, -0.5F, -2.0F, 2.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.3536F, 0.299F, -0.5444F, 0.1108F, 0.7823F, 0.0783F));

        horn.addChild(
            "head_r5",
            ModelPartBuilder.create()
                .uv(58, 52)
                .cuboid(-0.5F, -1.5F, -2.0F, 1.0F, 2.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.0607F, -1.051F, -0.1944F, 0.6244F, -0.6816F, -0.4262F));
      case T_VIRIDIAURATA:
        horn = thorax.getChild("Head").getChild("Horn");
        horn.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(39, 46)
                .cuboid(0.0F, 2.25F, -9.5F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.499F, 4.215F, 0.5181F, -0.8727F, 0.0F, 0.0F));

        horn.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(58, 57)
                .cuboid(0.0F, 2.25F, -9.5F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.499F, 6.7813F, -5.494F, -1.6581F, 0.0F, 0.0F));

        horn.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(56, 52)
                .cuboid(0.0F, 2.25F, -9.5F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.499F, -0.9488F, 5.8159F, -0.0873F, 0.0F, 0.0F));

        horn.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(55, 47)
                .cuboid(0.0F, 2.25F, -9.5F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.499F, 0.3158F, 3.2452F, -0.3054F, 0.0F, 0.0F));

        horn.addChild(
            "cube_r21",
            ModelPartBuilder.create()
                .uv(45, 59)
                .cuboid(-0.5F, -1.0F, -6.5F, 2.0F, 2.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.499F, -2.5F, 3.6436F, 0.6109F, 0.0F, 0.0F));
        break;
      default:
        break;
    }

    return modelPartData;
  }
}
