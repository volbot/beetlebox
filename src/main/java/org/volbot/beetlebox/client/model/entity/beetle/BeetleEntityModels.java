package org.volbot.beetlebox.client.model.entity.beetle;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.ModelPartBuilder;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.ModelTransform;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

@Environment(EnvType.CLIENT)
public class BeetleEntityModels {

  public static ModelPartData get_model(ModelPartData modelPartData, BeetleType beetle_type) {

    switch (beetle_type) {
      case D_TITANUS:
        modelPartData = stag_beetle(modelPartData);
        break;
      case A_VERRUCOSUS:
        modelPartData = segment_beetle(modelPartData);
        break;
      default:
        modelPartData = base_beetle(modelPartData);
        break;
    }

    modelPartData = VariantModels.apply_variant(modelPartData, beetle_type);

    return modelPartData;
  }

  public static ModelPartData base_beetle(ModelPartData modelPartData) {
    ModelPartData Body =
        modelPartData.addChild(
            "Body", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

    ModelPartData Abdomen =
        Body.addChild(
            "Abdomen", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -5.5F, -2.0F));

    ModelPartData AbdomenLegs =
        Abdomen.addChild(
            "AbdomenLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 5.5F, 2.0F));

    ModelPartData AbdomenLeftLegs =
        AbdomenLegs.addChild(
            "AbdomenLeftLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenLeftBackLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-3.0F, -2.0F, -5.0F));

    AbdomenLeftBackLeg.addChild(
        "cube_r1",
        ModelPartBuilder.create()
            .uv(26, 22)
            .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
        ModelTransform.of(1.284F, 1.3323F, 0.1839F, -0.4939F, 0.4703F, -0.2393F));

    ModelPartData AbdomenLeftBackLegLower =
        AbdomenLeftBackLeg.addChild(
            "AbdomenLeftBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, 2.5F, 3.25F));

    AbdomenLeftBackLegLower.addChild(
        "cube_r2",
        ModelPartBuilder.create()
            .uv(0, 10)
            .mirrored()
            .cuboid(-0.5F, -0.4183F, -0.4977F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.of(0.0F, 0.0F, 0.5F, 1.2217F, 0.0F, 0.0F));

    ModelPartData AbdomenLeftForwardLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, -2.0F, -8.0F));

    AbdomenLeftForwardLeg.addChild(
        "cube_r3",
        ModelPartBuilder.create()
            .uv(0, 22)
            .cuboid(-0.648F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
        ModelTransform.of(0.284F, 0.5823F, 0.1839F, -0.4761F, 1.1883F, -0.0554F));

    ModelPartData AbdomenLeftForwardLegLower =
        AbdomenLeftForwardLeg.addChild(
            "AbdomenLeftForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.0F, 2.5F, 1.0F));

    AbdomenLeftForwardLegLower.addChild(
        "cube_r4",
        ModelPartBuilder.create()
            .uv(0, 0)
            .cuboid(2.0F, -2.45F, 0.35F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
        ModelTransform.of(-2.0F, 1.0F, 3.0F, 1.3188F, 0.3711F, 0.1699F));

    ModelPartData AbdomenRightLegs =
        AbdomenLegs.addChild(
            "AbdomenRightLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenRightForwardLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, -2.0F, -8.0F));

    AbdomenRightForwardLeg.addChild(
        "cube_r5",
        ModelPartBuilder.create()
            .uv(0, 22)
            .mirrored()
            .cuboid(-1.352F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.of(-0.284F, 0.5823F, 0.1839F, -0.4761F, -1.1883F, 0.0554F));

    ModelPartData AbdomenRightForwardLegLower =
        AbdomenRightForwardLeg.addChild(
            "AbdomenRightForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.0F, 2.5F, 1.0F));

    AbdomenRightForwardLegLower.addChild(
        "cube_r6",
        ModelPartBuilder.create()
            .uv(0, 0)
            .mirrored()
            .cuboid(-0.5952F, -0.9478F, -0.6301F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.of(0.0F, 0.0F, 1.0F, 1.3188F, -0.3711F, -0.1699F));

    ModelPartData AbdomenRightBackLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(3.0F, -2.0F, -5.0F));

    ModelPartData cube_r7 =
        AbdomenRightBackLeg.addChild(
            "cube_r7",
            ModelPartBuilder.create()
                .uv(26, 22)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.284F, 1.3323F, 0.1839F, -0.4939F, -0.4703F, 0.2393F));

    ModelPartData AbdomenRightBackLegLower =
        AbdomenRightBackLeg.addChild(
            "AbdomenRightBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, 2.0F, 3.25F));

    ModelPartData cube_r8 =
        AbdomenRightBackLegLower.addChild(
            "cube_r8",
            ModelPartBuilder.create()
                .uv(0, 10)
                .cuboid(0.0F, -1.7F, 0.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.5F, 1.5F, 1.5F, 1.2217F, 0.0F, 0.0F));

    Abdomen.addChild(
        "AbdomenMain",
        ModelPartBuilder.create()
            .uv(20, 29)
            .cuboid(1.0F, -5.0F, -5.0F, 1.0F, 2.0F, 5.0F, new Dilation(0.0F))
            .uv(0, 0)
            .cuboid(-3.0F, -5.0F, -5.0F, 4.0F, 2.0F, 7.0F, new Dilation(0.0F))
            .uv(10, 34)
            .cuboid(-2.0F, -6.0F, -5.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F))
            .uv(20, 29)
            .mirrored()
            .cuboid(-4.0F, -5.0F, -5.0F, 1.0F, 2.0F, 5.0F, new Dilation(0.0F))
            .mirrored(false)
            .uv(14, 10)
            .cuboid(-4.0F, -3.0F, -5.0F, 6.0F, 1.0F, 5.0F, new Dilation(0.0F))
            .uv(33, 34)
            .cuboid(-3.0F, -3.0F, 0.0F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F)),
        ModelTransform.pivot(1.0F, 5.0F, 6.0F));

    ModelPartData WingBone =
        Abdomen.addChild(
            "WingBone", ModelPartBuilder.create(), ModelTransform.pivot(1.0F, -0.5F, 1.0F));

    ModelPartData Elytra =
        WingBone.addChild(
            "Elytra", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, -1.0F, 0.0F));

    Elytra.addChild(
        "LeftElytron",
        ModelPartBuilder.create()
            .uv(14, 17)
            .mirrored()
            .cuboid(1.0F, 0.5F, 0.0F, 1.0F, 2.0F, 7.0F, new Dilation(0.0F))
            .mirrored(false)
            .uv(0, 10)
            .mirrored()
            .cuboid(-2.0F, -0.5F, 0.0F, 3.0F, 3.0F, 7.0F, new Dilation(0.0F))
            .mirrored(false)
            .uv(36, 22)
            .mirrored()
            .cuboid(-2.0F, -1.5F, 0.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F))
            .mirrored(false)
            .uv(37, 12)
            .mirrored()
            .cuboid(-2.0F, 0.5F, 7.0F, 3.0F, 2.0F, 1.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.pivot(2.0F, 1.0F, 0.0F));

    Elytra.addChild(
        "RightElytron",
        ModelPartBuilder.create()
            .uv(0, 10)
            .cuboid(-1.0F, -0.5F, 0.0F, 3.0F, 3.0F, 7.0F, new Dilation(0.0F))
            .uv(37, 12)
            .cuboid(-1.0F, 0.5F, 7.0F, 3.0F, 2.0F, 1.0F, new Dilation(0.0F))
            .uv(14, 17)
            .cuboid(-2.0F, 0.5F, 0.0F, 1.0F, 2.0F, 7.0F, new Dilation(0.0F))
            .uv(36, 22)
            .cuboid(0.0F, -1.5F, 0.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
        ModelTransform.pivot(-2.0F, 1.0F, 0.0F));

    ModelPartData Wings =
        WingBone.addChild(
            "Wings", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, 6.0F, 1.0F));

    ModelPartData RightWing =
        Wings.addChild(
            "RightWing", ModelPartBuilder.create(), ModelTransform.pivot(-2.0F, -5.5F, 0.0F));

    RightWing.addChild(
        "cube_r9",
        ModelPartBuilder.create()
            .uv(0, 56)
            .mirrored()
            .cuboid(-1.25F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
        ModelTransform.of(0.0F, 1.0F, 0.0F, 0.0F, 0.0F, -0.2618F));

    ModelPartData RightWingLower =
        RightWing.addChild(
            "RightWingLower", ModelPartBuilder.create(), ModelTransform.pivot(1.0F, 0.25F, 5.75F));

    RightWingLower.addChild(
        "cube_r10",
        ModelPartBuilder.create()
            .uv(10, 56)
            .mirrored()
            .cuboid(-1.25F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
        ModelTransform.of(-1.0F, 0.75F, -5.75F, 0.0F, 0.0F, -0.2618F));

    ModelPartData LeftWing =
        Wings.addChild(
            "LeftWing", ModelPartBuilder.create(), ModelTransform.pivot(2.0F, -5.5F, 0.0F));

    LeftWing.addChild(
        "cube_r11",
        ModelPartBuilder.create()
            .uv(0, 56)
            .cuboid(-3.75F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
        ModelTransform.of(0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.2618F));

    ModelPartData LeftWingLower =
        LeftWing.addChild(
            "LeftWingLower", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, 0.25F, 5.75F));

    LeftWingLower.addChild(
        "cube_r12",
        ModelPartBuilder.create()
            .uv(10, 56)
            .cuboid(-3.75F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
        ModelTransform.of(1.0F, 0.75F, -5.75F, 0.0F, 0.0F, 0.2618F));

    ModelPartData Thorax =
        Body.addChild(
            "Thorax", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -4.5F, -3.0F));

    ModelPartData ThoraxLegs =
        Thorax.addChild(
            "ThoraxLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.001F, 4.5F, 3.0436F));

    ModelPartData ThoraxLeftLeg =
        ThoraxLegs.addChild(
            "ThoraxLeftLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.716F, -2.8323F, -2.6839F));

    ThoraxLeftLeg.addChild(
        "cube_r13",
        ModelPartBuilder.create()
            .uv(10, 27)
            .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
        ModelTransform.of(0.284F, 0.3323F, 0.1839F, 0.4939F, -0.4703F, -0.2393F));

    ModelPartData ThoraxLeftLegLower =
        ThoraxLeftLeg.addChild(
            "ThoraxLeftLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(1.5F, 1.5F, -3.3822F));

    ThoraxLeftLegLower.addChild(
        "cube_r14",
        ModelPartBuilder.create()
            .uv(21, 37)
            .cuboid(-1.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
        ModelTransform.of(0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    ModelPartData ThoraxRightLeg =
        ThoraxLegs.addChild(
            "ThoraxRightLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.7179F, -2.8323F, -2.6839F));

    ThoraxRightLeg.addChild(
        "cube_r15",
        ModelPartBuilder.create()
            .uv(10, 27)
            .mirrored()
            .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.of(-0.284F, 0.3323F, 0.1839F, 0.4939F, 0.4703F, 0.2393F));

    ModelPartData ThoraxRightLegLower =
        ThoraxRightLeg.addChild(
            "ThoraxRightLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-1.5F, 1.5F, -3.3822F));

    ThoraxRightLegLower.addChild(
        "cube_r16",
        ModelPartBuilder.create()
            .uv(21, 37)
            .mirrored()
            .cuboid(0.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
            .mirrored(false),
        ModelTransform.of(-0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    Thorax.addChild(
        "ThoraxMain",
        ModelPartBuilder.create()
            .uv(16, 0)
            .cuboid(-2.0F, -3.0F, -5.0F, 6.0F, 3.0F, 3.0F, new Dilation(0.0F))
            .uv(35, 0)
            .cuboid(-1.0F, -4.0F, -4.0F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F))
            .uv(0, 37)
            .cuboid(-3.0F, -2.0F, -5.0F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F))
            .uv(0, 37)
            .mirrored()
            .cuboid(4.0F, -2.0F, -5.0F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F))
            .mirrored(false)
            .uv(28, 29)
            .cuboid(-2.0F, -2.0F, -6.0F, 6.0F, 3.0F, 1.0F, new Dilation(0.0F))
            .uv(24, 17)
            .cuboid(-2.0F, 0.0F, -5.0F, 6.0F, 1.0F, 3.0F, new Dilation(0.0F)),
        ModelTransform.pivot(-0.999F, 1.0F, 4.0436F));

    Thorax.addChild(
        "ThoraxAttach", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

    ModelPartData Head =
        Thorax.addChild(
            "Head",
            ModelPartBuilder.create()
                .uv(32, 7)
                .cuboid(-1.999F, -1.0F, -2.9564F, 4.0F, 2.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.pivot(0.0F, 1.0F, -1.0F));

    Head.addChild("Horn", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.5F, -2.0F));

    return modelPartData;
  }

  public static ModelPartData stag_beetle(ModelPartData modelPartData) {
    ModelPartData Body =
        modelPartData.addChild(
            "Body", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

    ModelPartData Abdomen =
        Body.addChild(
            "Abdomen", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -5.5F, -2.0F));

    ModelPartData AbdomenLegs =
        Abdomen.addChild(
            "AbdomenLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 5.5F, 2.0F));

    ModelPartData AbdomenLeftLegs =
        AbdomenLegs.addChild(
            "AbdomenLeftLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenLeftBackLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-3.0F, -2.0F, -5.0F));

    ModelPartData cube_r1 =
        AbdomenLeftBackLeg.addChild(
            "cube_r1",
            ModelPartBuilder.create()
                .uv(26, 22)
                .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(1.284F, 1.3323F, 0.1839F, -0.4939F, 0.4703F, -0.2393F));

    ModelPartData AbdomenLeftBackLegLower =
        AbdomenLeftBackLeg.addChild(
            "AbdomenLeftBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, 2.5F, 3.25F));

    ModelPartData cube_r2 =
        AbdomenLeftBackLegLower.addChild(
            "cube_r2",
            ModelPartBuilder.create()
                .uv(0, 10)
                .mirrored()
                .cuboid(-0.5F, -0.4183F, -0.4977F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.0F, 0.0F, 0.5F, 1.2217F, 0.0F, 0.0F));

    ModelPartData AbdomenLeftForwardLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, -2.0F, -8.0F));

    ModelPartData cube_r3 =
        AbdomenLeftForwardLeg.addChild(
            "cube_r3",
            ModelPartBuilder.create()
                .uv(0, 22)
                .cuboid(-0.648F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.284F, 0.5823F, 0.1839F, -0.4761F, 1.1883F, -0.0554F));

    ModelPartData AbdomenLeftForwardLegLower =
        AbdomenLeftForwardLeg.addChild(
            "AbdomenLeftForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.0F, 2.5F, 1.0F));

    ModelPartData cube_r4 =
        AbdomenLeftForwardLegLower.addChild(
            "cube_r4",
            ModelPartBuilder.create()
                .uv(0, 0)
                .cuboid(2.0F, -2.45F, 0.35F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-2.0F, 1.0F, 3.0F, 1.3188F, 0.3711F, 0.1699F));

    ModelPartData AbdomenRightLegs =
        AbdomenLegs.addChild(
            "AbdomenRightLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenRightForwardLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, -2.0F, -8.0F));

    ModelPartData cube_r5 =
        AbdomenRightForwardLeg.addChild(
            "cube_r5",
            ModelPartBuilder.create()
                .uv(0, 22)
                .mirrored()
                .cuboid(-1.352F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.284F, 0.5823F, 0.1839F, -0.4761F, -1.1883F, 0.0554F));

    ModelPartData AbdomenRightForwardLegLower =
        AbdomenRightForwardLeg.addChild(
            "AbdomenRightForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.0F, 2.5F, 1.0F));

    ModelPartData cube_r6 =
        AbdomenRightForwardLegLower.addChild(
            "cube_r6",
            ModelPartBuilder.create()
                .uv(0, 0)
                .mirrored()
                .cuboid(-0.5952F, -0.9478F, -0.6301F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.0F, 0.0F, 1.0F, 1.3188F, -0.3711F, -0.1699F));

    ModelPartData AbdomenRightBackLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(3.0F, -2.0F, -5.0F));

    ModelPartData cube_r7 =
        AbdomenRightBackLeg.addChild(
            "cube_r7",
            ModelPartBuilder.create()
                .uv(26, 22)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.284F, 1.3323F, 0.1839F, -0.4939F, -0.4703F, 0.2393F));

    ModelPartData AbdomenRightBackLegLower =
        AbdomenRightBackLeg.addChild(
            "AbdomenRightBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, 2.0F, 3.25F));

    ModelPartData cube_r8 =
        AbdomenRightBackLegLower.addChild(
            "cube_r8",
            ModelPartBuilder.create()
                .uv(0, 10)
                .cuboid(0.0F, -1.7F, 0.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.5F, 1.5F, 1.5F, 1.2217F, 0.0F, 0.0F));

    ModelPartData AbdomenMain =
        Abdomen.addChild(
            "AbdomenMain",
            ModelPartBuilder.create()
                .uv(20, 29)
                .cuboid(1.0F, -4.0F, -5.0F, 1.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .uv(0, 0)
                .cuboid(-3.0F, -4.0F, -5.0F, 4.0F, 1.0F, 7.0F, new Dilation(0.0F))
                .uv(20, 29)
                .mirrored()
                .cuboid(-4.0F, -4.0F, -5.0F, 1.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(14, 10)
                .cuboid(-4.0F, -3.0F, -5.0F, 6.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .uv(33, 34)
                .cuboid(-3.0F, -3.0F, 0.0F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.pivot(1.0F, 5.0F, 6.0F));

    ModelPartData WingBone =
        Abdomen.addChild(
            "WingBone", ModelPartBuilder.create(), ModelTransform.pivot(1.0F, -0.5F, 1.0F));

    ModelPartData Elytra =
        WingBone.addChild(
            "Elytra", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, 0.5F, 0.0F));

    ModelPartData LeftElytron =
        Elytra.addChild(
            "LeftElytron",
            ModelPartBuilder.create()
                .uv(14, 17)
                .mirrored()
                .cuboid(1.0F, 1.5F, 0.0F, 1.0F, 1.0F, 7.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(0, 10)
                .mirrored()
                .cuboid(-2.0F, 0.5F, 0.0F, 3.0F, 2.0F, 7.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(37, 12)
                .mirrored()
                .cuboid(-2.0F, 1.5F, 7.0F, 3.0F, 1.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.pivot(2.0F, -0.5F, 0.0F));

    ModelPartData RightElytron =
        Elytra.addChild(
            "RightElytron",
            ModelPartBuilder.create()
                .uv(37, 12)
                .cuboid(-1.0F, 1.5F, 7.0F, 3.0F, 1.0F, 1.0F, new Dilation(0.0F))
                .uv(14, 17)
                .cuboid(-2.0F, 1.5F, 0.0F, 1.0F, 1.0F, 7.0F, new Dilation(0.0F))
                .uv(0, 10)
                .cuboid(-1.0F, 0.5F, 0.0F, 3.0F, 2.0F, 7.0F, new Dilation(0.0F)),
            ModelTransform.pivot(-2.0F, -0.5F, 0.0F));

    ModelPartData Wings =
        WingBone.addChild(
            "Wings", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, 6.0F, 1.0F));

    ModelPartData RightWing =
        Wings.addChild(
            "RightWing", ModelPartBuilder.create(), ModelTransform.pivot(-2.0F, -4.75F, 0.0F));

    ModelPartData cube_r9 =
        RightWing.addChild(
            "cube_r9",
            ModelPartBuilder.create()
                .uv(0, 56)
                .mirrored()
                .cuboid(-1.25F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.0F, 1.25F, 0.0F, 0.0F, 0.0F, -0.2618F));

    ModelPartData RightWingLower =
        RightWing.addChild(
            "RightWingLower", ModelPartBuilder.create(), ModelTransform.pivot(1.0F, -0.5F, 5.75F));

    ModelPartData cube_r10 =
        RightWingLower.addChild(
            "cube_r10",
            ModelPartBuilder.create()
                .uv(10, 56)
                .mirrored()
                .cuboid(-1.25F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.0F, 1.75F, -5.75F, 0.0F, 0.0F, -0.2618F));

    ModelPartData LeftWing =
        Wings.addChild(
            "LeftWing", ModelPartBuilder.create(), ModelTransform.pivot(2.0F, -5.0F, 0.0F));

    ModelPartData cube_r11 =
        LeftWing.addChild(
            "cube_r11",
            ModelPartBuilder.create()
                .uv(0, 56)
                .cuboid(-3.75F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
            ModelTransform.of(0.0F, 1.5F, 0.0F, 0.0F, 0.0F, 0.2618F));

    ModelPartData LeftWingLower =
        LeftWing.addChild(
            "LeftWingLower", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, -0.25F, 5.75F));

    ModelPartData cube_r12 =
        LeftWingLower.addChild(
            "cube_r12",
            ModelPartBuilder.create()
                .uv(10, 56)
                .cuboid(-3.75F, -1.0F, -1.25F, 5.0F, 1.0F, 7.0F, new Dilation(0.0F)),
            ModelTransform.of(1.0F, 1.75F, -5.75F, 0.0F, 0.0F, 0.2618F));

    ModelPartData Thorax =
        Body.addChild(
            "Thorax", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -4.5F, -3.0F));

    ModelPartData ThoraxLegs =
        Thorax.addChild(
            "ThoraxLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.001F, 4.5F, 3.0436F));

    ModelPartData ThoraxLeftLeg =
        ThoraxLegs.addChild(
            "ThoraxLeftLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.716F, -2.8323F, -2.6839F));

    ModelPartData cube_r13 =
        ThoraxLeftLeg.addChild(
            "cube_r13",
            ModelPartBuilder.create()
                .uv(10, 27)
                .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.284F, 0.3323F, 0.1839F, 0.4939F, -0.4703F, -0.2393F));

    ModelPartData ThoraxLeftLegLower =
        ThoraxLeftLeg.addChild(
            "ThoraxLeftLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(1.5F, 1.5F, -3.3822F));

    ModelPartData cube_r14 =
        ThoraxLeftLegLower.addChild(
            "cube_r14",
            ModelPartBuilder.create()
                .uv(21, 37)
                .cuboid(-1.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    ModelPartData ThoraxRightLeg =
        ThoraxLegs.addChild(
            "ThoraxRightLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.7179F, -2.8323F, -2.6839F));

    ModelPartData cube_r15 =
        ThoraxRightLeg.addChild(
            "cube_r15",
            ModelPartBuilder.create()
                .uv(10, 27)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.284F, 0.3323F, 0.1839F, 0.4939F, 0.4703F, 0.2393F));

    ModelPartData ThoraxRightLegLower =
        ThoraxRightLeg.addChild(
            "ThoraxRightLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-1.5F, 1.5F, -3.3822F));

    ModelPartData cube_r16 =
        ThoraxRightLegLower.addChild(
            "cube_r16",
            ModelPartBuilder.create()
                .uv(21, 37)
                .mirrored()
                .cuboid(0.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    ModelPartData ThoraxMain =
        Thorax.addChild(
            "ThoraxMain",
            ModelPartBuilder.create()
                .uv(0, 37)
                .cuboid(-3.001F, -1.0F, -5.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .uv(0, 37)
                .mirrored()
                .cuboid(3.9971F, -1.0F, -5.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(28, 29)
                .cuboid(-1.5019F, -1.5F, -6.0F, 5.0F, 2.0F, 1.0F, new Dilation(0.0F))
                .uv(16, 0)
                .mirrored()
                .cuboid(-2.0019F, -2.0F, -5.0F, 6.0F, 3.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.pivot(-0.9981F, 1.0F, 4.0436F));

    ModelPartData ThoraxAttach =
        Thorax.addChild(
            "ThoraxAttach",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

    ModelPartData Head =
        Thorax.addChild(
            "Head",
            ModelPartBuilder.create()
                .uv(31, 7)
                .cuboid(-2.999F, -0.5F, -2.9564F, 6.0F, 2.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.pivot(0.0F, 0.0F, -1.0F));

    ModelPartData Pincers =
        Head.addChild(
            "Pincers", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.5F, -2.0F));

    ModelPartData PincerRight =
        Pincers.addChild(
            "PincerRight",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.499F, -0.15F, 0.5436F));

    ModelPartData cube_r17 =
        PincerRight.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(57, 60)
                .cuboid(1.0F, 1.2F, -1.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.25F, -2.0F, -6.0F, -0.038F, -0.4368F, 0.1927F));

    ModelPartData head_r1 =
        PincerRight.addChild(
            "head_r1",
            ModelPartBuilder.create()
                .uv(43, 58)
                .cuboid(-1.0F, -1.0F, -11.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(3.5F, 2.4F, 5.0F, -0.2182F, 0.48F, 0.0F));

    ModelPartData PincerLeft =
        Pincers.addChild(
            "PincerLeft", ModelPartBuilder.create(), ModelTransform.pivot(2.499F, -0.15F, 0.5436F));

    ModelPartData cube_r18 =
        PincerLeft.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(57, 60)
                .mirrored()
                .cuboid(-2.0F, 1.2F, -1.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(1.25F, -2.0F, -6.0F, -0.038F, 0.4368F, -0.1927F));

    ModelPartData head_r2 =
        PincerLeft.addChild(
            "head_r2",
            ModelPartBuilder.create()
                .uv(43, 58)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -11.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.5F, 2.4F, 5.0F, -0.2182F, -0.48F, 0.0F));

    ModelPartData ThoraxMain2 =
        Thorax.addChild(
            "ThoraxMain2", ModelPartBuilder.create(), ModelTransform.pivot(0.999F, 1.0F, 4.0436F));

    ModelPartData ThoraxMain3 =
        Thorax.addChild(
            "ThoraxMain3", ModelPartBuilder.create(), ModelTransform.pivot(0.999F, 1.0F, 4.0436F));

    ModelPartData ThoraxMain4 =
        Thorax.addChild(
            "ThoraxMain4", ModelPartBuilder.create(), ModelTransform.pivot(0.999F, 1.0F, 4.0436F));

    ModelPartData ThoraxMain5 =
        Thorax.addChild(
            "ThoraxMain5", ModelPartBuilder.create(), ModelTransform.pivot(0.9981F, 1.0F, 4.0436F));
    return modelPartData;
  }

  public static ModelPartData segment_beetle(ModelPartData modelPartData) {
    ModelPartData Body =
        modelPartData.addChild(
            "Body", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

    ModelPartData Abdomen =
        Body.addChild(
            "Abdomen", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -5.5F, -2.0F));

    ModelPartData AbdomenLegs =
        Abdomen.addChild(
            "AbdomenLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 5.5F, 2.0F));

    ModelPartData AbdomenLeftLegs =
        AbdomenLegs.addChild(
            "AbdomenLeftLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenLeftBackLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-3.0F, -2.0F, -5.0F));

    ModelPartData cube_r1 =
        AbdomenLeftBackLeg.addChild(
            "cube_r1",
            ModelPartBuilder.create()
                .uv(26, 22)
                .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(1.284F, 1.3323F, 0.1839F, -0.4939F, 0.4703F, -0.2393F));

    ModelPartData AbdomenLeftBackLegLower =
        AbdomenLeftBackLeg.addChild(
            "AbdomenLeftBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, 2.5F, 3.25F));

    ModelPartData cube_r2 =
        AbdomenLeftBackLegLower.addChild(
            "cube_r2",
            ModelPartBuilder.create()
                .uv(0, 10)
                .mirrored()
                .cuboid(-0.5F, -0.4183F, -0.4977F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.0F, 0.0F, 0.5F, 1.2217F, 0.0F, 0.0F));

    ModelPartData AbdomenLeftForwardLeg =
        AbdomenLeftLegs.addChild(
            "AbdomenLeftForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, -2.0F, -8.0F));

    ModelPartData cube_r3 =
        AbdomenLeftForwardLeg.addChild(
            "cube_r3",
            ModelPartBuilder.create()
                .uv(0, 22)
                .cuboid(-0.648F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.284F, 0.5823F, 0.1839F, -0.4761F, 1.1883F, -0.0554F));

    ModelPartData AbdomenLeftForwardLegLower =
        AbdomenLeftForwardLeg.addChild(
            "AbdomenLeftForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(4.0F, 2.5F, 1.0F));

    ModelPartData cube_r4 =
        AbdomenLeftForwardLegLower.addChild(
            "cube_r4",
            ModelPartBuilder.create()
                .uv(0, 0)
                .cuboid(2.0F, -2.45F, 0.35F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-2.0F, 1.0F, 3.0F, 1.3188F, 0.3711F, 0.1699F));

    ModelPartData AbdomenRightLegs =
        AbdomenLegs.addChild(
            "AbdomenRightLegs",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.716F, -1.8323F, 8.3161F));

    ModelPartData AbdomenRightForwardLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightForwardLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.5F, -2.0F, -8.0F));

    ModelPartData cube_r5 =
        AbdomenRightForwardLeg.addChild(
            "cube_r5",
            ModelPartBuilder.create()
                .uv(0, 22)
                .mirrored()
                .cuboid(-1.352F, -0.4643F, -0.4894F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.284F, 0.5823F, 0.1839F, -0.4761F, -1.1883F, 0.0554F));

    ModelPartData AbdomenRightForwardLegLower =
        AbdomenRightForwardLeg.addChild(
            "AbdomenRightForwardLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-4.0F, 2.5F, 1.0F));

    ModelPartData cube_r6 =
        AbdomenRightForwardLegLower.addChild(
            "cube_r6",
            ModelPartBuilder.create()
                .uv(0, 0)
                .mirrored()
                .cuboid(-0.5952F, -0.9478F, -0.6301F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(0.0F, 0.0F, 1.0F, 1.3188F, -0.3711F, -0.1699F));

    ModelPartData AbdomenRightBackLeg =
        AbdomenRightLegs.addChild(
            "AbdomenRightBackLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(3.0F, -2.0F, -5.0F));

    ModelPartData cube_r7 =
        AbdomenRightBackLeg.addChild(
            "cube_r7",
            ModelPartBuilder.create()
                .uv(26, 22)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-1.284F, 1.3323F, 0.1839F, -0.4939F, -0.4703F, 0.2393F));

    ModelPartData AbdomenRightBackLegLower =
        AbdomenRightBackLeg.addChild(
            "AbdomenRightBackLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.5F, 2.0F, 3.25F));

    ModelPartData cube_r8 =
        AbdomenRightBackLegLower.addChild(
            "cube_r8",
            ModelPartBuilder.create()
                .uv(0, 10)
                .cuboid(0.0F, -1.7F, 0.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.5F, 1.5F, 1.5F, 1.2217F, 0.0F, 0.0F));

    ModelPartData AbdomenMain =
        Abdomen.addChild(
            "AbdomenMain",
            ModelPartBuilder.create()
                .uv(0, 0)
                .cuboid(-3.0F, -5.0F, -5.0F, 4.0F, 2.0F, 7.0F, new Dilation(0.0F))
                .uv(14, 10)
                .cuboid(-4.0F, -3.0F, -5.0F, 6.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .uv(33, 34)
                .cuboid(-3.0F, -3.0F, 0.0F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.pivot(1.0F, 5.0F, 6.0F));

    ModelPartData WingBone =
        Abdomen.addChild(
            "WingBone", ModelPartBuilder.create(), ModelTransform.pivot(1.0F, -0.5F, 1.0F));

    ModelPartData Elytra =
        WingBone.addChild(
            "Elytra", ModelPartBuilder.create(), ModelTransform.pivot(-1.0F, -1.0F, 0.0F));

    ModelPartData LeftElytron =
        Elytra.addChild(
            "LeftElytron",
            ModelPartBuilder.create()
                .uv(15, 18)
                .mirrored()
                .cuboid(1.0F, 0.5F, 1.0F, 1.0F, 2.0F, 6.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(0, 10)
                .mirrored()
                .cuboid(-2.0F, -0.5F, 0.0F, 3.0F, 3.0F, 7.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(36, 22)
                .mirrored()
                .cuboid(-2.0F, -1.5F, 1.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(37, 12)
                .mirrored()
                .cuboid(-2.0F, 0.5F, 7.0F, 3.0F, 2.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.pivot(2.0F, 1.0F, 0.0F));

    ModelPartData RightElytron =
        Elytra.addChild(
            "RightElytron",
            ModelPartBuilder.create()
                .uv(0, 10)
                .cuboid(-1.0F, -0.5F, 0.0F, 3.0F, 3.0F, 7.0F, new Dilation(0.0F))
                .uv(37, 12)
                .cuboid(-1.0F, 0.5F, 7.0F, 3.0F, 2.0F, 1.0F, new Dilation(0.0F))
                .uv(15, 18)
                .cuboid(-2.0F, 0.5F, 1.0F, 1.0F, 2.0F, 6.0F, new Dilation(0.0F))
                .uv(36, 22)
                .cuboid(0.0F, -1.5F, 1.0F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.pivot(-2.0F, 1.0F, 0.0F));

    ModelPartData Thorax =
        Body.addChild(
            "Thorax", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -4.5F, -3.0F));

    ModelPartData ThoraxLegs =
        Thorax.addChild(
            "ThoraxLegs", ModelPartBuilder.create(), ModelTransform.pivot(0.001F, 4.5F, 3.0436F));

    ModelPartData ThoraxLeftLeg =
        ThoraxLegs.addChild(
            "ThoraxLeftLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.716F, -2.8323F, -2.6839F));

    ModelPartData cube_r9 =
        ThoraxLeftLeg.addChild(
            "cube_r9",
            ModelPartBuilder.create()
                .uv(10, 27)
                .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)),
            ModelTransform.of(0.284F, 0.3323F, 0.1839F, 0.4939F, -0.4703F, -0.2393F));

    ModelPartData ThoraxLeftLegLower =
        ThoraxLeftLeg.addChild(
            "ThoraxLeftLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(1.5F, 1.5F, -3.3822F));

    ModelPartData cube_r10 =
        ThoraxLeftLegLower.addChild(
            "cube_r10",
            ModelPartBuilder.create()
                .uv(21, 37)
                .cuboid(-1.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    ModelPartData ThoraxRightLeg =
        ThoraxLegs.addChild(
            "ThoraxRightLeg",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-2.7179F, -2.8323F, -2.6839F));

    ModelPartData cube_r11 =
        ThoraxRightLeg.addChild(
            "cube_r11",
            ModelPartBuilder.create()
                .uv(10, 27)
                .mirrored()
                .cuboid(-1.0F, -1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.284F, 0.3323F, 0.1839F, 0.4939F, 0.4703F, 0.2393F));

    ModelPartData ThoraxRightLegLower =
        ThoraxRightLeg.addChild(
            "ThoraxRightLegLower",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-1.5F, 1.5F, -3.3822F));

    ModelPartData cube_r12 =
        ThoraxRightLegLower.addChild(
            "cube_r12",
            ModelPartBuilder.create()
                .uv(21, 37)
                .mirrored()
                .cuboid(0.0F, -1.7F, -1.1F, 1.0F, 5.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-0.5F, 1.0F, -1.0F, -1.2654F, 0.0F, 0.0F));

    ModelPartData ThoraxMain =
        Thorax.addChild(
            "ThoraxMain",
            ModelPartBuilder.create()
                .uv(16, 0)
                .cuboid(-2.0F, -3.0F, -5.0F, 6.0F, 3.0F, 3.0F, new Dilation(0.0F))
                .uv(35, 0)
                .cuboid(-1.0F, -4.0F, -5.0F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .uv(0, 37)
                .cuboid(-3.0019F, -2.0F, -5.0F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F))
                .uv(0, 37)
                .mirrored()
                .cuboid(4.0F, -2.0F, -5.0F, 1.0F, 2.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(28, 29)
                .cuboid(-2.0F, -2.0F, -6.0F, 6.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .uv(24, 17)
                .cuboid(-2.0F, 0.0F, -5.0F, 6.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .uv(47, 55)
                .cuboid(-1.0F, -3.0F, -6.0F, 4.0F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.pivot(-0.999F, 1.0F, 4.0436F));

    ModelPartData ThoraxAttach =
        Thorax.addChild(
            "ThoraxAttach",
            ModelPartBuilder.create(),
            ModelTransform.pivot(-0.249F, -1.75F, -2.9564F));

    ModelPartData Head =
        Thorax.addChild(
            "Head",
            ModelPartBuilder.create()
                .uv(32, 7)
                .cuboid(-1.999F, -1.0F, -2.9564F, 4.0F, 2.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.pivot(0.0F, 1.0F, -1.0F));

    ModelPartData Horn =
        Head.addChild("Horn", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.5F, -2.0F));
    return modelPartData;
  }
}
