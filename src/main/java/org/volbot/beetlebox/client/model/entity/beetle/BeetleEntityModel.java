package org.volbot.beetlebox.client.model.entity.beetle;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelData;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;

@Environment(EnvType.CLIENT)
public class BeetleEntityModel<T extends BeetleEntity> extends EntityModel<T> {

  private final ModelPart body;
  private final ModelPart abdomen;
  private final ModelPart abdomenlegs;
  private final ModelPart abdomenleftlegs;
  private final ModelPart abdomenleftbackleg;
  private final ModelPart abdomenleftbackleglower;
  private final ModelPart abdomenleftforwardleg;
  private final ModelPart abdomenleftforwardleglower;
  private final ModelPart abdomenrightlegs;
  private final ModelPart abdomenrightforwardleg;
  private final ModelPart abdomenrightforwardleglower;
  private final ModelPart abdomenrightbackleg;
  private final ModelPart abdomenrightbackleglower;
  private final ModelPart abdomenmain;
  private final ModelPart wingbone;
  private final ModelPart elytra;
  private final ModelPart leftelytron;
  private final ModelPart rightelytron;
  private final ModelPart wings;
  private final ModelPart rightwing;
  private final ModelPart rightwinglower;
  private final ModelPart leftwing;
  private final ModelPart leftwinglower;
  private final ModelPart thorax;
  private final ModelPart thoraxlegs;
  private final ModelPart thoraxleftleg;
  private final ModelPart thoraxleftleglower;
  private final ModelPart thoraxrightleg;
  private final ModelPart thoraxrightleglower;
  private final ModelPart thoraxmain;
  private final ModelPart thoraxattach;
  private final ModelPart head;
  private final ModelPart horn;
  private final ModelPart pincers;
  private final ModelPart pincerleft;
  private final ModelPart pincerright;

  private static float STANDARD_SCALE = 0.8f;
  private static float ANIM_RATE = 3.5f;
  private static float SWING_MOD = 0.3f;
  private float scale = STANDARD_SCALE;
  private float head_turn_scale = 1.0f;

  public BeetleEntityModel(ModelPart root, float head_turn_scale) {
    this.head_turn_scale = head_turn_scale;
    // collect body parts for animations
    // grouped by common parent
    this.body = root.getChild("Body");

    this.abdomen = body.getChild("Abdomen");
    this.thorax = body.getChild("Thorax");

    this.head = thorax.getChild("Head");
    this.thoraxmain = thorax.getChild("ThoraxMain");
    this.thoraxattach = thorax.getChild("ThoraxAttach");
    this.thoraxlegs = thorax.getChild("ThoraxLegs");

    this.thoraxleftleg = thoraxlegs.getChild("ThoraxLeftLeg");
    this.thoraxrightleg = thoraxlegs.getChild("ThoraxRightLeg");

    this.thoraxleftleglower = thoraxleftleg.getChild("ThoraxLeftLegLower");

    this.thoraxrightleglower = thoraxrightleg.getChild("ThoraxRightLegLower");

    if (head.hasChild("Horn")) {
      this.horn = head.getChild("Horn");
    } else {
      this.horn = null;
    }

    if (head.hasChild("Pincers")) {
      this.pincers = head.getChild("Pincers");
      this.pincerleft = pincers.getChild("PincerLeft");
      this.pincerright = pincers.getChild("PincerRight");
    } else {
      this.pincers = null;
      this.pincerleft = null;
      this.pincerright = null;
    }

    this.abdomenmain = abdomen.getChild("AbdomenMain");
    this.abdomenlegs = abdomen.getChild("AbdomenLegs");
    this.wingbone = abdomen.getChild("WingBone");

    this.elytra = wingbone.getChild("Elytra");

    this.leftelytron = elytra.getChild("LeftElytron");
    this.rightelytron = elytra.getChild("RightElytron");

    if (wingbone.hasChild("Wings")) {
      this.wings = wingbone.getChild("Wings");

      this.rightwing = wings.getChild("RightWing");
      this.leftwing = wings.getChild("LeftWing");

      this.leftwinglower = leftwing.getChild("LeftWingLower");

      this.rightwinglower = rightwing.getChild("RightWingLower");
    } else {
      this.wings = null;
      this.rightwing = null;
      this.leftwing = null;
      this.rightwinglower = null;
      this.leftwinglower = null;
    }

    this.abdomenleftlegs = abdomenlegs.getChild("AbdomenLeftLegs");
    this.abdomenrightlegs = abdomenlegs.getChild("AbdomenRightLegs");

    this.abdomenleftforwardleg = abdomenleftlegs.getChild("AbdomenLeftForwardLeg");
    this.abdomenleftbackleg = abdomenleftlegs.getChild("AbdomenLeftBackLeg");

    this.abdomenrightforwardleg = abdomenrightlegs.getChild("AbdomenRightForwardLeg");
    this.abdomenrightbackleg = abdomenrightlegs.getChild("AbdomenRightBackLeg");

    this.abdomenleftforwardleglower = abdomenleftforwardleg.getChild("AbdomenLeftForwardLegLower");

    this.abdomenleftbackleglower = abdomenleftbackleg.getChild("AbdomenLeftBackLegLower");

    this.abdomenrightforwardleglower =
        abdomenrightforwardleg.getChild("AbdomenRightForwardLegLower");

    this.abdomenrightbackleglower = abdomenrightbackleg.getChild("AbdomenRightBackLegLower");
  }

  @Override
  public void setAngles(
      BeetleEntity entity,
      float limbSwing,
      float limbSwingAmount,
      float ageInTicks,
      float netHeadYaw,
      float headPitch) {

    // set beetle size
    int size = entity.getStat(BeetleStat.BB_SIZE);
    this.scale = STANDARD_SCALE * (float) size / 10f;

    boolean grounded = entity.isOnGround();
    boolean flying = entity.isFlying();
    float attackTime = entity.attackTime;
    int timeFlying = entity.timeFlying;
    int speedMult = entity.getStat(BeetleStat.BB_SPEED);

    pose_air(grounded, flying);
    if (grounded) {
      anim_walk(limbSwing * 1.2f, limbSwingAmount, speedMult);
      if (!flying) {
        this.elytra.pitch = 0f;
        this.leftelytron.pitch = 0f;
        this.leftelytron.yaw = 0f;
        this.leftelytron.roll = 0f;
        this.rightelytron.pitch = 0f;
        this.rightelytron.yaw = 0f;
        this.rightelytron.roll = 0f;
        if (this.wings != null) {
          this.leftwinglower.yaw = 0f;
          this.leftwinglower.roll = 0f;
          this.rightwinglower.yaw = 0f;
          this.rightwinglower.roll = 0f;
          this.leftwing.pitch = 0f;
          this.leftwing.yaw = 0f;
          this.leftwing.roll = 0f;
          this.rightwing.pitch = 0f;
          this.rightwing.yaw = 0f;
          this.rightwing.roll = 0f;
        }
      }
    }

    if (entity.canFly()) {
      if (flying) {
        if (timeFlying < 5) {
          anim_toggle_wingState(ageInTicks, timeFlying, 5, false);
        } else {
          anim_fly(ageInTicks, timeFlying);
        }
      } else {
        if (timeFlying < 5) {
          anim_toggle_wingState(ageInTicks, timeFlying, 5, true);
        } else {
          this.leftwinglower.yaw = 0f;
          this.leftwinglower.roll = 0f;
          this.rightwinglower.yaw = 0f;
          this.rightwinglower.roll = 0f;
          this.leftwing.pitch = 0f;
          this.leftwing.yaw = 0f;
          this.leftwing.roll = 0f;
          this.rightwing.pitch = 0f;
          this.rightwing.yaw = 0f;
          this.rightwing.roll = 0f;
          if (grounded) {
            this.elytra.pitch = 0f;
            this.leftelytron.pitch = 0f;
            this.leftelytron.yaw = 0f;
            this.leftelytron.roll = 0f;
            this.rightelytron.pitch = 0f;
            this.rightelytron.yaw = 0f;
            this.rightelytron.roll = 0f;
          }
        }
      }
    }

    float rad = MathHelper.PI / 180;
    // set head angles
    this.head.yaw = netHeadYaw * ((float) Math.PI / 260) * head_turn_scale;
    this.head.pitch = headPitch * ((float) Math.PI / 180) * head_turn_scale;

    if (entity.isTargeting()) {
      this.head.pitch += rad * 15f;
    }
    if (attackTime >= 0) {
      anim_attack(ageInTicks, attackTime);
    }
  }

  public void pose_air(boolean invert, boolean flying) {
    float rad = MathHelper.PI / 180;

    float animProgress = invert ? 0f : flying ? 1f : 0.5f;

    this.body.pitch = animProgress * rad * -22.5f;
    this.thorax.pitch = animProgress * rad * -12.5f;
    this.abdomen.pitch = animProgress * rad * -22.5f;

    this.thoraxrightleg.pitch = animProgress * rad * 30f;
    this.thoraxrightleg.yaw = animProgress * rad * 25f;
    this.thoraxleftleg.pitch = animProgress * rad * 30f;
    this.thoraxleftleg.yaw = animProgress * rad * -25f;

    this.abdomenrightforwardleglower.pitch = animProgress * rad * 7.5f;
    this.abdomenrightforwardleglower.yaw = animProgress * rad * -80f;
    this.abdomenrightforwardleglower.roll = animProgress * rad * -50f;
    this.abdomenleftforwardleglower.pitch = animProgress * rad * 7.5f;
    this.abdomenleftforwardleglower.yaw = animProgress * rad * 80f;
    this.abdomenleftforwardleglower.roll = animProgress * rad * 50f;

    this.abdomenrightforwardleg.yaw = animProgress * rad * -25f;
    this.abdomenrightforwardleg.roll = animProgress * rad * 55f;
    this.abdomenleftforwardleg.yaw = animProgress * rad * 25f;
    this.abdomenleftforwardleg.roll = animProgress * rad * -55f;
  }

  public boolean anim_toggle_wingState(
      float ageInTicks, int progress, int totalTicks, boolean invert) {
    float animProgress = ((float) progress + (ageInTicks % 1)) / (float) totalTicks;
    if (animProgress > 1f) {
      return true;
    }
    if (invert) animProgress = 1f - animProgress;
    float rad = MathHelper.PI / 180f;

    this.elytra.pitch = animProgress * rad * 45f;
    this.leftelytron.pitch = animProgress * rad * -51f;
    this.leftelytron.yaw = animProgress * rad * 24f;
    this.leftelytron.roll = animProgress * rad * -101f;
    this.rightelytron.pitch = animProgress * rad * -51f;
    this.rightelytron.yaw = animProgress * rad * -24f;
    this.rightelytron.roll = animProgress * rad * 101f;

    this.rightwing.yaw = animProgress * rad * -87.5f;
    this.leftwing.yaw = animProgress * rad * 87.5f;

    this.leftwinglower.yaw = animProgress * rad * 180f;
    this.leftwinglower.roll = animProgress * rad * 30f;
    this.rightwinglower.yaw = animProgress * rad * -180f;
    this.rightwinglower.roll = animProgress * rad * -30f;

    return false;
  }

  public void anim_attack(float ageInTicks, float attackTime) {
    float progress = 5f - attackTime;
    float animProgress = ((float) progress + (ageInTicks % 1)) / 5f;
    if (animProgress > 1f) {
      return;
    }
    float rad = MathHelper.PI / 180;
    float sine = MathHelper.sin(-animProgress * (MathHelper.PI));
    this.head.pitch += (sine * 60f - 10f) * rad * head_turn_scale;
  }

  public void anim_fly(float ageInTicks, int timeFlying) {

    float rad = MathHelper.PI / 180;

    this.elytra.pitch = rad * 45f;

    this.leftelytron.pitch = rad * -51f;
    this.leftelytron.yaw = rad * 24f;
    this.leftelytron.roll = rad * -101f;
    this.rightelytron.pitch = rad * -51f;
    this.rightelytron.yaw = rad * -24f;
    this.rightelytron.roll = rad * 101f;

    this.leftwinglower.yaw = rad * 180f;
    this.leftwinglower.roll = rad * 30f;
    this.rightwinglower.yaw = rad * -180f;
    this.rightwinglower.roll = rad * -30f;

    this.rightwing.pitch = rad * 10f;
    this.rightwing.yaw = rad * -87.5f;
    this.rightwing.roll = 0f;
    this.leftwing.pitch = rad * 10f;
    this.leftwing.yaw = rad * 87.5f;
    this.leftwing.roll = 0f;

    float sine = MathHelper.sin(ageInTicks * ANIM_RATE);

    this.rightwing.pitch += sine * rad * 40f;
    this.rightwing.yaw += sine * rad * 50f;
    this.rightwing.roll += sine * rad * 25f;
    this.leftwing.pitch += sine * rad * 40f;
    this.leftwing.yaw += sine * rad * -50f;
    this.leftwing.roll += sine * rad * -25f;
  }

  public void anim_walk(float limbSwing, float limbSwingAmount, float speedMult) {
    this.abdomenrightbackleg.yaw = 0.1f;
    this.abdomenrightbackleg.roll = -0.2f;
    this.abdomenrightbackleg.pivotZ = -5f;
    this.abdomenleftbackleg.yaw = -0.1f;
    this.abdomenleftbackleg.roll = 0.2f;
    this.abdomenleftbackleg.pivotZ = -5f;

    this.abdomenrightforwardleg.yaw = 0.2f;
    this.abdomenrightforwardleg.roll = 0f;
    this.abdomenrightforwardleg.pivotZ = -8f;
    this.abdomenleftforwardleg.yaw = -0.2f;
    this.abdomenleftforwardleg.roll = 0f;
    this.abdomenleftforwardleg.pivotZ = -8f;
    this.abdomenrightforwardleglower.pitch = -0.4f;
    this.abdomenrightforwardleglower.roll = 0f;
    this.abdomenrightforwardleglower.yaw = -0.1f;
    this.abdomenleftforwardleglower.pitch = -0.4f;
    this.abdomenleftforwardleglower.roll = 0f;
    this.abdomenleftforwardleglower.yaw = 0.1f;

    this.thoraxrightleg.pitch = 0.1f;
    this.thoraxrightleg.yaw = 1f;
    this.thoraxrightleg.roll = 0f;
    this.thoraxleftleg.pitch = 0.1f;
    this.thoraxleftleg.yaw = -1f;
    this.thoraxleftleg.roll = 0f;
    this.thoraxrightleglower.yaw = -1.1f;
    this.thoraxleftleglower.yaw = 1.1f;
    this.thoraxrightleglower.pitch = 0.2f;
    this.thoraxleftleglower.pitch = 0.2f;
    this.thoraxrightleglower.roll = 0f;
    this.thoraxleftleglower.roll = 0f;

    float[] limb1 = armAngle(limbSwing, 0);
    float[] limb2 = armAngle(limbSwing, 3);
    float[] limb3 = armAngle(limbSwing, 1);
    float[] limb4 = armAngle(limbSwing, 4);
    float[] limb5 = armAngle(limbSwing, 2);
    float[] limb6 = armAngle(limbSwing, 5);

    SWING_MOD = 0.3f;
    float newlimbSwingAmount = limbSwingAmount / ((speedMult / 50));

    this.abdomenrightbackleg.pivotZ += -limb1[0] * SWING_MOD * 1.0f * newlimbSwingAmount;
    this.abdomenrightbackleg.yaw += -limb1[0] * SWING_MOD * 0.3f * newlimbSwingAmount;
    this.abdomenrightbackleg.roll += limb1[1] * SWING_MOD * 0.6f * newlimbSwingAmount;
    this.abdomenleftbackleg.pivotZ += -limb2[0] * SWING_MOD * 1.0f * newlimbSwingAmount;
    this.abdomenleftbackleg.yaw += limb2[0] * SWING_MOD * 0.3f * newlimbSwingAmount;
    this.abdomenleftbackleg.roll += -limb2[1] * SWING_MOD * 0.6f * newlimbSwingAmount;

    this.abdomenrightforwardleg.pivotZ += -limb3[0] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenrightforwardleg.yaw += -limb3[0] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenrightforwardleg.roll += limb3[1] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenleftforwardleg.pivotZ += -limb4[0] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenleftforwardleg.yaw += limb4[0] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenleftforwardleg.roll += -limb4[1] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.abdomenrightforwardleglower.pitch += limb3[1] * SWING_MOD * 0.4f * newlimbSwingAmount;
    this.abdomenrightforwardleglower.roll += -limb3[1] * SWING_MOD * newlimbSwingAmount;
    this.abdomenleftforwardleglower.pitch += limb4[1] * SWING_MOD * 0.4f * newlimbSwingAmount;
    this.abdomenleftforwardleglower.roll += limb4[1] * SWING_MOD * newlimbSwingAmount;

    this.thoraxrightleg.pitch += -limb5[1] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.thoraxrightleg.yaw += -limb5[0] * SWING_MOD * 1f * newlimbSwingAmount;
    this.thoraxleftleg.pitch += -limb6[1] * SWING_MOD * 0.7f * newlimbSwingAmount;
    this.thoraxleftleg.yaw += limb6[0] * SWING_MOD * 1f * newlimbSwingAmount;
    // this.thoraxrightleglower.pitch += -limb5[1] * SWING_MOD * 0.1f * newlimbSwingAmount;
    this.thoraxrightleglower.yaw += limb5[0] * SWING_MOD * 1.1f * newlimbSwingAmount - 0.2f;
    // this.thoraxleftleglower.pitch += -limb6[1] * SWING_MOD * 0.1f * newlimbSwingAmount;
    this.thoraxleftleglower.yaw += -limb6[0] * SWING_MOD * 1.1f * newlimbSwingAmount + 0.2f;
  }

  public float[] armAngle(float limbSwing, int index) {
    float total = 10f;
    float offset1 = index;
    float offset2 = total / 3;
    float one = Math.abs(((limbSwing * ANIM_RATE + offset1) % total) - (total / 2));
    float two =
        Math.max(
            Math.abs(((limbSwing * ANIM_RATE + offset1 + offset2) % total) - (total / 2)) - offset2,
            0);
    return new float[] {one, two};
  }

  @Override
  public void render(
      MatrixStack matrices,
      VertexConsumer vertices,
      int light,
      int overlay,
      float red,
      float green,
      float blue,
      float alpha) {
    matrices.push();
    matrices.translate(0.0F, 1.4f, 0.0F);
    matrices.scale(scale, scale, scale);
    matrices.translate(0.0F, -0.025F * (scale - 1), 0.0F);
    this.body.render(matrices, vertices, light, overlay, red, green, blue, alpha);
    matrices.pop();
  }

  public static TexturedModelData getTexturedModelData(BeetleType beetle_type) {
    ModelData modelData = new ModelData();
    ModelPartData modelPartData = modelData.getRoot();

    BeetleEntityModels.get_model(modelPartData, beetle_type);

    return TexturedModelData.of(modelData, 64, 64);
  }
}
