package org.volbot.beetlebox.client.model.armor;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.ModelData;
import net.minecraft.client.model.ModelPartBuilder;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.ModelTransform;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.entity.model.ArmorEntityModel;
import net.minecraft.client.render.entity.model.EntityModelPartNames;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.entity.LivingEntity;

@Environment(EnvType.CLIENT)
public class BeetlepackModel<T extends LivingEntity> extends ArmorEntityModel<T> {

  public BeetlepackModel() {
    super(getTexturedModelData().createModel());
  }

  public static TexturedModelData getTexturedModelData() {

    ModelData modelData = PlayerEntityModel.getModelData(new Dilation(1F), 0);
    ModelPartData modelPartData = modelData.getRoot();

    ModelPartData body =
        modelPartData.addChild(
            EntityModelPartNames.BODY,
            ModelPartBuilder.create()
                .uv(48, 48)
                .cuboid(-4.0f, 0.0f, -2.0f, 8.0f, 12.0f, 4.0f, new Dilation(0f)),
            ModelTransform.pivot(0.0f, 0.0f + 0f, 0.0f));

    ModelPartData backpack =
        body.addChild(
            "backpack", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 11.0F, 1.0F));

    ModelPartData backpackcenter =
        backpack.addChild(
            "backpackcenter",
            ModelPartBuilder.create()
                .uv(20, 13)
                .cuboid(-1.7166F, 8.193F, -3.2173F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .uv(22, 4)
                .cuboid(-0.2166F, 18.4133F, -3.9664F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.pivot(-0.2834F, -19.6838F, 3.8234F));

    ModelPartData cube_r1 =
        backpackcenter.addChild(
            "cube_r1",
            ModelPartBuilder.create()
                .uv(8, 25)
                .cuboid(0.0F, 0.0F, -2.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.2166F, 17.9298F, 0.7067F, 0.2618F, 0.0F, 0.0F));

    ModelPartData cube_r2 =
        backpackcenter.addChild(
            "cube_r2",
            ModelPartBuilder.create()
                .uv(33, 0)
                .cuboid(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.2166F, 18.1751F, 1.0444F, 1.0908F, 0.0F, 0.0F));

    ModelPartData cube_r3 =
        backpackcenter.addChild(
            "cube_r3",
            ModelPartBuilder.create()
                .uv(32, 5)
                .cuboid(-0.5687F, -0.7559F, -0.7776F, 1.2F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.252F, 10.1144F, 1.7017F, 1.2287F, 0.0F, 0.0F));

    ModelPartData cube_r4 =
        backpackcenter.addChild(
            "cube_r4",
            ModelPartBuilder.create()
                .uv(33, 13)
                .cuboid(-0.4962F, -0.0312F, -1.0206F, 1.0F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.2796F, 16.7809F, 2.3968F, 0.0175F, 0.0F, 0.0F));

    ModelPartData cube_r5 =
        backpackcenter.addChild(
            "cube_r5",
            ModelPartBuilder.create()
                .uv(0, 26)
                .cuboid(-0.5313F, -3.9559F, -1.7776F, 1.0F, 4.1F, 1.8F, new Dilation(0.0F)),
            ModelTransform.of(0.3147F, 10.1763F, 1.6885F, 1.0629F, 0.0F, 0.0F));

    ModelPartData cube_r6 =
        backpackcenter.addChild(
            "cube_r6",
            ModelPartBuilder.create()
                .uv(0, 33)
                .cuboid(-0.7313F, -3.65F, 0.8F, 1.2F, 0.9F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.4147F, 13.7684F, 0.5023F, 0.0796F, 0.0F, 0.0F));

    ModelPartData cube_r7 =
        backpackcenter.addChild(
            "cube_r7",
            ModelPartBuilder.create()
                .uv(0, 0)
                .cuboid(-0.2313F, -3.45F, 0.95F, 0.4F, 1.0F, 0.75F, new Dilation(0.0F)),
            ModelTransform.of(0.3147F, 13.9356F, 0.5408F, 0.0796F, 0.0F, 0.0F));

    ModelPartData cube_r8 =
        backpackcenter.addChild(
            "cube_r8",
            ModelPartBuilder.create()
                .uv(17, 25)
                .cuboid(-0.5313F, -3.35F, 0.3F, 1.0F, 7.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.3147F, 13.9356F, 0.7408F, 0.0796F, 0.0F, 0.0F));

    ModelPartData backpack_left =
        backpack.addChild(
            "backpack_left",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.4688F, -19.3972F, 2.8758F));

    ModelPartData cube_r9 =
        backpack_left.addChild(
            "cube_r9",
            ModelPartBuilder.create()
                .uv(28, 0)
                .cuboid(1.9F, -2.8F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F))
                .uv(28, 0)
                .cuboid(0.9F, -2.6F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.5822F, 17.7987F, 2.7315F, 1.2135F, 1.0559F, 2.61F));

    ModelPartData cube_r10 =
        backpack_left.addChild(
            "cube_r10",
            ModelPartBuilder.create()
                .uv(22, 17)
                .cuboid(0.4F, -3.05F, -1.475F, 1.0F, 6.5F, 1.75F, new Dilation(0.0F)),
            ModelTransform.of(0.5312F, 13.4159F, 2.0962F, 0.0796F, 0.7854F, 0.0F));

    ModelPartData cube_r11 =
        backpack_left.addChild(
            "cube_r11",
            ModelPartBuilder.create()
                .uv(0, 0)
                .cuboid(-2.1F, -0.5F, -2.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.3387F, 18.1072F, -1.0899F, 0.1955F, 1.483F, 0.065F));

    ModelPartData cube_r12 =
        backpack_left.addChild(
            "cube_r12",
            ModelPartBuilder.create()
                .uv(0, 21)
                .cuboid(0.3F, -0.5F, -0.45F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.3081F, 8.8612F, -0.2755F, -0.3539F, 0.3991F, 0.5242F));

    ModelPartData cube_r13 =
        backpack_left.addChild(
            "cube_r13",
            ModelPartBuilder.create()
                .uv(0, 15)
                .cuboid(-1.7F, -0.5F, -2.15F, 2.0F, 1.0F, 4.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.0049F, 9.4064F, 0.1802F, -0.5053F, 0.1568F, -0.0113F));

    ModelPartData cube_r14 =
        backpack_left.addChild(
            "cube_r14",
            ModelPartBuilder.create()
                .uv(5, 35)
                .cuboid(-7.8F, -0.6345F, 1.2512F, 5.0F, 1.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(2.1464F, 17.9093F, -0.4454F, 3.0118F, -0.017F, 1.441F));

    ModelPartData cube_r15 =
        backpack_left.addChild(
            "cube_r15",
            ModelPartBuilder.create()
                .uv(13, 0)
                .cuboid(-1.7F, -0.4345F, 1.2512F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(2.1464F, 17.9093F, -0.4454F, 1.7453F, 1.4399F, 0.0F));

    ModelPartData cube_r16 =
        backpack_left.addChild(
            "cube_r16",
            ModelPartBuilder.create()
                .uv(9, 15)
                .cuboid(-1.7F, -0.4345F, -0.8988F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F)),
            ModelTransform.of(1.4817F, 17.2946F, -0.5329F, 1.0908F, 1.4399F, 0.0F));

    ModelPartData cube_r17 =
        backpack_left.addChild(
            "cube_r17",
            ModelPartBuilder.create()
                .uv(29, 29)
                .cuboid(-0.75F, -1.975F, 0.1F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.2959F, 10.6254F, 1.7177F, 0.7985F, -1.0722F, 0.7177F));

    ModelPartData cube_r18 =
        backpack_left.addChild(
            "cube_r18",
            ModelPartBuilder.create()
                .uv(29, 17)
                .cuboid(-0.2F, -2.6F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.926F, 17.4125F, 2.1691F, 0.5356F, 0.4417F, 1.6928F));

    ModelPartData cube_r19 =
        backpack_left.addChild(
            "cube_r19",
            ModelPartBuilder.create()
                .uv(22, 9)
                .cuboid(-1.0F, -0.65F, -0.3F, 1.0F, 2.05F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.9688F, 17.428F, 2.0476F, 0.3933F, 0.005F, 1.4532F));

    ModelPartData cube_r20 =
        backpack_left.addChild(
            "cube_r20",
            ModelPartBuilder.create()
                .uv(12, 30)
                .cuboid(-1.0F, -0.65F, -0.3F, 1.0F, 2.05F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.8086F, 16.7325F, 1.3767F, 0.3285F, 0.1221F, 1.1235F));

    ModelPartData cube_r21 =
        backpack_left.addChild(
            "cube_r21",
            ModelPartBuilder.create()
                .uv(11, 19)
                .cuboid(-1.45F, -1.45F, -0.55F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F)),
            ModelTransform.of(0.6399F, 16.3978F, 0.6973F, 0.3927F, 0.7854F, 0.0F));

    ModelPartData cube_r22 =
        backpack_left.addChild(
            "cube_r22",
            ModelPartBuilder.create()
                .uv(11, 19)
                .cuboid(-1.45F, -1.45F, -0.75F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F)),
            ModelTransform.of(0.6399F, 13.3978F, 0.6973F, 0.3927F, 0.7854F, 0.0F));

    ModelPartData cube_r23 =
        backpack_left.addChild(
            "cube_r23",
            ModelPartBuilder.create()
                .uv(11, 19)
                .cuboid(-1.45F, -1.45F, -1.25F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F)),
            ModelTransform.of(0.6399F, 10.3978F, 0.6973F, 0.3927F, 0.7854F, 0.0F));

    ModelPartData cube_r24 =
        backpack_left.addChild(
            "cube_r24",
            ModelPartBuilder.create()
                .uv(29, 23)
                .cuboid(-0.625F, -1.325F, 0.2F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(0.8486F, 10.454F, -1.0591F, 1.545F, -0.1325F, 0.3294F));

    ModelPartData cube_r25 =
        backpack_left.addChild(
            "cube_r25",
            ModelPartBuilder.create()
                .uv(7, 30)
                .cuboid(-0.5F, -4.425F, 1.5F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.2184F, 10.2155F, 1.7868F, 1.4407F, -0.036F, 1.4383F));

    ModelPartData cube_r26 =
        backpack_left.addChild(
            "cube_r26",
            ModelPartBuilder.create()
                .uv(30, 8)
                .cuboid(-0.5F, -1.675F, 0.1F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.2184F, 10.2155F, 1.1868F, 0.3499F, -0.036F, 1.4383F));

    ModelPartData cube_r27 =
        backpack_left.addChild(
            "cube_r27",
            ModelPartBuilder.create()
                .uv(11, 6)
                .cuboid(-2.45F, -3.3F, 0.5F, 3.8F, 6.7F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.378F, 13.1602F, 0.4853F, 0.036F, 0.351F, -0.1141F));

    ModelPartData cube_r28 =
        backpack_left.addChild(
            "cube_r28",
            ModelPartBuilder.create()
                .uv(18, 10)
                .cuboid(-1.65F, 0.7F, 0.3F, 0.4F, 2.7F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-1.4301F, 13.2736F, 1.0856F, 0.036F, 0.351F, -0.1141F));

    ModelPartData cube_r29 =
        backpack_left.addChild(
            "cube_r29",
            ModelPartBuilder.create()
                .uv(22, 27)
                .cuboid(-0.05F, -0.9F, -1.1F, 2.0F, 3.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(-0.1803F, 8.5604F, -0.4541F, 0.9163F, 1.4399F, 0.0F));

    ModelPartData cube_r30 =
        backpack_left.addChild(
            "cube_r30",
            ModelPartBuilder.create()
                .uv(0, 6)
                .cuboid(-1.35F, -3.35F, -0.5F, 4.0F, 7.0F, 1.0F, new Dilation(0.0F)),
            ModelTransform.of(1.115F, 13.316F, 0.072F, 0.1309F, 1.4399F, 0.0F));

    ModelPartData backpack_right =
        backpack.addChild(
            "backpack_right",
            ModelPartBuilder.create(),
            ModelTransform.pivot(2.4688F, -19.3972F, 2.8758F));

    ModelPartData cube_r31 =
        backpack_right.addChild(
            "cube_r31",
            ModelPartBuilder.create()
                .uv(13, 0)
                .mirrored()
                .cuboid(-2.3F, -0.4345F, 1.2512F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-7.084F, 17.9093F, -0.4454F, 1.7453F, -1.4399F, 0.0F));

    ModelPartData cube_r32 =
        backpack_right.addChild(
            "cube_r32",
            ModelPartBuilder.create()
                .uv(5, 35)
                .mirrored()
                .cuboid(2.8F, -0.6345F, 1.2512F, 5.0F, 1.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-7.084F, 17.9093F, -0.4454F, 3.0118F, 0.017F, -1.441F));

    ModelPartData cube_r33 =
        backpack_right.addChild(
            "cube_r33",
            ModelPartBuilder.create()
                .uv(0, 6)
                .mirrored()
                .cuboid(-2.65F, -3.35F, -0.5F, 4.0F, 7.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-6.0526F, 13.316F, 0.072F, 0.1309F, -1.4399F, 0.0F));

    ModelPartData cube_r34 =
        backpack_right.addChild(
            "cube_r34",
            ModelPartBuilder.create()
                .uv(29, 23)
                .mirrored()
                .cuboid(-0.375F, -1.325F, 0.2F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.7862F, 10.454F, -1.0591F, 1.545F, 0.1325F, -0.3294F));

    ModelPartData cube_r35 =
        backpack_right.addChild(
            "cube_r35",
            ModelPartBuilder.create()
                .uv(7, 30)
                .mirrored()
                .cuboid(-0.5F, -4.425F, 1.5F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.7192F, 10.2155F, 1.7868F, 1.4407F, 0.036F, -1.4383F));

    ModelPartData cube_r36 =
        backpack_right.addChild(
            "cube_r36",
            ModelPartBuilder.create()
                .uv(0, 0)
                .mirrored()
                .cuboid(-1.9F, -0.5F, -2.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-4.5989F, 18.1072F, -1.0899F, 0.1955F, -1.483F, -0.065F));

    ModelPartData cube_r37 =
        backpack_right.addChild(
            "cube_r37",
            ModelPartBuilder.create()
                .uv(0, 21)
                .mirrored()
                .cuboid(-2.3F, -0.5F, -0.45F, 2.0F, 1.0F, 3.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.6295F, 8.8612F, -0.2755F, -0.3539F, -0.3991F, -0.5242F));

    ModelPartData cube_r38 =
        backpack_right.addChild(
            "cube_r38",
            ModelPartBuilder.create()
                .uv(0, 15)
                .mirrored()
                .cuboid(-0.3F, -0.5F, -2.15F, 2.0F, 1.0F, 4.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.9327F, 9.4064F, 0.1802F, -0.5053F, -0.1568F, 0.0113F));

    ModelPartData cube_r39 =
        backpack_right.addChild(
            "cube_r39",
            ModelPartBuilder.create()
                .uv(9, 15)
                .mirrored()
                .cuboid(-2.3F, -0.4345F, -0.8988F, 4.0F, 1.0F, 2.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-6.4193F, 17.2946F, -0.5329F, 1.0908F, -1.4399F, 0.0F));

    ModelPartData cube_r40 =
        backpack_right.addChild(
            "cube_r40",
            ModelPartBuilder.create()
                .uv(29, 29)
                .mirrored()
                .cuboid(-0.25F, -1.975F, 0.1F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.6417F, 10.6254F, 1.7177F, 0.7985F, 1.0722F, -0.7177F));

    ModelPartData cube_r41 =
        backpack_right.addChild(
            "cube_r41",
            ModelPartBuilder.create()
                .uv(28, 0)
                .mirrored()
                .cuboid(-2.9F, -2.8F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false)
                .uv(28, 0)
                .mirrored()
                .cuboid(-1.9F, -2.6F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-4.3554F, 17.7987F, 2.7315F, 1.2135F, -1.0559F, -2.61F));

    ModelPartData cube_r42 =
        backpack_right.addChild(
            "cube_r42",
            ModelPartBuilder.create()
                .uv(29, 17)
                .mirrored()
                .cuboid(-0.8F, -2.6F, -0.8F, 1.0F, 4.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-4.0116F, 17.4125F, 2.1691F, 0.5356F, -0.4417F, -1.6928F));

    ModelPartData cube_r43 =
        backpack_right.addChild(
            "cube_r43",
            ModelPartBuilder.create()
                .uv(22, 9)
                .mirrored()
                .cuboid(0.0F, -0.65F, -0.3F, 1.0F, 2.05F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.9688F, 17.428F, 2.0476F, 0.3933F, -0.005F, -1.4532F));

    ModelPartData cube_r44 =
        backpack_right.addChild(
            "cube_r44",
            ModelPartBuilder.create()
                .uv(12, 30)
                .mirrored()
                .cuboid(0.0F, -0.65F, -0.3F, 1.0F, 2.05F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.7461F, 16.7325F, 1.3767F, 0.3285F, -0.1221F, -1.1235F));

    ModelPartData cube_r45 =
        backpack_right.addChild(
            "cube_r45",
            ModelPartBuilder.create()
                .uv(11, 19)
                .mirrored()
                .cuboid(-1.05F, -1.45F, -0.55F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.5775F, 16.3978F, 0.6973F, 0.3927F, -0.7854F, 0.0F));

    ModelPartData cube_r46 =
        backpack_right.addChild(
            "cube_r46",
            ModelPartBuilder.create()
                .uv(11, 19)
                .mirrored()
                .cuboid(-1.05F, -1.45F, -0.75F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.5775F, 13.3978F, 0.6973F, 0.3927F, -0.7854F, 0.0F));

    ModelPartData cube_r47 =
        backpack_right.addChild(
            "cube_r47",
            ModelPartBuilder.create()
                .uv(11, 19)
                .mirrored()
                .cuboid(-1.05F, -1.45F, -1.25F, 2.5F, 2.5F, 2.5F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.5775F, 10.3978F, 0.6973F, 0.3927F, -0.7854F, 0.0F));

    ModelPartData cube_r48 =
        backpack_right.addChild(
            "cube_r48",
            ModelPartBuilder.create()
                .uv(30, 8)
                .mirrored()
                .cuboid(-0.5F, -1.675F, 0.1F, 1.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-3.7192F, 10.2155F, 1.1868F, 0.3499F, 0.036F, -1.4383F));

    ModelPartData cube_r49 =
        backpack_right.addChild(
            "cube_r49",
            ModelPartBuilder.create()
                .uv(11, 6)
                .mirrored()
                .cuboid(-1.35F, -3.3F, 0.5F, 3.8F, 6.7F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-4.5596F, 13.1602F, 0.4853F, 0.036F, -0.351F, 0.1141F));

    ModelPartData cube_r50 =
        backpack_right.addChild(
            "cube_r50",
            ModelPartBuilder.create()
                .uv(18, 10)
                .cuboid(1.25F, 0.7F, 0.2F, 0.4F, 2.7F, 1.1F, new Dilation(0.0F)),
            ModelTransform.of(-3.5075F, 13.2736F, 1.0856F, 0.036F, -0.351F, 0.1141F));

    ModelPartData cube_r51 =
        backpack_right.addChild(
            "cube_r51",
            ModelPartBuilder.create()
                .uv(22, 27)
                .mirrored()
                .cuboid(-1.85F, -0.9F, -1.1F, 2.0F, 3.0F, 1.0F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-4.7573F, 8.5604F, -0.4541F, 0.9163F, -1.4399F, 0.0F));

    ModelPartData cube_r52 =
        backpack_right.addChild(
            "cube_r52",
            ModelPartBuilder.create()
                .uv(22, 17)
                .mirrored()
                .cuboid(-1.4F, -3.05F, -1.475F, 1.0F, 6.5F, 1.75F, new Dilation(0.0F))
                .mirrored(false),
            ModelTransform.of(-5.4688F, 13.4159F, 2.0962F, 0.0796F, -0.7854F, 0.0F));
    return TexturedModelData.of(modelData, 64, 64);
  }
}
