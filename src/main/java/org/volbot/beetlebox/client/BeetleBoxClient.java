package org.volbot.beetlebox.client;

import net.fabricmc.loader.api.FabricLoader;

import java.util.UUID;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.transfer.v1.fluid.FluidVariant;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import org.volbot.beetlebox.item.tools.JarUtils;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.BBSoundRegistry;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import org.volbot.beetlebox.client.render.gui.BeetlepackScreen;
import org.volbot.beetlebox.client.render.gui.BeetlepackScreenHandler;
import org.volbot.beetlebox.client.keys.BBKeybindManager;
import org.volbot.beetlebox.client.registry.BBItemModels;
import org.volbot.beetlebox.client.registry.BBBlockModels;
import org.volbot.beetlebox.client.registry.BBArmorModels;
import org.volbot.beetlebox.client.registry.BBEntityModels;

@Environment(EnvType.CLIENT)
public class BeetleBoxClient implements ClientModInitializer {
	
	@Override
	public void onInitializeClient() {
    
        HandledScreens.register(BeetlepackScreenHandler.BEETLEPACK_SCREEN_HANDLER_TYPE, BeetlepackScreen::new);

        if(FabricLoader.getInstance().isModLoaded("trinkets")) {
            org.volbot.beetlebox.compat.trinkets.BeetlepackTrinketRenderer.init();
        }

        BBKeybindManager.registerKeys();

        BBEntityModels.register();
        BBItemModels.register();
        BBBlockModels.register();
        BBArmorModels.register();

        ClientPlayNetworking.registerGlobalReceiver(new Identifier("beetlebox", "boiler_fluid"),
                (client, handler, buf, responseSender) -> {
                    BlockPos pos = buf.readBlockPos();
                    FluidVariant variant = FluidVariant.fromPacket(buf);
                    var world = handler.getWorld();
                    long fluid_amt = buf.readLong();
                    client.execute(() -> {
                        world.getBlockEntity(pos, BlockRegistry.BOILER_BLOCK_ENTITY)
                            .orElse(null).fluidStorage.variant = variant;
                        world.getBlockEntity(pos, BlockRegistry.BOILER_BLOCK_ENTITY)
                            .orElse(null).fluidStorage.amount = fluid_amt;
                    });
                });

        ClientPlayNetworking.registerGlobalReceiver(new Identifier("beetlebox", "beetle_proj_packet"),
                (client, handler, buf, responseSender) -> {
                    boolean landed = buf.readBoolean();
                    NbtCompound entity_nbt = buf.readNbt();
                    UUID entity_id = buf.readUuid();
                    var world = handler.getWorld();
                    client.execute(() -> {
                        Entity entity = world.getEntityLookup().get(entity_id);
                        BeetleProjectileEntity e = ((BeetleProjectileEntity) entity);
                        if (e != null) {
                            e.landed = landed;
                            var opt = JarUtils.readJarNbt(entity_nbt, world);
                            if(opt.isPresent())
                                e.entity=(BeetleEntity)opt.get();
                        }
                    });
                });

    }
}
