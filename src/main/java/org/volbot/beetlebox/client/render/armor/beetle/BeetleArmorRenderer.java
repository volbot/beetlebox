package org.volbot.beetlebox.client.render.armor.beetle;

import java.awt.Color;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ArmorRenderer;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.ArmorEntityModel;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.item.equipment.BeetleArmorTypes.ArmorPattern;

@Environment(EnvType.CLIENT)
public class BeetleArmorRenderer<T extends ArmorEntityModel<LivingEntity>>
    implements ArmorRenderer {

  private final T armorModel;
  private final BeetleType beetle_type;

  public BeetleArmorRenderer(T armorModel, BeetleType beetle_type) {
    super();
    this.armorModel = armorModel;
    this.beetle_type = beetle_type;
  }

  @Override
  public void render(
      MatrixStack matrices,
      VertexConsumerProvider vertexConsumers,
      ItemStack stack,
      LivingEntity entity,
      EquipmentSlot slot,
      int light,
      BipedEntityModel<LivingEntity> contextModel) {
    contextModel.copyBipedStateTo(armorModel);
    setVisible(slot);

    renderArmorParts(matrices, vertexConsumers, stack, getArmorTexture(slot), light);
  }

  private void renderArmorParts(
      MatrixStack matrices,
      VertexConsumerProvider vertexConsumers,
      ItemStack stack,
      Identifier texture,
      int light) {
    Color color;
    if (beetle_type.armorPattern() == ArmorPattern.CUSTOM) {
      color = new Color(0xFFFFFF);
    } else {
      color = new Color(beetle_type.color1());
    }
    VertexConsumer vertexConsumer =
        ItemRenderer.getArmorGlintConsumer(
            vertexConsumers, RenderLayer.getArmorCutoutNoCull(texture), false, stack.hasGlint());
    armorModel.render(
        matrices,
        vertexConsumer,
        light,
        OverlayTexture.DEFAULT_UV,
        color.getRed() / 255f,
        color.getGreen() / 255f,
        color.getBlue() / 255f,
        1f);
  }

  private void setVisible(EquipmentSlot slot) {
    armorModel.setVisible(false);
    switch (slot) {
      case HEAD:
        armorModel.head.visible = true;
        armorModel.hat.visible = true;
        break;
      case CHEST:
        armorModel.body.visible = true;
        armorModel.rightArm.visible = true;
        armorModel.leftArm.visible = true;
        break;
      case LEGS:
        armorModel.body.visible = true;
        armorModel.rightLeg.visible = true;
        armorModel.leftLeg.visible = true;
        break;
      case FEET:
        armorModel.rightLeg.visible = true;
        armorModel.leftLeg.visible = true;
        break;
      default:
        return;
    }
  }

  public Identifier getArmorTexture(EquipmentSlot slot) {
    String armor_id;
    String helmet_id;
    switch (beetle_type.armorPattern()) {
      case CUSTOM:
        armor_id = beetle_type.id();
        helmet_id = beetle_type.id();
        break;
      default:
        armor_id = "template_armor";
        switch (beetle_type.helmetShape()) {
          case STANDARD:
            helmet_id = "template_armor";
            break;
          default:
            helmet_id = beetle_type.id();
            break;
        }
        break;
    }
    switch (slot) {
      case HEAD:
        return new Identifier("beetlebox", "textures/models/armor/" + helmet_id + "_helmet.png");
      case CHEST:
      case FEET:
        return new Identifier("beetlebox", "textures/models/armor/" + armor_id + "_layer_1.png");
      case LEGS:
        return new Identifier("beetlebox", "textures/models/armor/" + armor_id + "_layer_2.png");
      default:
        return null;
    }
  }
}
