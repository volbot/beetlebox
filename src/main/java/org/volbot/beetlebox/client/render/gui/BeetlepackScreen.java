package org.volbot.beetlebox.client.render.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.UUID;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import org.volbot.beetlebox.client.render.gui.BeetlepackScreenHandler.BeetlepackSlot;
import org.volbot.beetlebox.config.BBConfigRegistry;
import org.volbot.beetlebox.data.tags.BeetleEntityTagGenerator;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleClass;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.LarvaJarItem;

public class BeetlepackScreen extends HandledScreen<ScreenHandler> {
  private static final Identifier TEXTURE =
      new Identifier("beetlebox", "textures/gui/container/beetlepack.png");
  private static final Identifier TEXTURE_ICONS =
      new Identifier("beetlebox", "textures/gui/beetlepack_icons.png");

  private final UUID player_uuid;
  private final World world;
  private final TextRenderer text_renderer;

  public BeetlepackScreen(ScreenHandler handler, PlayerInventory inventory, Text title) {
    super(handler, inventory, Text.of("Beetlepack"));
    player_uuid = inventory.player.getUuid();
    world = inventory.player.getWorld();
    text_renderer = MinecraftClient.getInstance().textRenderer;
  }

  @Override
  public void render(DrawContext context, int mouseX, int mouseY, float delta) {
    this.renderBackground(context);
    super.render(context, mouseX, mouseY, delta);
    int i = (this.width - this.backgroundWidth) / 2;
    int j = (this.height - this.backgroundHeight) / 2;
    for (int k = 0; k < this.getScreenHandler().slots.size(); k++) {
      Slot slot = this.getScreenHandler().getSlot(k);
      if (!(slot instanceof BeetlepackSlot)) {
        continue;
      }
      if (slot.getStack().getItem() instanceof BeetleJarItem
          || slot.getStack().getItem() instanceof LarvaJarItem) {
        NbtCompound nbt = slot.getStack().hasNbt() ? slot.getStack().getNbt() : new NbtCompound();
        Text name = Text.of("ERROR");
        boolean bl = nbt.contains("EntityType");
        if (!bl) continue;
        if (nbt.contains("EntityName")) {
          name = Text.of(nbt.getString("EntityName"));
        } else {
          EntityType<?> e = EntityType.get(nbt.getString("EntityType")).orElse(null);
          if (e != null) {
            name = e.getName();
            if (name.getString().length() > 11) {
              name = Text.of(name.asTruncatedString(10) + "...");
            }
            context.drawText(text_renderer, name, slot.x + i + 19, slot.y + j - 1, 0x404040, false);
            if (slot.getStack().getItem() instanceof LarvaJarItem) {
              context.drawTexture(
                  TEXTURE_ICONS, slot.x + i + 18, slot.y + j + 7, 0, 36, 9, 8, 256, 256);
              context.drawTexture(
                  TEXTURE_ICONS, slot.x + i + 60, slot.y + j + 5, 10, 36, 11, 11, 256, 256);
              context.drawTexture(
                  TEXTURE_ICONS, slot.x + i + 28, slot.y + j + 8, 0, 53, 31, 6, 256, 256);
              context.drawTexture(
                  TEXTURE_ICONS,
                  slot.x + i + 28,
                  slot.y + j + 8,
                  0,
                  47,
                  (int)
                      Math.round(
                          31 * nbt.getInt("GrowingTime") / BBConfigRegistry.LARVA_BASE_TICKS),
                  6);
            } else if (e.isIn(BeetleEntityTagGenerator.BEETLES)) {
              NbtCompound entity_nbt = nbt.getCompound("EntityTag");
              boolean owned =
                  (entity_nbt.containsUuid("Owner")
                      && entity_nbt.getUuid("Owner").compareTo(player_uuid) == 0);
              context.drawTexture(
                  TEXTURE_ICONS, slot.x + i + 18, slot.y + j + 7, owned ? 0 : 9, 0, 9, 9, 256, 256);
              if (owned) {
                BeetleEntity beetle = (BeetleEntity) e.create(world);
                beetle.readNbt(entity_nbt);
                beetle.readCustomDataFromNbt(entity_nbt);
                float health = beetle.getHealth() / beetle.getMaxHealth();
                context.drawTexture(
                    TEXTURE_ICONS,
                    slot.x + i + 28,
                    slot.y + j + 7,
                    health >= 0.5 ? 0 : health >= 0.25 ? 9 : 18,
                    9,
                    9,
                    9,
                    256,
                    256);
                context.drawTexture(
                    TEXTURE_ICONS,
                    slot.x + i + 38,
                    slot.y + j + 7,
                    beetle.getHighestStat().ordinal() * 9,
                    18,
                    9,
                    9,
                    256,
                    256);
                if (beetle.beetle_class != BeetleClass.NONE) {
                  context.drawTexture(
                      TEXTURE_ICONS,
                      slot.x + i + 48,
                      slot.y + j + 7,
                      (beetle.beetle_class.ordinal() - 1) * 9,
                      27,
                      9,
                      9,
                      256,
                      256);
                }
              }
            }
          }
        }
      }
    }
    this.drawMouseoverTooltip(context, mouseX, mouseY);
  }

  @Override
  protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
    RenderSystem.setShaderTexture(0, TEXTURE);
    int i = (this.width - this.backgroundWidth) / 2;
    int j = (this.height - this.backgroundHeight) / 2;
    context.drawTexture(TEXTURE, i, j, 0, 0, this.backgroundWidth, this.backgroundHeight, 256, 256);
  }

  @Override
  protected void init() {
    super.init();
  }
}
