package org.volbot.beetlebox.client.render.armor.beetle;

import java.awt.Color;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.ElytraFeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.ElytraEntityModel;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.EntityModelLoader;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.item.equipment.BeetleArmorItem;
import org.volbot.beetlebox.item.equipment.BeetleArmorTypes.ArmorPattern;
import org.volbot.beetlebox.item.equipment.materials.ChitinMaterial;

@Environment(EnvType.CLIENT)
public class BeetleElytraFeatureRenderer<T extends LivingEntity, M extends EntityModel<T>>
    extends ElytraFeatureRenderer<T, M> {

  private final ElytraEntityModel<T> elytra;

  public BeetleElytraFeatureRenderer(
      FeatureRendererContext<T, M> context, EntityModelLoader loader) {
    super(context, loader);
    this.elytra = new ElytraEntityModel<T>(ElytraEntityModel.getTexturedModelData().createModel());
  }

  @Override
  public void render(
      MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider,
      int i,
      T livingEntity,
      float f,
      float g,
      float h,
      float j,
      float k,
      float l) {
    ItemStack itemStack = livingEntity.getEquippedStack(EquipmentSlot.CHEST);
    if (!(itemStack.getOrCreateNbt().contains("beetle_chest_elytra"))) {
      return;
    }
    BeetleType beetle_type =
        BeetleType.get(
            ((ChitinMaterial) (((BeetleArmorItem) itemStack.getItem()).getMaterial())).getName());
    String armor_id;
    Color color;
    switch (beetle_type.armorPattern()) {
      case CUSTOM:
        armor_id = beetle_type.id();
        color = new Color(0xFFFFFF);
        break;
      default:
        armor_id = "template";
        color = new Color(beetle_type.color1());
        break;
    }
    Identifier skin =
        new Identifier("beetlebox", "textures/entity/elytra/" + armor_id + "_elytra.png");
    if (beetle_type.armorPattern() == ArmorPattern.CUSTOM) {
    } else {
    }
    matrixStack.push();
    matrixStack.translate(0.0f, 0.0f, 0.125f);
    M context = this.getContextModel();
    context.copyStateTo(this.elytra);
    this.elytra.setAngles(livingEntity, f, g, j, k, l);
    VertexConsumer vertexConsumer =
        ItemRenderer.getArmorGlintConsumer(
            vertexConsumerProvider,
            RenderLayer.getArmorCutoutNoCull(skin),
            false,
            itemStack.hasGlint());
    this.elytra.render(
        matrixStack,
        vertexConsumer,
        i,
        OverlayTexture.DEFAULT_UV,
        color.getRed() / 255f,
        color.getGreen() / 255f,
        color.getBlue() / 255f,
        1.0f);
    matrixStack.pop();
  }
}
