package org.volbot.beetlebox.client.render.entity.beetle;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.client.model.entity.beetle.BeetleEntityModel;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;

@Environment(EnvType.CLIENT)
public class BeetleEntityRenderer<T extends BeetleEntity>
    extends MobEntityRenderer<T, BeetleEntityModel<T>> {

  private final Identifier TEXTURE;

  public BeetleEntityRenderer(
      EntityRendererFactory.Context context,
      Identifier texture,
      EntityModelLayer model_layer,
      float head_turn_scale) {
    super(context, new BeetleEntityModel<T>(context.getPart(model_layer), head_turn_scale), 0.27f);
    TEXTURE = texture;
  }

  @Override
  public Identifier getTexture(T entity) {
    this.shadowRadius = 0.27f * (entity.getStat(BeetleStat.BB_SIZE) / 10f);
    return TEXTURE;
  }
}
