package org.volbot.beetlebox.client.render.block.entity;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RotationAxis;
import org.volbot.beetlebox.client.render.item.JarRenderer;
import org.volbot.beetlebox.entity.block.IncubatorBlockEntity;
import org.volbot.beetlebox.block.IncubatorBlock;
import org.volbot.beetlebox.registry.ItemRegistry;

@Environment(EnvType.CLIENT)
public class IncubatorBlockEntityRenderer implements BlockEntityRenderer<IncubatorBlockEntity> {


	public IncubatorBlockEntityRenderer(BlockEntityRendererFactory.Context ctx) { }

	@Override
	public void render(IncubatorBlockEntity te, float f, MatrixStack matrices, VertexConsumerProvider vcp, int i,
			int j) {		
        matrices.push();
        int degrees;
		if (te.getCachedState().getProperties().contains(IncubatorBlock.FACING)) {
			switch (te.getCachedState().get(IncubatorBlock.FACING)) {
			case NORTH:
				degrees = 270;
                matrices.translate(1f, 0f, 0f);
				break;
			case WEST:
				degrees = 0;
				break;
			case SOUTH:
				degrees = 90;
				matrices.translate(0f, 0f, 1f);
				break;
			case EAST:
				degrees = 180;
				matrices.translate(1f, 0f, 1f);
				break;
			default:
				degrees = 0;
				break;
			}
		} else {
			degrees = 0;
		}
		matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(degrees));
		matrices.multiply(RotationAxis.POSITIVE_X.rotationDegrees(180));
		ItemStack stack;
		for (int slot = 0; slot < te.size(); slot++) {
			stack = te.getStack(slot);
			if (stack.isOf(ItemRegistry.LARVA_JAR)) {
				matrices = translateForNextJar(matrices, slot);
				JarRenderer.renderJar(matrices, vcp, i);
			} else {
				matrices = translateForNextJar(matrices, slot);
			}
		}
        matrices.pop();
	}

	public MatrixStack translateForNextJar(MatrixStack matrices, int slot) {
		switch (slot) {
		case 0:
			matrices.translate(0.44f, 0f, -0.4375f);
			break;
		case 1:
			matrices.translate(0.31f, 0f, -0.0625f);
			break;
		case 2:
			matrices.translate(0.31f, 0f, 0.0625f);
			break;
		case 3:
			matrices.translate(-0.62f, 0f, -0.5f);
			break;
		case 4:
			matrices.translate(0.31f, 0f, 0.0625f);
			break;
		case 5:
			matrices.translate(0.31f, 0f, -0.0625f);
			break;
		}
		return matrices;
	}
}
