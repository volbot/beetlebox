package org.volbot.beetlebox.client.render.entity.projectile;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;
import org.joml.Quaternionf;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;

public class BeetleProjectileEntityRenderer<T extends BeetleProjectileEntity>
    extends EntityRenderer<T> {

  Quaternionf rot = new Quaternionf();

  public BeetleProjectileEntityRenderer(
      EntityRendererFactory.Context ctx, float scale, boolean lit) {
    super(ctx);
  }

  public BeetleProjectileEntityRenderer(EntityRendererFactory.Context context) {
    this(context, 1.0f, false);
  }

  @Override
  public void render(
      T entity,
      float yaw,
      float tickDelta,
      MatrixStack matrices,
      VertexConsumerProvider vertexConsumers,
      int light) {
    if (((Entity) entity).age < 2
        && this.dispatcher.camera.getFocusedEntity().squaredDistanceTo((Entity) entity) < 75) {
      return;
    }
    matrices.push();
    matrices.multiply(
        RotationAxis.POSITIVE_Y.rotationDegrees(
            MathHelper.lerp(
                tickDelta,
                ((PersistentProjectileEntity) entity).prevYaw,
                ((Entity) entity).getYaw()))); // - 90.0f));
    matrices.multiply(
        RotationAxis.POSITIVE_Z.rotationDegrees(
            MathHelper.lerp(
                tickDelta,
                ((PersistentProjectileEntity) entity).prevPitch,
                ((Entity) entity).getPitch())));
    if (entity.entity != null) {

      BeetleEntity temp = entity.entity;
      temp.setOnGround(false);

      temp.setStat(BeetleStat.BB_SIZE, 5);

      MinecraftClient.getInstance()
          .getEntityRenderDispatcher()
          .render(temp, 0, 0, 0, yaw, 0, matrices, vertexConsumers, light);
    }
    matrices.pop();
    super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
  }

  @Override
  public Identifier getTexture(T e) {
    return new Identifier("beetlebox", "textures/entity/beetle/actaeon.png");
  }
}
