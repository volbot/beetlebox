package org.volbot.beetlebox.client.render.armor;

import java.util.function.Predicate;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ArmorRenderer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.RotationAxis;
import org.volbot.beetlebox.client.model.armor.BeetlepackModel;
import org.volbot.beetlebox.client.render.item.JarRenderer;
import org.volbot.beetlebox.item.equipment.BeetlepackInventory;
import org.volbot.beetlebox.registry.ItemRegistry;

@Environment(EnvType.CLIENT)
public class BeetlepackRenderer<T extends BeetlepackModel<LivingEntity>> implements ArmorRenderer {

  protected T armorModel;

  protected static final Identifier TEXTURE =
      new Identifier("beetlebox", "textures/models/armor/beetlepack.png");

  private static final Predicate<ItemStack> BEETLEPACK_RENDER_JAR_PREDICATE =
      stack ->
          stack.isOf(ItemRegistry.BEETLE_JAR)
              || stack.isOf(ItemRegistry.LEG_BEETLE_JAR)
              || stack.isOf(ItemRegistry.LARVA_JAR);

  public BeetlepackRenderer(T model) {
    super();
    armorModel = model;
  }

  @Override
  public void render(
      MatrixStack matrices,
      VertexConsumerProvider vcp,
      ItemStack stack,
      LivingEntity entity,
      EquipmentSlot slot,
      int light,
      BipedEntityModel<LivingEntity> contextModel) {

    contextModel.copyBipedStateTo(armorModel);
    armorModel.setVisible(false);
    armorModel.body.visible = true;

    float scale_factor = 1.1f;
    matrices.push();
    matrices.scale(scale_factor, scale_factor, scale_factor);

    ArmorRenderer.renderPart(matrices, vcp, light, stack, armorModel, TEXTURE);
    scale_factor = 0.5f;
    matrices.scale(scale_factor, scale_factor, scale_factor);

    if (entity.isInSneakingPose()) // adjust for crouching
    matrices.translate(0.0f, 0.395f, 0f);

    matrices.multiply(RotationAxis.POSITIVE_X.rotation(armorModel.body.pitch));
    matrices.multiply(RotationAxis.POSITIVE_Y.rotation(armorModel.body.yaw));
    matrices.multiply(RotationAxis.POSITIVE_Z.rotation(armorModel.body.roll));

    BeetlepackInventory inventory = new BeetlepackInventory(stack);
    for (int i = 0; i < inventory.size(); i++) {
      ItemStack stack2 = inventory.getStack(i);
      if (BEETLEPACK_RENDER_JAR_PREDICATE.test(stack2)) {
        // IMPORTANT: each jar is rendered
        //  on a substack to isolate mutations
        matrices.push();
        renderJar(matrices, vcp, stack2, i, light);
        matrices.pop();
      }
    }

    matrices.pop();
  }

  public void renderJar(
      MatrixStack matrices, VertexConsumerProvider vcp, ItemStack stack, int slot, int light) {

    // invert X for even slots
    if (slot % 2 == 0) matrices.scale(-1f, 1f, 1f);

    // ROTATION: YAW
    matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(45f));
    // ROTATION: PITCH
    matrices.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(-67.5f));

    //////////////////////
    // ROTATION COMPLETE
    //  translation is now relative
    // to rendered angles

    // TRANSLATION: Z (horizontal ?odd+right-left)
    matrices.translate(0.0f, 0f, -0.035f);

    //////////////////
    // BASE LOCATION
    //  slot-based positioning should
    //  1) go after this
    //  2) only use X and Y translates

    // TRANSLATION: Y (depth +in-out)
    if (slot < 2) {
      matrices.translate(0.0f, -0.125f, 0f);
    } else if (slot < 4) {
      matrices.translate(0.0f, -0.05f, 0f);
    } else if (slot < 6) {
      matrices.translate(0.0f, 0.075f, 0f);
    }

    // TRANSLATION: X (vertical +up -down)
    if (slot < 2) {
      matrices.translate(-0.215f, 0f, 0f);
    } else if (slot < 4) {
      matrices.translate(-0.215f - 0.35f, 0f, 0f);
    } else if (slot < 6) {
      matrices.translate(-0.215f - 0.7f, 0f, 0f);
    }

    // RENDER JAR
    JarRenderer.renderJar(matrices, vcp, light, true);
  }
}
