package org.volbot.beetlebox.client.keys;

import org.lwjgl.glfw.GLFW;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.util.Identifier;

@SuppressWarnings("deprecation")
@Environment(EnvType.CLIENT)
public class BBKeybindManager {

	public static KeyBinding elytra_boost_keybind;
	public static KeyBinding wallclimb_keybind;
	public static KeyBinding bp_t_attack_keybind;
	public static KeyBinding bp_t_flight_keybind;
	public static KeyBinding bp_t_intake_keybind;
	public static KeyBinding bp_open_keybind;
	
	public static void registerKeys() {
    
		elytra_boost_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.elytra_boost",
				InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_R, "category.beetlebox.beetlebox"));
		wallclimb_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.wall_climb",
				InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_J, "category.beetlebox.beetlebox"));
		bp_t_attack_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.bp_t_attack",
				InputUtil.Type.KEYSYM, -1, "category.beetlebox.beetlebox"));
		bp_t_flight_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.bp_t_flight",
				InputUtil.Type.KEYSYM, -1, "category.beetlebox.beetlebox"));
		bp_t_intake_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.bp_t_intake",
				InputUtil.Type.KEYSYM, -1, "category.beetlebox.beetlebox"));
		bp_open_keybind = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.beetlebox.bp_open",
				InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_B, "category.beetlebox.beetlebox"));
		
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			while (elytra_boost_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetle_elytraboost_packet"), PacketByteBufs.create()); 
			}
			while (wallclimb_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetle_togglewallclimb_packet"), PacketByteBufs.create()); 
			}
			while (bp_t_attack_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetlepack_toggleattack_packet"), PacketByteBufs.create()); 
			}
			while (bp_t_flight_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetlepack_toggleflight_packet"), PacketByteBufs.create()); 
			}
			while (bp_t_intake_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetlepack_toggleintake_packet"), PacketByteBufs.create()); 
			}
			while (bp_open_keybind.wasPressed()) {
                ClientPlayNetworking.send(new Identifier("beetlebox", "beetlepack_open_packet"), PacketByteBufs.create()); 
			}
		});
	}
}
