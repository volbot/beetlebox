package org.volbot.beetlebox.compat.jei;

import java.util.ArrayList;
import java.util.List;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.gui.ITickTimer;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.registration.IRecipeCatalystRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.client.MinecraftClient;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.book.CraftingRecipeCategory;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import org.volbot.beetlebox.recipe.UpgradeRecipe;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.DataRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BBJEIPlugin implements IModPlugin {

  public static IJeiHelpers jeiHelper;

  public final Identifier TEXTURE_WIDGETS = new Identifier("beetlebox", "textures/gui/widgets.png");
  public static IDrawableAnimated RECIPE_ARROW;

  public static final BBJEIBoilingRecipeCategory JEI_BOILING_RECIPE =
      new BBJEIBoilingRecipeCategory();
  public static final BBJEIUpgradeRecipeCategory JEI_UPGRADE_RECIPE =
      new BBJEIUpgradeRecipeCategory();

  @Override
  public void registerRecipes(IRecipeRegistration registration) {
    var instance = MinecraftClient.getInstance();
    var recipe_manager = instance.world.getRecipeManager();

    registration.addRecipes(
        JEI_BOILING_RECIPE.RECIPE_TYPE,
        recipe_manager.listAllOfType(DataRegistry.BOILING_RECIPE_TYPE).stream()
            .filter(r -> !r.getOutput(instance.world.getRegistryManager()).isEmpty())
            .toList());

    registration.addRecipes(JEI_UPGRADE_RECIPE.RECIPE_TYPE, generateUpgradeActivationRecipes());
  }

  @Override
  public void registerRecipeCatalysts(IRecipeCatalystRegistration registration) {
    registration.addRecipeCatalyst(
        BlockRegistry.BOILER_ITEM.getDefaultStack(), JEI_BOILING_RECIPE.RECIPE_TYPE);
    registration.addRecipeCatalyst(
        ItemRegistry.UPGRADE_DORMANT.getDefaultStack(), JEI_UPGRADE_RECIPE.RECIPE_TYPE);
  }

  @Override
  public void registerCategories(IRecipeCategoryRegistration registration) {
    jeiHelper = registration.getJeiHelpers();
    ITickTimer tickTimer = jeiHelper.getGuiHelper().createTickTimer(160, 24, true);
    RECIPE_ARROW =
        jeiHelper
            .getGuiHelper()
            .drawableBuilder(TEXTURE_WIDGETS, 0, 0, 24, 17)
            .buildAnimated(tickTimer, IDrawableAnimated.StartDirection.LEFT);
    registration.addRecipeCategories(JEI_BOILING_RECIPE);
    registration.addRecipeCategories(JEI_UPGRADE_RECIPE);
  }

  @Override
  public Identifier getPluginUid() {
    return new Identifier("beetlebox", "default");
  }

  private List<UpgradeRecipe> generateUpgradeActivationRecipes() {
    ArrayList<UpgradeRecipe> list = new ArrayList<>();
    for (String key : ItemRegistry.UPGRADE_MAP.keySet()) {
      ItemStack upgrade = ItemRegistry.UPGRADE_MAP.get(key).getDefaultStack();
      ItemStack dormant = ItemRegistry.UPGRADE_DORMANT.getDefaultStack();
      dormant.getOrCreateNbt().putBoolean(key, true);
      DefaultedList<Ingredient> ingredients = DefaultedList.ofSize(1, Ingredient.ofStacks(dormant));
      list.add(
          new UpgradeRecipe(
              new Identifier("beetlebox", "upgrade_recipe"),
              "misc",
              CraftingRecipeCategory.MISC,
              1,
              1,
              ingredients,
              upgrade));
    }
    return list;
  }
}
