package org.volbot.beetlebox.compat.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.recipe.UpgradeRecipe;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BBJEIUpgradeRecipeCategory extends BBJEIAbstractRecipeCategory<UpgradeRecipe> {

  private final Identifier TEXTURE_BG =
      new Identifier("beetlebox", "textures/gui/jei/upgrade_jei.png");

  BBJEIUpgradeRecipeCategory() {
    super(
        new Identifier("beetlebox", "upgrade_recipe"),
        UpgradeRecipe.class,
        ItemRegistry.UPGRADE_DORMANT.getDefaultStack());
  }

  @Override
  public void setRecipe(
      IRecipeLayoutBuilder layout, UpgradeRecipe recipe, IFocusGroup ingredients) {
    layout
        .addSlot(RecipeIngredientRole.INPUT, 25, 35)
        .addIngredients(recipe.getIngredients().get(0));
    layout.addSlot(RecipeIngredientRole.OUTPUT, 84, 35).addItemStack(recipe.getOutput(null));
  }

  @Override
  public IDrawable getBackground() {
    return BBJEIPlugin.jeiHelper.getGuiHelper().createDrawable(TEXTURE_BG, 0, 0, 125, 86);
  }
}
