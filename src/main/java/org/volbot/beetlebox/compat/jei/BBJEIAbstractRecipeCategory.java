package org.volbot.beetlebox.compat.jei;

import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Recipe;

import mezz.jei.api.recipe.category.IRecipeCategory;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.gui.drawable.IDrawable;

public abstract class BBJEIAbstractRecipeCategory<T extends Recipe<?>> implements IRecipeCategory<T> {

    public final RecipeType<T> RECIPE_TYPE;
    private final ItemStack ICON_STACK;

    public BBJEIAbstractRecipeCategory(
            Identifier  id     ,
            Class<T>    clazz  ,
            ItemStack   icon    
            ) {
    
            super();
            RECIPE_TYPE = RecipeType.create(id.getNamespace(),id.getPath(),clazz);
            ICON_STACK = icon;
            
            }


    @Override
    public Text getTitle() {
        return Text.translatable(ICON_STACK.getTranslationKey());
    }

    @Override
    public IDrawable getIcon() {
       return BBJEIPlugin.jeiHelper.getGuiHelper().createDrawableItemStack(ICON_STACK); 
    }

    @Override
    public RecipeType<T> getRecipeType() {
        return RECIPE_TYPE;
    }
}
