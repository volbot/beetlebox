package org.volbot.beetlebox.compat.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.recipe.BoilingRecipe;
import org.volbot.beetlebox.registry.BlockRegistry;

public class BBJEIBoilingRecipeCategory extends BBJEIAbstractRecipeCategory<BoilingRecipe> {

  private final Identifier TEXTURE_BG =
      new Identifier("beetlebox", "textures/gui/jei/boiler_jei.png");

  BBJEIBoilingRecipeCategory() {
    super(
        new Identifier("beetlebox", "boiling_recipe"),
        BoilingRecipe.class,
        BlockRegistry.BOILER_ITEM.getDefaultStack());
  }

  @Override
  public void draw(
      BoilingRecipe recipe,
      IRecipeSlotsView recipeSlotsView,
      DrawContext drawContext,
      double mouseX,
      double mouseY) {
    BBJEIPlugin.RECIPE_ARROW.draw(drawContext, 60, 35);
    drawCookTime(drawContext, recipe.getCookTime(), 141, 6);
    drawFluidUsage(drawContext, recipe.fluid_droplets, 141, 16);
  }

  public void drawCookTime(DrawContext drawContext, int cookTime, int width, int y) {
    String str = (cookTime / 20.) + " sec";
    TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;
    int strlen = textRenderer.getWidth(str);
    drawContext.drawText(textRenderer, str, width - strlen - 4, y, 0xFF808080, false);
  }

  public void drawFluidUsage(DrawContext drawContext, int mb, int width, int y) {
    String str = mb + "mB";
    TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;
    int strlen = textRenderer.getWidth(str);
    drawContext.drawText(textRenderer, str, width - strlen - 4, y, 0xFF808080, false);
  }

  @Override
  public void setRecipe(
      IRecipeLayoutBuilder layout, BoilingRecipe recipe, IFocusGroup ingredients) {
    layout
        .addSlot(RecipeIngredientRole.INPUT, 34, 14)
        .addIngredients(recipe.getIngredients().get(0));
    layout.addSlot(RecipeIngredientRole.OUTPUT, 93, 35).addItemStack(recipe.getOutput(null));
  }

  @Override
  public IDrawable getBackground() {
    return BBJEIPlugin.jeiHelper.getGuiHelper().createDrawable(TEXTURE_BG, 0, 0, 141, 86);
  }
}
