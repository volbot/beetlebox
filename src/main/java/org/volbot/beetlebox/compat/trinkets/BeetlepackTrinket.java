package org.volbot.beetlebox.compat.trinkets;

import dev.emi.trinkets.TrinketSlot;
import dev.emi.trinkets.api.TrinketComponent;
import dev.emi.trinkets.api.TrinketInventory;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import org.volbot.beetlebox.item.equipment.BeetlepackItem;
import org.volbot.beetlebox.registry.ItemRegistry;
import dev.emi.trinkets.api.TrinketsApi;
import dev.emi.trinkets.api.Trinket;
import dev.emi.trinkets.api.SlotReference;
import net.minecraft.util.Identifier;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;

public class BeetlepackTrinket extends BeetlepackItem implements Trinket {

    public BeetlepackTrinket(Settings settings) {
        super(settings);
    }

    public static void init() {
        ItemRegistry.BEETLEPACK = new BeetlepackTrinket(new FabricItemSettings());
        Registry.register(Registries.ITEM, new Identifier("beetlebox", "beetlepack"), ItemRegistry.BEETLEPACK);
        TrinketsApi.registerTrinket(ItemRegistry.BEETLEPACK, (BeetlepackTrinket)ItemRegistry.BEETLEPACK);
    }

    public static ItemStack getBackStack(PlayerEntity playerEntity) {
        TrinketComponent tc = TrinketsApi.getTrinketComponent(playerEntity).get();
        if (tc.isEquipped(ItemRegistry.BEETLEPACK)) {
            return tc.getInventory().get("chest").get("back").getStack(0);
        } else {
            return ItemStack.EMPTY;
        }
    }

    public static void setBackStack(PlayerEntity playerEntity, ItemStack stack) {
        TrinketComponent tc = TrinketsApi.getTrinketComponent(playerEntity).get();
        var back_inv = tc.getInventory().get("chest").get("back");
        if (!back_inv.isEmpty()){
            back_inv.clear();
        }
        back_inv.setStack(0, stack);
        back_inv.markDirty();
    }

    @Override
    public void onEquip(ItemStack newStack, SlotReference slot, LivingEntity entity) {
        for (ItemStack armor_stack : entity.getArmorItems()) {
            if (armor_stack.isOf(ItemRegistry.BEETLEPACK)) {
                entity.dropStack(armor_stack.copy());
                armor_stack.setCount(0);
            }
        }
    }

@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		ItemStack stack = user.getStackInHand(hand);
		if (!user.isSneaking() && equipItem(user, stack)) {
			return TypedActionResult.success(stack, world.isClient());
        }
		return super.use(world, user, hand);
	}

	public static boolean equipItem(PlayerEntity user, ItemStack stack) {
		return equipItem((LivingEntity) user, stack);
	}

	public static boolean equipItem(LivingEntity user, ItemStack stack) {
		var optional = TrinketsApi.getTrinketComponent(user);
		if (optional.isPresent()) {
			TrinketComponent comp = optional.get();
			for (var group : comp.getInventory().values()) {
				for (TrinketInventory inv : group.values()) {
					for (int i = 0; i < inv.size(); i++) {
						if (inv.getStack(i).isEmpty()) {
							SlotReference ref = new SlotReference(inv, i);
							if (TrinketSlot.canInsert(stack, ref, user)) {
								ItemStack newStack = stack.copy();
								inv.setStack(i, newStack);
								if (!stack.isEmpty()) {
								   user.emitGameEvent(GameEvent.EQUIP);
								}
								stack.setCount(0);
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

    @Override
    public EquipmentSlot getSlotType() {
        return EquipmentSlot.MAINHAND;
    }

    @Override
    public void tick(ItemStack stack, SlotReference slot, LivingEntity entity) {
        this.inventoryTick(stack, entity.getWorld(), entity, slot.index(), false);
    }
}
