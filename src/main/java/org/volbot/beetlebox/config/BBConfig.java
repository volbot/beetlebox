package org.volbot.beetlebox.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.io.PrintWriter;

import net.fabricmc.loader.api.FabricLoader;

public class BBConfig {

    private final HashMap<String, String> config = new HashMap<>();

    public BBConfig() {
        try {
            loadConfig();
        } catch (Exception e) {
            System.out.println("ERROR LOADING CONFIG");
            System.out.println(e.getMessage()); 
        }

        populateDefaults(config);
    }

    private void populateDefaults(HashMap<String, String> config) {

        config.putIfAbsent("beetlepack.toggle.intake", "true");
        config.putIfAbsent("beetlepack.toggle.deploy.flight", "true");
        config.putIfAbsent("beetlepack.toggle.deploy.combat", "true");

        config.putIfAbsent("tank.tame_ticks", "10800");
        config.putIfAbsent("tank.tame_levels", "5");
        config.putIfAbsent("tank.breed_ticks", "200");
       
        config.putIfAbsent("larva.base_ticks", "84000"); 
        config.putIfAbsent("larva.multiplier.default", "1");
        config.putIfAbsent("larva.multiplier.incubator", "5");
        config.putIfAbsent("larva.multiplier.beetlepack", "2");
        config.putIfAbsent("larva.husks.max", "4");
        config.putIfAbsent("larva.husks.min", "1");

        config.putIfAbsent("boiler.fluid.capacity", "2000");
        config.putIfAbsent("boiler.fluid.usage", "125");
        
        config.putIfAbsent("jelly.stat_increment", "20");
        config.putIfAbsent("jelly.stat_increment_limit", "100");

    }

    private void writeConfig(HashMap<String,String> config, File file) throws IOException {
        PrintWriter printer = new PrintWriter(file);

        printer.println("#############################");
        printer.println("## BEETLEBOX CONFIGURATION ##");
        printer.println("#############################");
        printer.println("# Beetlebox generates every possible default setting, ");
        printer.println("#   but does not require them. ");
        printer.println("# Feel free to delete lines that are otherwise default, ");
        printer.println("#   or reorganize as you see fit! ");
        printer.println();

        List<String> keySet_ordered = new ArrayList<String>(config.keySet());
        Collections.sort(keySet_ordered);

        for(String key : keySet_ordered) {
            String value = config.get(key);
            printer.println(key+" = "+value);
        }

        printer.close();
    }

    private void loadConfig() throws IOException {
        Path path = FabricLoader.getInstance().getConfigDir();
        File file = path.resolve("beetlebox.conf").toFile();

        if(!file.exists()) {
            file.getParentFile().mkdirs();
            Files.createFile(file.toPath());
       
            populateDefaults(config);

            writeConfig(config,file);

            return;
        }

        String line;    int i=0;
        Scanner reader = new Scanner(file);
        while (reader.hasNextLine()){
            line=reader.nextLine(); i++;
            //remove spaces
            line = line.replaceAll("\\s","");

            //scrub single-line comments
            if(line.isEmpty() || line.startsWith("#"))
                continue;

            String[] parts = line.split("=",2);
            if(parts.length==2) {

                //scrub end-of-line comments
                String value = parts[1].split(" #")[0];

                //write to internal
                config.put( parts[0], value );

            } else {

                reader.close();
                throw new RuntimeException("Syntax error in config file on line "+i+"!");

            }
        }
        reader.close();
    }

    public int getInt(String key) {
        if(!config.containsKey(key))
            return -1;
        String value = config.get(key);
        return Integer.parseInt(value);
    }

    public boolean getBoolean(String key) {
        if(!config.containsKey(key))
            return false;
        String value = config.get(key);
        return value.compareTo("true")==0;
    }
}
