package org.volbot.beetlebox.config;

public class BBConfigRegistry {
    
    public static BBConfig CONF;

    public static boolean BEETLEPACK_TOGGLE_INTAKE = true;
    public static boolean BEETLEPACK_TOGGLE_DEPLOY_FLIGHT = true;
    public static boolean BEETLEPACK_TOGGLE_DEPLOY_ATTACK = true;

    public static int TANK_TAME_TICKS = 10800;
    public static int TANK_TAME_LEVELS = 5;
    public static int TANK_BREED_TICKS = 200;

    public static int LARVA_BASE_TICKS = 84000;
    public static int LARVA_MULTIPLIER_DEFAULT = 1;
    public static int LARVA_MULTIPLIER_INCUBATOR = 5;
    public static int LARVA_MULTIPLIER_BEETLEPACK = 2;
    public static int LARVA_HUSKS_MAX = 4;
    public static int LARVA_HUSKS_MIN = 1;

    public static int BOILER_FLUID_CAPACITY = 2000;
    public static int BOILER_FLUID_USAGE = 125;

    public static int JELLY_STAT_INCREMENT = 20;
    public static int JELLY_STAT_INCREMENT_LIMIT = 100;

    public static void registerConfigs() {
        CONF = new BBConfig();

        assignConfigs();
    }

    public static void assignConfigs() {
        BEETLEPACK_TOGGLE_INTAKE = CONF.getBoolean("beetlepack.toggle.intake");
        BEETLEPACK_TOGGLE_DEPLOY_FLIGHT = CONF.getBoolean("beetlepack.toggle.deploy.flight");
        BEETLEPACK_TOGGLE_DEPLOY_ATTACK = CONF.getBoolean("beetlepack.toggle.deploy.attack");

        TANK_TAME_TICKS = CONF.getInt("tank.tame_ticks");
        TANK_TAME_LEVELS = CONF.getInt("tank.tame_levels");
        TANK_BREED_TICKS = CONF.getInt("tank.breed_ticks");

        LARVA_BASE_TICKS = CONF.getInt("larva.base_ticks");
        LARVA_MULTIPLIER_DEFAULT = CONF.getInt("larva.multiplier.default");
        LARVA_MULTIPLIER_INCUBATOR = CONF.getInt("larva.multiplier.incubator");
        LARVA_MULTIPLIER_BEETLEPACK = CONF.getInt("larva.multiplier.beetlepack");
        LARVA_HUSKS_MAX = CONF.getInt("larva.husks.max");
        LARVA_HUSKS_MIN = CONF.getInt("larva.husks.min");

        BOILER_FLUID_CAPACITY = CONF.getInt("boiler.fluid.capacity");
        BOILER_FLUID_USAGE = CONF.getInt("boiler.fluid.usage");

        JELLY_STAT_INCREMENT = CONF.getInt("jelly.stat_increment");
        JELLY_STAT_INCREMENT_LIMIT = CONF.getInt("jelly.stat_increment_limit");
    }

}
