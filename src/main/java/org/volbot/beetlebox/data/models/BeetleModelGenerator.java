package org.volbot.beetlebox.data.models;

import java.util.Optional;
import java.util.Vector;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricModelProvider;
import net.minecraft.data.client.BlockStateModelGenerator;
import net.minecraft.data.client.ItemModelGenerator;
import net.minecraft.data.client.Model;
import net.minecraft.data.client.Models;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BeetleModelGenerator extends FabricModelProvider {

  public static Vector<Item> items = new Vector<>();

  public BeetleModelGenerator(FabricDataOutput output) {
    super(output);
  }

  @Override
  public void generateBlockStateModels(BlockStateModelGenerator blockStateModelGenerator) {
    blockStateModelGenerator
        .registerLog(BlockRegistry.ASH_LOG)
        .log(BlockRegistry.ASH_LOG)
        .wood(BlockRegistry.ASH_WOOD);
    blockStateModelGenerator.registerLog(BlockRegistry.ASH_WOOD);
    blockStateModelGenerator
        .registerLog(BlockRegistry.ASH_LOG_STRIPPED)
        .log(BlockRegistry.ASH_LOG_STRIPPED)
        .wood(BlockRegistry.ASH_WOOD_STRIPPED);
    blockStateModelGenerator.registerLog(BlockRegistry.ASH_WOOD_STRIPPED);

    blockStateModelGenerator
        .registerCubeAllModelTexturePool(BlockRegistry.ASH_PLANKS)
        .family(BlockRegistry.ASH_FAMILY);

    blockStateModelGenerator.registerTintableCross(
        BlockRegistry.ASH_SAPLING, BlockStateModelGenerator.TintType.NOT_TINTED);
  }

  @Override
  public void generateItemModels(ItemModelGenerator itemModelGenerator) {
    items.add(ItemRegistry.SUBSTRATE);
    items.add(ItemRegistry.GELATIN);
    items.add(ItemRegistry.SUGAR_GELATIN);
    items.add(ItemRegistry.GELATIN_GLUE);
    items.add(ItemRegistry.BEETLE_JELLY);
    items.add(ItemRegistry.BEETLE_HUSK);
    items.add(ItemRegistry.UPGRADE_DORMANT);
    items.add(ItemRegistry.BEETLEPACK);
    items.addAll(ItemRegistry.helmet_upgrades);
    items.addAll(ItemRegistry.chest_upgrades);
    items.addAll(ItemRegistry.legs_upgrades);
    items.addAll(ItemRegistry.boots_upgrades);
    for (BeetleType beetle_type : BeetleType.values()) {
      itemModelGenerator.register(
          beetle_type.spawnEgg(),
          new Model(
              Optional.of(new Identifier("minecraft", "item/template_spawn_egg")),
              Optional.empty()));
      itemModelGenerator.register(
          beetle_type.elytron(),
          new Model(
              Optional.of(new Identifier("beetlebox", "item/template_elytron")), Optional.empty()));
      Item[] armor = beetle_type.armorItems();
      itemModelGenerator.register(
          armor[0],
          new Model(
              Optional.of(
                  new Identifier(
                      "beetlebox", "item/template_helmet" + beetle_type.helmetShape().texSuffix())),
              Optional.empty()));
      itemModelGenerator.register(
          armor[1],
          new Model(
              Optional.of(new Identifier("beetlebox", "item/template_chestplate")),
              Optional.empty()));
      itemModelGenerator.register(
          armor[2],
          new Model(
              Optional.of(new Identifier("beetlebox", "item/template_leggings")),
              Optional.empty()));
      itemModelGenerator.register(
          armor[3],
          new Model(
              Optional.of(new Identifier("beetlebox", "item/template_boots")), Optional.empty()));
    }
    for (Item item : items) {
      itemModelGenerator.register(item, Models.GENERATED);
    }
  }
}
