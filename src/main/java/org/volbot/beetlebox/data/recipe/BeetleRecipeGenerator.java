package org.volbot.beetlebox.data.recipe;

import net.fabricmc.loader.api.FabricLoader;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.block.Blocks;
import net.minecraft.data.server.recipe.RecipeJsonProvider;
import net.minecraft.data.server.recipe.RecipeProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder;
import net.minecraft.data.server.recipe.ShapelessRecipeJsonBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.book.RecipeCategory;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.data.tags.BeetleItemTagGenerator;
import org.volbot.beetlebox.registry.BlockRegistry;
import org.volbot.beetlebox.registry.ItemRegistry;

public class BeetleRecipeGenerator extends FabricRecipeProvider {

	public BeetleRecipeGenerator(FabricDataOutput output) {
		super(output);
	}

	public static HashMap<String, ShapedRecipeJsonBuilder> shaped_recipes = new HashMap<>();
	public static HashMap<String, ShapelessRecipeJsonBuilder> shapeless_recipes = new HashMap<>();
    public static Item[] stat_syrups = new Item[] {
        ItemRegistry.APPLE_SYRUP, 
            ItemRegistry.MELON_SYRUP, 
            ItemRegistry.SUGAR_SYRUP,
            ItemRegistry.CACTUS_SYRUP
    };
    public static Item[] misc_syrups = new Item[] {
        ItemRegistry.BERRY_SYRUP, 
        ItemRegistry.HONEY_SYRUP 
    };
    public static Item[] mags = new Item[] { Items.IRON_INGOT, Items.GOLD_INGOT, Items.DIAMOND, Items.NETHERITE_SCRAP };

    @Override
    public void generate(Consumer<RecipeJsonProvider> exporter) {

        // GENERATE ASH WOOD RECIPES (very automatically)
        RecipeProvider.generateFamily(exporter, BlockRegistry.ASH_FAMILY);

        RecipeProvider.offerPlanksRecipe2(exporter, BlockRegistry.ASH_PLANKS, BeetleItemTagGenerator.ASH_LOGS, 4);
        RecipeProvider.offerBarkBlockRecipe(exporter, BlockRegistry.ASH_WOOD, BlockRegistry.ASH_LOG);
        RecipeProvider.offerBarkBlockRecipe(exporter, BlockRegistry.ASH_WOOD_STRIPPED, BlockRegistry.ASH_LOG_STRIPPED);

        // GENERATE JELLY RECIPES (very manually)
        for (int syrup_dex = 0; syrup_dex < stat_syrups.length; syrup_dex++) {
            for (int mag_dex = 0; mag_dex < mags.length; mag_dex++) {
                for (int dir_dex = 0; dir_dex < 2; dir_dex++) {
                    JellyMixRecipeJsonBuilder recipe = JellyMixRecipeJsonBuilder
                        .create(RecipeCategory.FOOD, ItemRegistry.BEETLE_JELLY)
                        .input(Ingredient.ofItems(ItemRegistry.GELATIN))
                        .criterion(RecipeProvider.hasItem(ItemRegistry.GELATIN),
                                RecipeProvider.conditionsFromItem(ItemRegistry.GELATIN));
                    recipe.input(Ingredient.ofItems(stat_syrups[syrup_dex]));
                    recipe.input(Ingredient.ofItems(mags[mag_dex]));
                    if (dir_dex == 0) {
                        recipe.input(Ingredient.ofItems(Items.GRASS));
                    } else {
                        recipe.input(Ingredient.ofItems(Items.TALL_GRASS));
                    }

                    recipe.offerTo(exporter,
                            new Identifier("beetlebox", "beetle_jelly_" + syrup_dex + mag_dex + dir_dex));
                }
            }
        }
        for (int syrup_dex = 0; syrup_dex < misc_syrups.length; syrup_dex++) {
                    JellyMixRecipeJsonBuilder recipe = JellyMixRecipeJsonBuilder
                        .create(RecipeCategory.FOOD, ItemRegistry.BEETLE_JELLY)
                        .input(Ingredient.ofItems(ItemRegistry.GELATIN))
                        .criterion(RecipeProvider.hasItem(ItemRegistry.GELATIN),
                                RecipeProvider.conditionsFromItem(ItemRegistry.GELATIN));
                    recipe.input(Ingredient.ofItems(misc_syrups[syrup_dex]));
                    recipe.offerTo(exporter,
                            new Identifier("beetlebox", "beetle_jelly_" + syrup_dex+stat_syrups.length));
        }

        // IMPORT ANY EXTERNALLY QUEUED RECIPES
        for (String s : shaped_recipes.keySet()) {
            ShapedRecipeJsonBuilder recipe = shaped_recipes.get(s);
            recipe.offerTo(exporter, s);
        }
        for (String s : shapeless_recipes.keySet()) {
            ShapelessRecipeJsonBuilder recipe = shapeless_recipes.get(s);
            recipe.offerTo(exporter, s);
        }

        //###########
        //## TOOLS ##
        //###########
        //JAR
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.BEETLE_JAR).pattern(" l ").pattern("gjg")
            .pattern(" g ")
            .input('l', Ingredient.fromTag(TagKey.of(RegistryKeys.ITEM, new Identifier("minecraft", "logs"))))
            .input('g', Items.GLASS_PANE).input('j', Items.LEAD).criterion(RecipeProvider.hasItem(ItemRegistry.NET),
                    RecipeProvider.conditionsFromItem(ItemRegistry.NET))
            .offerTo(exporter);

        //SUBSTRATE
        ShapelessRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.LARVA_JAR)
            .input(ItemRegistry.BEETLE_JAR, 1)
            .input(ItemRegistry.SUBSTRATE, 1)
            .criterion(RecipeProvider.hasItem(BlockRegistry.ASH_LOG),
                    RecipeProvider.conditionsFromItem(BlockRegistry.ASH_LOG))
            .offerTo(exporter);

        //LEGENDARY JAR
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.LEG_BEETLE_JAR).pattern("dod").pattern("eje")
            .pattern("ded").input('o', Items.OBSIDIAN).input('e', Items.ENDER_EYE).input('d', Items.DIAMOND)
            .input('j', ItemRegistry.BEETLE_JAR).criterion(RecipeProvider.hasItem(ItemRegistry.NET),
                    RecipeProvider.conditionsFromItem(ItemRegistry.NET))
            .offerTo(exporter);

        //BEETLEPACK
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.BEETLEPACK)
            .pattern("olo")
            .pattern("hjh")
            .pattern("olo")
            .input('o', Items.OBSIDIAN)
            .input('j', ItemRegistry.BEETLE_JAR)
            .input('h', ItemRegistry.BEETLE_HUSK)
            .input('l', Items.LEATHER)
            .criterion(RecipeProvider.hasItem(ItemRegistry.BEETLE_HUSK),
                    RecipeProvider.conditionsFromItem(ItemRegistry.BEETLE_HUSK))
            .offerTo(exporter);

        //BUGSLINGER
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.BUGSLINGER)
            .pattern("hhb")
            .pattern("ojh")
            .pattern("lib")
            .input('h', ItemRegistry.BEETLE_HUSK)
            .input('b', Items.IRON_BARS)
            .input('o', Items.OBSIDIAN)
            .input('j', ItemRegistry.BEETLE_JAR)
            .input('l', Ingredient.fromTag(BeetleItemTagGenerator.ASH_LOGS))
            .input('i', Items.IRON_INGOT)
            .criterion(RecipeProvider.hasItem(ItemRegistry.BEETLE_HUSK),
                    RecipeProvider.conditionsFromItem(ItemRegistry.BEETLE_HUSK))
            .offerTo(exporter);

        //NET
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, ItemRegistry.NET).pattern(" sw").pattern(" ss")
            .pattern("s  ").input('s', Items.STICK)
            .input('w', Ingredient.fromTag(TagKey.of(RegistryKeys.ITEM, new Identifier("minecraft", "wool"))))
            .criterion(RecipeProvider.hasItem(Items.STICK), RecipeProvider.conditionsFromItem(Items.STICK))
            .offerTo(exporter);


        //##############
        //## MACHINES ##
        //##############
        //TANK
        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, BlockRegistry.TANK).pattern("iii").pattern("gjg")
            .pattern("iii").input('i', Items.IRON_INGOT).input('g', Items.GLASS_PANE)
            .input('j', ItemRegistry.BEETLE_JAR).criterion(RecipeProvider.hasItem(ItemRegistry.NET),
                    RecipeProvider.conditionsFromItem(ItemRegistry.NET))
            .offerTo(exporter);

        /* LEGENDARY TANK - currently disabled
           ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, BlockRegistry.LEG_TANK).pattern("idi").pattern("gjg")
           .pattern("idi").input('i', Items.GOLD_INGOT).input('d', Items.DIAMOND).input('g', Items.GLASS_PANE)
           .input('j', ItemRegistry.LEG_BEETLE_JAR).criterion(RecipeProvider.hasItem(ItemRegistry.NET),
           RecipeProvider.conditionsFromItem(ItemRegistry.NET))
           .offerTo(exporter);
           */

        //BOILER
        ShapedRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, BlockRegistry.BOILER).pattern("b b")
            .pattern("bcb").pattern("bib").input('b', Items.BRICK).input('c', Items.CAULDRON)
            .input('i', Items.IRON_BARS)
            .criterion(RecipeProvider.hasItem(Blocks.CAULDRON), RecipeProvider.conditionsFromItem(Blocks.CAULDRON))
            .offerTo(exporter);

        //IMMIGRATOR
        ShapedRecipeJsonBuilder.create(RecipeCategory.DECORATIONS, BlockRegistry.IMMIGRATOR).pattern("ia ")
            .pattern("ipg").pattern("ia ").input('i', Items.IRON_INGOT).input('a', BlockRegistry.ASH_PLANKS)
            .input('g', Items.GLASS).input('p', Items.STICKY_PISTON)
            .criterion(RecipeProvider.hasItem(BlockRegistry.TANK),
                    RecipeProvider.conditionsFromItem(BlockRegistry.TANK))
            .offerTo(exporter);

        //EMIGRATOR
        ShapedRecipeJsonBuilder.create(RecipeCategory.DECORATIONS, BlockRegistry.EMIGRATOR).pattern("ia ")
            .pattern("ipg").pattern("ia ").input('i', Items.IRON_INGOT).input('a', BlockRegistry.ASH_PLANKS)
            .input('g', Items.GLASS).input('p', Items.PISTON).criterion(RecipeProvider.hasItem(BlockRegistry.TANK),
                    RecipeProvider.conditionsFromItem(BlockRegistry.TANK))
            .offerTo(exporter);

        //INCUBATOR
        ShapedRecipeJsonBuilder.create(RecipeCategory.DECORATIONS, BlockRegistry.INCUBATOR).pattern("lbl")
            .pattern("hbh").pattern("iti").input('l', Items.REDSTONE_LAMP).input('b', Items.IRON_BARS)
            .input('h', ItemRegistry.BEETLE_HUSK).input('t', Items.REDSTONE_TORCH).input('i', Items.IRON_INGOT)
            .criterion(RecipeProvider.hasItem(ItemRegistry.LARVA_JAR),
                    RecipeProvider.conditionsFromItem(ItemRegistry.LARVA_JAR))
            .offerTo(exporter);

        //#############################
        //## CRAFTING INTERMEDIARIES ##
        //#############################
        //GELATIN GLUE
        offerSmelting(exporter, List.of(ItemRegistry.SUGAR_GELATIN), RecipeCategory.MISC, ItemRegistry.GELATIN_GLUE,
                0.7f, 200, "gelatin_glue");
        //SUGAR GELATIN
        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, ItemRegistry.SUGAR_GELATIN)
            .input(Items.SUGAR, 2)
            .input(ItemRegistry.GELATIN, 2)
            .criterion(RecipeProvider.hasItem(ItemRegistry.GELATIN),
                    RecipeProvider.conditionsFromItem(ItemRegistry.GELATIN))
            .offerTo(exporter);

        //SUBSTRATE
        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, ItemRegistry.SUBSTRATE)
            .input(Ingredient.fromTag(BeetleItemTagGenerator.ASH_LOGS), 1)
            .input(Ingredient.fromTag(TagKey.of(RegistryKeys.ITEM, new Identifier("minecraft", "logs"))), 3)
            .criterion(RecipeProvider.hasItem(BlockRegistry.ASH_LOG),
                    RecipeProvider.conditionsFromItem(BlockRegistry.ASH_LOG))
            .offerTo(exporter);

        ShapelessRecipeJsonBuilder.create(RecipeCategory.MISC, Items.SLIME_BALL)
            .input(Ingredient.ofItems(ItemRegistry.GELATIN_GLUE),1)
            .input(Items.LIME_DYE)
            .criterion(RecipeProvider.hasItem(ItemRegistry.GELATIN),
                    RecipeProvider.conditionsFromItem(ItemRegistry.GELATIN))
            .offerTo(exporter);
    }
}
