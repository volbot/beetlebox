package org.volbot.beetlebox.data;

import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.minecraft.registry.RegistryBuilder;
import net.minecraft.registry.RegistryKeys;
import org.volbot.beetlebox.data.damage.BeetleDamageTypes;
import org.volbot.beetlebox.data.lang.BeetleEnglishProvider;
import org.volbot.beetlebox.data.loot.BeetleBlockLootGenerator;
import org.volbot.beetlebox.data.loot.BeetleLootGenerator;
import org.volbot.beetlebox.data.models.BeetleModelGenerator;
import org.volbot.beetlebox.data.recipe.BeetleRecipeGenerator;
import org.volbot.beetlebox.data.tags.BeetleItemTagGenerator;
import org.volbot.beetlebox.data.tags.BeetleBlockTagGenerator;
import org.volbot.beetlebox.data.tags.BeetleDamageTagGenerator;
import org.volbot.beetlebox.data.tags.BeetleEntityTagGenerator;
import org.volbot.beetlebox.registry.ConfiguredFeatureRegistry;
import org.volbot.beetlebox.registry.PlacedFeatureRegistry;

public class BeetleDataGen implements DataGeneratorEntrypoint {
	
	@Override
	public void onInitializeDataGenerator(FabricDataGenerator fabricDataGenerator) {
		FabricDataGenerator.Pack pack = fabricDataGenerator.createPack();
		pack.addProvider(BeetleModelGenerator::new);
		pack.addProvider(JsonDataGenerator::new);
		pack.addProvider(BeetleItemTagGenerator::new);
		pack.addProvider(BeetleBlockTagGenerator::new);
		pack.addProvider(BeetleEntityTagGenerator::new);
		pack.addProvider(BeetleDamageTagGenerator::new);
		pack.addProvider(BeetleRecipeGenerator::new);
		pack.addProvider(BeetleLootGenerator::new);
		pack.addProvider(BeetleBlockLootGenerator::new);
		pack.addProvider(BeetleEnglishProvider::new);
	}

	@Override
	public void buildRegistry(RegistryBuilder registryBuilder) {
		registryBuilder.addRegistry(RegistryKeys.DAMAGE_TYPE, BeetleDamageTypes::bootstrap);
		registryBuilder.addRegistry(RegistryKeys.CONFIGURED_FEATURE, ConfiguredFeatureRegistry::bootstrap);
		registryBuilder.addRegistry(RegistryKeys.PLACED_FEATURE, PlacedFeatureRegistry::bootstrap);
	}


}
