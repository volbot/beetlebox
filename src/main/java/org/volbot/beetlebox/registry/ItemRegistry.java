package org.volbot.beetlebox.registry;

import java.util.HashMap;
import java.util.Vector;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import org.volbot.beetlebox.client.render.gui.BeetlepackScreenHandler;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.item.BeetleJelly;
import org.volbot.beetlebox.item.FruitSyrup;
import org.volbot.beetlebox.item.GelatinPowder;
import org.volbot.beetlebox.item.BeetleHusk;
import org.volbot.beetlebox.item.equipment.BeetlepackItem;
import org.volbot.beetlebox.item.equipment.materials.BeetleItemUpgrade;
import org.volbot.beetlebox.item.equipment.materials.DormantUpgrade;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.LarvaJarItem;
import org.volbot.beetlebox.item.tools.NetItem;
import org.volbot.beetlebox.item.tools.BugslingerItem;

public class ItemRegistry {

    public static final Item BEETLE_JELLY = new BeetleJelly(new FabricItemSettings());

    public static Vector<Item> beetle_helmets = new Vector<>();
    public static Vector<Item> spawn_eggs = new Vector<>();
    public static Vector<Item> armor_sets = new Vector<>();
    public static Vector<Item> beetle_drops = new Vector<>();

    public static Vector<Item> helmet_upgrades = new Vector<>();
    public static Vector<Item> chest_upgrades = new Vector<>();
    public static Vector<Item> legs_upgrades = new Vector<>();
    public static Vector<Item> boots_upgrades = new Vector<>();

    public static HashMap<String,Item> UPGRADE_MAP = new HashMap<>();

    public static final Item SUBSTRATE = new Item(new FabricItemSettings());
    public static final Item LARVA_JAR = new LarvaJarItem(new FabricItemSettings());

    public static final Item BEETLE_HUSK = new BeetleHusk(new FabricItemSettings());

    public static final Item GELATIN = new GelatinPowder(new FabricItemSettings());
    public static final Item SUGAR_GELATIN = new Item(new FabricItemSettings());
    public static final Item GELATIN_GLUE = new Item(new FabricItemSettings());

    public static ArmorItem BEETLEPACK;        
    public static Item BUGSLINGER = new BugslingerItem(new FabricItemSettings());        
    
    public static final Item APPLE_SYRUP = new FruitSyrup(new FabricItemSettings());
    public static final Item MELON_SYRUP = new FruitSyrup(new FabricItemSettings());
    public static final Item BERRY_SYRUP = new FruitSyrup(new FabricItemSettings());
    public static final Item SUGAR_SYRUP = new FruitSyrup(new FabricItemSettings());
    public static final Item CACTUS_SYRUP = new FruitSyrup(new FabricItemSettings());
    public static final Item HONEY_SYRUP = new FruitSyrup(new FabricItemSettings());

    public static final Item UPGRADE_DORMANT = new DormantUpgrade(new FabricItemSettings());
    public static final Item UPGRADE_H_ATTACK = new BeetleItemUpgrade("beetle_helmet_attack", EquipmentSlot.HEAD,
            new FabricItemSettings());
    public static final Item UPGRADE_H_NV = new BeetleItemUpgrade("beetle_helmet_nv", EquipmentSlot.HEAD,
            new FabricItemSettings());
    public static final Item UPGRADE_C_ELYTRA = new BeetleItemUpgrade("beetle_chest_elytra", EquipmentSlot.CHEST,
            new FabricItemSettings());
    public static final Item UPGRADE_C_BOOST = new BeetleItemUpgrade("beetle_chest_boost", EquipmentSlot.CHEST,
            new FabricItemSettings());
    public static final Item UPGRADE_L_CLIMB = new BeetleItemUpgrade("beetle_legs_wallclimb", EquipmentSlot.LEGS,
            new FabricItemSettings());
    public static final Item UPGRADE_L_2JUMP = new BeetleItemUpgrade("beetle_legs_2jump", EquipmentSlot.LEGS,
            new FabricItemSettings());
    public static final Item UPGRADE_B_FALLDAM = new BeetleItemUpgrade("beetle_boots_falldamage", EquipmentSlot.FEET,
            new FabricItemSettings());
    public static final Item UPGRADE_B_SPEED = new BeetleItemUpgrade("beetle_boots_speed", EquipmentSlot.FEET,
            new FabricItemSettings());
    public static final Item UPGRADE_B_STEP = new BeetleItemUpgrade("beetle_boots_step", EquipmentSlot.FEET,
            new FabricItemSettings());

    public static final Item NET = new NetItem(new FabricItemSettings());

    public static final Item BEETLE_JAR = new BeetleJarItem<BeetleEntity>(new FabricItemSettings(), BeetleEntity.class);
    public static final Item LEG_BEETLE_JAR = new BeetleJarItem<LivingEntity>(
            new FabricItemSettings().rarity(Rarity.UNCOMMON), LivingEntity.class);

    public static void register() {

    helmet_upgrades.add(UPGRADE_H_ATTACK);
    helmet_upgrades.add(UPGRADE_H_NV);
    chest_upgrades.add(UPGRADE_C_ELYTRA);
    chest_upgrades.add(UPGRADE_C_BOOST);
    legs_upgrades.add(UPGRADE_L_CLIMB);
    legs_upgrades.add(UPGRADE_L_2JUMP);
    boots_upgrades.add(UPGRADE_B_FALLDAM);
    boots_upgrades.add(UPGRADE_B_SPEED);
    boots_upgrades.add(UPGRADE_B_STEP);

    UPGRADE_MAP.put("beetle_helmet_attack",UPGRADE_H_ATTACK);
    UPGRADE_MAP.put("beetle_helmet_nv",UPGRADE_H_NV);
    UPGRADE_MAP.put("beetle_chest_elytra",UPGRADE_C_ELYTRA);
    UPGRADE_MAP.put("beetle_chest_boost",UPGRADE_C_BOOST);
    UPGRADE_MAP.put("beetle_legs_2jump",UPGRADE_L_2JUMP);
    UPGRADE_MAP.put("beetle_legs_wallclimb",UPGRADE_L_CLIMB);
    UPGRADE_MAP.put("beetle_boots_falldamage",UPGRADE_B_FALLDAM);
    UPGRADE_MAP.put("beetle_boots_speed",UPGRADE_B_SPEED);
    UPGRADE_MAP.put("beetle_boots_step",UPGRADE_B_STEP);

       if (!FabricLoader.getInstance().isModLoaded("trinkets")) {
           BEETLEPACK = new BeetlepackItem(new FabricItemSettings());
           Registry.register(Registries.ITEM, new Identifier("beetlebox", "beetlepack"), BEETLEPACK);
       }
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "bugslinger"), BUGSLINGER);

    Registry.register(Registries.SCREEN_HANDLER, new Identifier("beetlebox", "beetlepack"),
            BeetlepackScreenHandler.BEETLEPACK_SCREEN_HANDLER_TYPE);


    Registry.register(Registries.ITEM, new Identifier("beetlebox", "substrate"), SUBSTRATE);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "larva_jar"), LARVA_JAR);

    Registry.register(Registries.ITEM, new Identifier("beetlebox", "beetle_husk"), BEETLE_HUSK);

    Registry.register(Registries.ITEM, new Identifier("beetlebox", "beetle_jar"), BEETLE_JAR);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "leg_beetle_jar"), LEG_BEETLE_JAR);

    Registry.register(Registries.ITEM, new Identifier("beetlebox", "net"), NET);
    
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "gelatin"), GELATIN);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "sugar_gelatin"), SUGAR_GELATIN);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "gelatin_glue"), GELATIN_GLUE);

    Registry.register(Registries.ITEM, new Identifier("beetlebox", "apple_syrup"), APPLE_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "melon_syrup"), MELON_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "berry_syrup"), BERRY_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "sugar_syrup"), SUGAR_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "cactus_syrup"), CACTUS_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "honey_syrup"), HONEY_SYRUP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "beetle_jelly"), BEETLE_JELLY);

    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_dormant"), UPGRADE_DORMANT);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_h_attack"), UPGRADE_H_ATTACK);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_h_nv"), UPGRADE_H_NV);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_c_elytra"), UPGRADE_C_ELYTRA);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_c_boost"), UPGRADE_C_BOOST);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_l_climb"), UPGRADE_L_CLIMB);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_l_2jump"), UPGRADE_L_2JUMP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_b_falldam"), UPGRADE_B_FALLDAM);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_b_step"), UPGRADE_B_STEP);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "upgrade_b_speed"), UPGRADE_B_SPEED);

}

}
