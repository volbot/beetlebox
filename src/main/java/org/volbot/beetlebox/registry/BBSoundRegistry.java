package org.volbot.beetlebox.registry;

import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;

public class BBSoundRegistry {

    public static final SoundEvent BEETLE_HIT = registerSoundEvent(new Identifier("beetlebox","beetle_hit"));
    public static final SoundEvent BEETLE_HURT = registerSoundEvent(new Identifier("beetlebox","beetle_hurt"));
    public static final SoundEvent BEETLE_FLY = registerSoundEvent(new Identifier("beetlebox","beetle_fly"));

    public static void register() {
        return;
    }

	public static SoundEvent registerSoundEvent(Identifier id) {
		return Registry.register(Registries.SOUND_EVENT, id, SoundEvent.of(id));
	}
}
