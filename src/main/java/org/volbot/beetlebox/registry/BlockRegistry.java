package org.volbot.beetlebox.registry;

import java.util.ArrayList;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.transfer.v1.fluid.FluidStorage;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSetType;
import net.minecraft.block.Blocks;
import net.minecraft.block.ButtonBlock;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.DoorBlock;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.FenceGateBlock;
import net.minecraft.block.LeavesBlock;
import net.minecraft.block.PillarBlock;
import net.minecraft.block.PressurePlateBlock;
import net.minecraft.block.PressurePlateBlock.ActivationRule;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.StairsBlock;
import net.minecraft.block.TrapdoorBlock;
import net.minecraft.block.WoodType;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.data.family.BlockFamilies;
import net.minecraft.data.family.BlockFamily;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.block.BeetleTankBlock;
import org.volbot.beetlebox.block.BoilerBlock;
import org.volbot.beetlebox.block.EmigratorBlock;
import org.volbot.beetlebox.block.ImmigratorBlock;
import org.volbot.beetlebox.block.IncubatorBlock;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.block.BoilerBlockEntity;
import org.volbot.beetlebox.entity.block.EmigratorBlockEntity;
import org.volbot.beetlebox.entity.block.ImmigratorBlockEntity;
import org.volbot.beetlebox.entity.block.IncubatorBlockEntity;
import org.volbot.beetlebox.entity.block.TankBlockEntity;
import org.volbot.beetlebox.entity.storage.EntityPlacementDispenserBehavior;
import org.volbot.beetlebox.item.block.BoilerItem;
import org.volbot.beetlebox.worldgen.AshSaplingGenerator;

public class BlockRegistry {

  public static final ArrayList<Item> machines = new ArrayList<>();
  public static final ArrayList<Item> ash_blocks = new ArrayList<>();

  // ------------
  // - MACHINES -
  // ------------
  public static final Block TANK =
      registerBlock(
          "tank",
          new BeetleTankBlock<BeetleEntity>(
              FabricBlockSettings.copy(Blocks.IRON_ORE).nonOpaque(), BeetleEntity.class),
          BB_BLOCK_TYPE.MACHINE);

  public static final BoilerBlock BOILER = new BoilerBlock(FabricBlockSettings.copy(Blocks.STONE));
  public static final BoilerItem BOILER_ITEM = new BoilerItem(BOILER, new FabricItemSettings());

  public static final Block INCUBATOR =
      registerBlock(
          "incubator",
          new IncubatorBlock(FabricBlockSettings.copy(Blocks.STONE)),
          BB_BLOCK_TYPE.MACHINE);

  public static final Block EMIGRATOR =
      registerBlock(
          "emigrator",
          new EmigratorBlock(FabricBlockSettings.copy(Blocks.OAK_PLANKS)),
          BB_BLOCK_TYPE.MACHINE);
  public static final Block IMMIGRATOR =
      registerBlock(
          "immigrator",
          new ImmigratorBlock(FabricBlockSettings.copy(Blocks.OAK_PLANKS)),
          BB_BLOCK_TYPE.MACHINE);

  // -----------------
  // - TILE ENTITIES -
  // -----------------
  public static final BlockEntityType<TankBlockEntity> TANK_BLOCK_ENTITY =
      Registry.register(
          Registries.BLOCK_ENTITY_TYPE,
          new Identifier("beetlebox", "tank_block_entity"),
          FabricBlockEntityTypeBuilder.create(TankBlockEntity::new, TANK).build());

  public static final BlockEntityType<BoilerBlockEntity> BOILER_BLOCK_ENTITY =
      Registry.register(
          Registries.BLOCK_ENTITY_TYPE,
          new Identifier("beetlebox", "boiler_block_entity"),
          FabricBlockEntityTypeBuilder.create(BoilerBlockEntity::new, BOILER).build());

  public static final BlockEntityType<EmigratorBlockEntity> EMIGRATOR_BLOCK_ENTITY =
      Registry.register(
          Registries.BLOCK_ENTITY_TYPE,
          new Identifier("beetlebox", "emigrator_block_entity"),
          FabricBlockEntityTypeBuilder.create(EmigratorBlockEntity::new, EMIGRATOR).build());

  public static final BlockEntityType<ImmigratorBlockEntity> IMMIGRATOR_BLOCK_ENTITY =
      Registry.register(
          Registries.BLOCK_ENTITY_TYPE,
          new Identifier("beetlebox", "immigrator_block_entity"),
          FabricBlockEntityTypeBuilder.create(ImmigratorBlockEntity::new, IMMIGRATOR).build());

  public static final BlockEntityType<IncubatorBlockEntity> INCUBATOR_BLOCK_ENTITY =
      Registry.register(
          Registries.BLOCK_ENTITY_TYPE,
          new Identifier("beetlebox", "incubator_block_entity"),
          FabricBlockEntityTypeBuilder.create(IncubatorBlockEntity::new, INCUBATOR).build());

  // ------------
  // - ASH WOOD -
  // ------------
  public static final BlockSetType ASH_BLOCKSET = BlockSetType.register(new BlockSetType("ash"));
  public static final WoodType ASH_WOOD_TYPE =
      WoodType.register(
          new WoodType(
              "ash",
              BlockSetType.BIRCH,
              BlockSoundGroup.WOOD,
              BlockSoundGroup.HANGING_SIGN,
              SoundEvents.BLOCK_FENCE_GATE_CLOSE,
              SoundEvents.BLOCK_FENCE_GATE_OPEN));

  public static final Block ASH_LOG =
      registerBlock(
          "ash_log",
          new PillarBlock(FabricBlockSettings.copy(Blocks.BIRCH_LOG)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_WOOD =
      registerBlock(
          "ash_wood",
          new PillarBlock(FabricBlockSettings.copy(Blocks.BIRCH_WOOD)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_LOG_STRIPPED =
      registerBlock(
          "ash_log_stripped",
          new PillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_BIRCH_LOG)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_WOOD_STRIPPED =
      registerBlock(
          "ash_wood_stripped",
          new PillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_BIRCH_WOOD)),
          BB_BLOCK_TYPE.WOOD);

  public static final Block ASH_PLANKS =
      registerBlock(
          "ash_planks",
          new Block(FabricBlockSettings.copy(Blocks.BIRCH_PLANKS)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_LEAVES =
      registerBlock(
          "ash_leaves",
          new LeavesBlock(FabricBlockSettings.copy(Blocks.BIRCH_LEAVES)),
          BB_BLOCK_TYPE.WOOD);

  public static final Block ASH_STAIRS =
      registerBlock(
          "ash_stairs",
          new StairsBlock(
              ASH_PLANKS.getDefaultState(), FabricBlockSettings.copy(Blocks.OAK_STAIRS)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_SLAB =
      registerBlock(
          "ash_slab", new SlabBlock(FabricBlockSettings.copy(Blocks.OAK_SLAB)), BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_FENCE =
      registerBlock(
          "ash_fence",
          new FenceBlock(FabricBlockSettings.copy(Blocks.OAK_FENCE)),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_FENCE_GATE =
      registerBlock(
          "ash_fence_gate",
          new FenceGateBlock(FabricBlockSettings.copy(Blocks.OAK_FENCE_GATE), ASH_WOOD_TYPE),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_DOOR =
      registerBlock(
          "ash_door",
          new DoorBlock(FabricBlockSettings.copy(Blocks.OAK_DOOR), ASH_BLOCKSET),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_BUTTON =
      registerBlock(
          "ash_button",
          new ButtonBlock(FabricBlockSettings.copy(Blocks.OAK_DOOR), ASH_BLOCKSET, 30, true),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_PLATE =
      registerBlock(
          "ash_plate",
          new PressurePlateBlock(
              ActivationRule.EVERYTHING,
              FabricBlockSettings.copy(Blocks.OAK_PRESSURE_PLATE),
              ASH_BLOCKSET),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_TRAPDOOR =
      registerBlock(
          "ash_trapdoor",
          new TrapdoorBlock(FabricBlockSettings.copy(Blocks.OAK_TRAPDOOR), ASH_BLOCKSET),
          BB_BLOCK_TYPE.WOOD);
  public static final Block ASH_SAPLING =
      registerBlock(
          "ash_sapling",
          new SaplingBlock(new AshSaplingGenerator(), FabricBlockSettings.copy(Blocks.OAK_SAPLING)),
          BB_BLOCK_TYPE.WOOD);

  public static final BlockFamily ASH_FAMILY =
      BlockFamilies.register(ASH_PLANKS)
          .button(ASH_BUTTON)
          .fence(ASH_FENCE)
          .fenceGate(ASH_FENCE_GATE)
          .door(ASH_DOOR)
          .pressurePlate(ASH_PLATE)
          .slab(ASH_SLAB)
          .stairs(ASH_STAIRS)
          .trapdoor(ASH_TRAPDOOR)
          .group("wooden")
          .unlockCriterionName("has_planks")
          .build();

  public static void register() {

    // register sided fluid storage for boiler
    FluidStorage.SIDED.registerForBlockEntity(
        (boiler, direction) -> boiler.fluidStorage, BOILER_BLOCK_ENTITY);

    // register boiler objects (cannot use helper due to custom class)
    Registry.register(Registries.BLOCK, new Identifier("beetlebox", "boiler"), BOILER);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", "boiler"), BOILER_ITEM);
    machines.add(BOILER_ITEM);

    // register dispenser behavior
    DispenserBlock.registerBehavior(
        ItemRegistry.BEETLE_JAR, new EntityPlacementDispenserBehavior());
    DispenserBlock.registerBehavior(
        ItemRegistry.LEG_BEETLE_JAR, new EntityPlacementDispenserBehavior());
  }

  private static Block registerBlock(String name, Block block, BB_BLOCK_TYPE type) {
    registerBlockItem(name, block, type);
    return Registry.register(Registries.BLOCK, new Identifier("beetlebox", name), block);
  }

  private static Item registerBlockItem(String name, Block block, BB_BLOCK_TYPE type) {
    Item item =
        Registry.register(
            Registries.ITEM,
            new Identifier("beetlebox", name),
            new BlockItem(block, new FabricItemSettings()));
    switch (type) {
      case MACHINE:
        machines.add(item);
        break;
      case WOOD:
        ash_blocks.add(item);
        break;
    }
    return item;
  }

  enum BB_BLOCK_TYPE {
    MACHINE,
    WOOD
  }
}
