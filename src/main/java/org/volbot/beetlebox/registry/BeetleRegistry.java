package org.volbot.beetlebox.registry;

import java.util.Vector;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.data.server.recipe.RecipeProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.LootingEnchantLootFunction;
import net.minecraft.loot.provider.number.UniformLootNumberProvider;
import net.minecraft.recipe.book.RecipeCategory;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.data.lang.BeetleEnglishProvider;
import org.volbot.beetlebox.data.loot.BeetleLootGenerator;
import org.volbot.beetlebox.data.recipe.BeetleRecipeGenerator;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleTypes.BeetleType;
import org.volbot.beetlebox.entity.projectile.BeetleProjectileEntity;
import org.volbot.beetlebox.item.equipment.BeetleArmorAbilities;

public class BeetleRegistry {

  public static final EntityType<BeetleProjectileEntity> BEETLE_PROJECTILE =
      FabricEntityTypeBuilder.<BeetleProjectileEntity>create(
              SpawnGroup.MISC, BeetleProjectileEntity::new)
          .trackedUpdateRate(20)
          .trackRangeBlocks(4)
          .dimensions(EntityDimensions.fixed(0.5f, 0.5f))
          .build();

  public static Vector<EntityType<? extends BeetleEntity>> beetles =
      new Vector<EntityType<? extends BeetleEntity>>();

  public static void register() {
    Registry.register(
        Registries.ENTITY_TYPE,
        new Identifier("beetlebox", "beetle_projectile"),
        BEETLE_PROJECTILE);

    // ENTITIES
    for (BeetleType beetle_type : BeetleType.values()) {
      registerBeetle(beetle_type);
    }
  }

  public static void registerBeetle(BeetleType beetle_type) {

    // register entity type
    BeetleRegistry.beetles.add(
        Registry.register(
            Registries.ENTITY_TYPE,
            new Identifier("beetlebox", beetle_type.id()),
            beetle_type.entityType()));
    FabricDefaultAttributeRegistry.register(
        beetle_type.entityType(), BeetleEntity.createBeetleAttributes(beetle_type));

    registerBeetleItems(beetle_type);
  }

  public static void registerBeetleItems(BeetleType beetle_type) {
    String beetle_id = beetle_type.id();
    String beetle_name = beetle_type.nameCommon();

    // spawn egg
    Item SPAWN_EGG = beetle_type.spawnEgg();
    Registry.register(
        Registries.ITEM, new Identifier("beetlebox", beetle_id + "_spawn_egg"), SPAWN_EGG);
    ItemRegistry.spawn_eggs.add(SPAWN_EGG);

    // elytron
    Item ELYTRON = beetle_type.elytron();
    Registry.register(
        Registries.ITEM, new Identifier("beetlebox", beetle_id + "_elytron"), ELYTRON);
    ItemRegistry.beetle_drops.add(ELYTRON);

    BeetleLootGenerator.beetle_loot.put(beetle_id, createLootTable(ELYTRON));

    // armor
    Item[] armor = beetle_type.armorItems();
    Registry.register(
        Registries.ITEM, new Identifier("beetlebox", beetle_id + "_helmet"), armor[0]);
    Registry.register(
        Registries.ITEM, new Identifier("beetlebox", beetle_id + "_chestplate"), armor[1]);
    Registry.register(
        Registries.ITEM, new Identifier("beetlebox", beetle_id + "_leggings"), armor[2]);
    Registry.register(Registries.ITEM, new Identifier("beetlebox", beetle_id + "_boots"), armor[3]);
    for (Item item : armor) {
      ItemRegistry.armor_sets.add(item);
    }
    ItemRegistry.beetle_helmets.add(armor[0]);

    // armor recipes
    BeetleRecipeGenerator.shaped_recipes.put(
        beetle_id + "_helmet", createHelmetRecipe(armor[0], ELYTRON));
    BeetleRecipeGenerator.shaped_recipes.put(
        beetle_id + "_chestplate", createChestplateRecipe(armor[1], ELYTRON));
    BeetleRecipeGenerator.shaped_recipes.put(
        beetle_id + "_leggings", createLegsRecipe(armor[2], ELYTRON));
    BeetleRecipeGenerator.shaped_recipes.put(
        beetle_id + "_boots", createBootsRecipe(armor[3], ELYTRON));

    BeetleArmorAbilities.beetle_abilities.put(beetle_id, beetle_type.ability().toString());

    // store data for en lang generation
    BeetleEnglishProvider.gen_lang.put(beetle_type.entityType().getTranslationKey(), beetle_name);

    BeetleEnglishProvider.gen_lang.put(armor[0].getTranslationKey(), beetle_name + " Helmet");
    BeetleEnglishProvider.gen_lang.put(armor[1].getTranslationKey(), beetle_name + " Chestplate");
    BeetleEnglishProvider.gen_lang.put(armor[2].getTranslationKey(), beetle_name + " Leggings");
    BeetleEnglishProvider.gen_lang.put(armor[3].getTranslationKey(), beetle_name + " Boots");
    BeetleEnglishProvider.gen_lang.put(ELYTRON.getTranslationKey(), beetle_name + " Elytron");
    BeetleEnglishProvider.gen_lang.put(SPAWN_EGG.getTranslationKey(), beetle_name + " Spawn Egg");
  }

  ///////////////////////
  // RECIPE GENERATORS //
  ///////////////////////

  public static ShapedRecipeJsonBuilder createChestplateRecipe(Item CHESTPLATE, Item ELYTRON) {
    return ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, CHESTPLATE)
        .pattern("eoe")
        .pattern("gcg")
        .pattern("eoe")
        .input('e', ELYTRON)
        .input('c', Items.DIAMOND_CHESTPLATE)
        .input('g', ItemRegistry.GELATIN_GLUE)
        .input('o', Items.OBSIDIAN)
        .criterion(RecipeProvider.hasItem(ELYTRON), RecipeProvider.conditionsFromItem(ELYTRON));
  }

  public static ShapedRecipeJsonBuilder createHelmetRecipe(Item HELMET, Item ELYTRON) {
    return ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, HELMET)
        .pattern("eoe")
        .pattern("ghg")
        .pattern("eoe")
        .input('e', ELYTRON)
        .input('h', Items.DIAMOND_HELMET)
        .input('g', ItemRegistry.GELATIN_GLUE)
        .input('o', Items.OBSIDIAN)
        .criterion(RecipeProvider.hasItem(ELYTRON), RecipeProvider.conditionsFromItem(ELYTRON));
  }

  public static ShapedRecipeJsonBuilder createLegsRecipe(Item LEGS, Item ELYTRON) {
    return ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, LEGS)
        .pattern("eoe")
        .pattern("glg")
        .pattern("eoe")
        .input('e', ELYTRON)
        .input('l', Items.DIAMOND_LEGGINGS)
        .input('g', ItemRegistry.GELATIN_GLUE)
        .input('o', Items.OBSIDIAN)
        .criterion(RecipeProvider.hasItem(ELYTRON), RecipeProvider.conditionsFromItem(ELYTRON));
  }

  public static ShapedRecipeJsonBuilder createBootsRecipe(Item BOOTS, Item ELYTRON) {
    return ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, BOOTS)
        .pattern("eoe")
        .pattern("gbg")
        .pattern("eoe")
        .input('e', ELYTRON)
        .input('b', Items.DIAMOND_BOOTS)
        .input('g', ItemRegistry.GELATIN_GLUE)
        .input('o', Items.OBSIDIAN)
        .criterion(RecipeProvider.hasItem(ELYTRON), RecipeProvider.conditionsFromItem(ELYTRON));
  }

  public static LootTable.Builder createLootTable(Item ELYTRON) {
    return LootTable.builder()
        .pool(
            LootPool.builder()
                .rolls(UniformLootNumberProvider.create(1.0F, 2.0F))
                .with(ItemEntry.builder(ELYTRON))
                .apply(
                    LootingEnchantLootFunction.builder(
                        UniformLootNumberProvider.create(0.0F, 3.0F))));
  }
}
