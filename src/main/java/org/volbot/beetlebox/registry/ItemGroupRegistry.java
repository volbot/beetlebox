package org.volbot.beetlebox.registry;

import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.volbot.beetlebox.data.recipe.BeetleRecipeGenerator;

public class ItemGroupRegistry {

  public static final RegistryKey<ItemGroup> ITEM_GROUP =
      RegistryKey.of(RegistryKeys.ITEM_GROUP, new Identifier("beetlebox", "beetlebox_tab"));
  public static final RegistryKey<ItemGroup> ITEM_GROUP_BEETLE =
      RegistryKey.of(RegistryKeys.ITEM_GROUP, new Identifier("beetlebox", "beetlebox_beetle_tab"));
  public static final RegistryKey<ItemGroup> ITEM_GROUP_JELLY =
      RegistryKey.of(RegistryKeys.ITEM_GROUP, new Identifier("beetlebox", "beetlebox_jelly_tab"));

  public static void register() {
    registerMainGroup();
    registerBeetleGroup();
    registerJellyGroup();
  }

  public static void registerMainGroup() {
    Registry.register(
        Registries.ITEM_GROUP,
        ITEM_GROUP,
        FabricItemGroup.builder()
            .icon(
                () -> {
                  ItemStack stack = new ItemStack(BlockRegistry.TANK);
                  return stack;
                })
            .displayName(Text.translatable("volbot.beetlebox.beetlebox_tab"))
            .entries(
                (context, entries) -> {
                  for (Item i : BlockRegistry.machines) entries.add(i);
                  entries.add(ItemRegistry.BEETLEPACK);
                  entries.add(ItemRegistry.BUGSLINGER);
                  entries.add(ItemRegistry.NET);
                  entries.add(ItemRegistry.BEETLE_JAR);
                  entries.add(ItemRegistry.LEG_BEETLE_JAR);
                  entries.add(ItemRegistry.LARVA_JAR);
                  entries.add(ItemRegistry.SUBSTRATE);
                  entries.add(ItemRegistry.BEETLE_HUSK);
                  entries.add(ItemRegistry.GELATIN);
                  entries.add(ItemRegistry.SUGAR_GELATIN);
                  entries.add(ItemRegistry.GELATIN_GLUE);
                  for (Item i : ItemRegistry.helmet_upgrades) entries.add(i);
                  for (Item i : ItemRegistry.chest_upgrades) entries.add(i);
                  for (Item i : ItemRegistry.legs_upgrades) entries.add(i);
                  for (Item i : ItemRegistry.boots_upgrades) entries.add(i);
                  for (Item i : BlockRegistry.ash_blocks) entries.add(i);
                })
            .build());
  }

  public static void registerBeetleGroup() {
    Registry.register(
        Registries.ITEM_GROUP,
        ITEM_GROUP_BEETLE,
        FabricItemGroup.builder()
            .icon(
                () -> {
                  ItemStack stack = new ItemStack(ItemRegistry.LARVA_JAR);
                  var nbt = stack.getOrCreateNbt();
                  nbt.putString("EntityType", "SOPHIE");
                  stack.setNbt(nbt);

                  return stack;
                })
            .displayName(Text.translatable("volbot.beetlebox.beetlebox_beetle_tab"))
            .entries(
                (context, entries) -> {
                  for (Item i : ItemRegistry.spawn_eggs) {
                    entries.add(i);
                  }
                  for (Item i : ItemRegistry.beetle_drops) {
                    entries.add(i);
                  }
                  for (Item i : ItemRegistry.armor_sets) {
                    entries.add(i);
                  }
                })
            .build());
  }

  public static void registerJellyGroup() {
    Registry.register(
        Registries.ITEM_GROUP,
        ITEM_GROUP_JELLY,
        FabricItemGroup.builder()
            .icon(
                () -> {
                  ItemStack stack = new ItemStack(ItemRegistry.BEETLE_JELLY);
                  var nbt = stack.getOrCreateNbt();
                  nbt.putString("FruitType", "apple");
                  nbt.putBoolean("Increase", true);
                  nbt.putInt("Level", 3);
                  stack.setNbt(nbt);

                  return stack;
                })
            .entries(
                (context, entries) -> {
                  String[] misc_syrups = {"berry", "honey"};
                  String[] stat_syrups = {"apple", "melon", "sugar", "cactus"};
                  for (int syrup_dex = 0; syrup_dex < stat_syrups.length; syrup_dex++) {
                    entries.add(BeetleRecipeGenerator.stat_syrups[syrup_dex]);
                    for (int dir_dex = 0; dir_dex < 2; dir_dex++) {
                      for (int mag_dex = 1; mag_dex <= 4; mag_dex++) {
                        ItemStack stack = ItemRegistry.BEETLE_JELLY.getDefaultStack();
                        NbtCompound nbt = stack.getOrCreateNbt();
                        nbt.putString("FruitType", stat_syrups[syrup_dex]);
                        nbt.putBoolean("Increase", dir_dex == 0);
                        nbt.putInt("Level", mag_dex);
                        stack.setNbt(nbt);
                        entries.add(stack);
                      }
                    }
                  }
                  for (int syrup_dex = 0; syrup_dex < misc_syrups.length; syrup_dex++) {
                    entries.add(BeetleRecipeGenerator.misc_syrups[syrup_dex]);
                    ItemStack stack = ItemRegistry.BEETLE_JELLY.getDefaultStack();
                    NbtCompound nbt = stack.getOrCreateNbt();
                    nbt.putString("FruitType", misc_syrups[syrup_dex]);
                    stack.setNbt(nbt);
                    entries.add(stack);
                  }
                })
            .displayName(Text.translatable("volbot.beetlebox.beetlebox_jelly_tab"))
            .build());
  }
}
