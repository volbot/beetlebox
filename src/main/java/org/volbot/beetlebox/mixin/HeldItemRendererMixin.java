package org.volbot.beetlebox.mixin;

import java.util.Optional;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.item.HeldItemRenderer;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RotationAxis;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.volbot.beetlebox.entity.beetle.BeetleEntity;
import org.volbot.beetlebox.entity.beetle.BeetleStats.BeetleStat;
import org.volbot.beetlebox.item.tools.BugslingerItem;
import org.volbot.beetlebox.registry.ItemRegistry;

@Environment(EnvType.CLIENT)
@Mixin(HeldItemRenderer.class)
public abstract class HeldItemRendererMixin {

  @Inject(
      method =
          "renderItem(Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/item/ItemStack;Lnet/minecraft/client/render/model/json/ModelTransformationMode;ZLnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/render/VertexConsumerProvider;I)V",
      at = @At(value = "RETURN"))
  public void renderItem(
      LivingEntity entity,
      ItemStack stack,
      ModelTransformationMode renderMode,
      boolean leftHanded,
      MatrixStack matrices,
      VertexConsumerProvider vcp,
      int light,
      CallbackInfo info) {

    if (stack.isOf(ItemRegistry.BUGSLINGER)) {
      World world = entity.getWorld();
      Optional<Entity> opt = BugslingerItem.getContained(stack, world);
      if (opt.isPresent()) {
        Entity entity2 = opt.get();
        if (entity2 instanceof BeetleEntity) {
          ((BeetleEntity) entity2).setStat(BeetleStat.BB_SIZE, 7);
          ((BeetleEntity) entity2).setFlying(true);
          ((BeetleEntity) entity2).setOnGround(true);
        }
        entity2.prevYaw = 0;
        float g = 1f;
        float h = Math.max(entity2.getHeight(), entity2.getWidth());
        if (h >= 1f) {
          g /= h;
        }

        matrices.push();

        matrices.translate(0, -0.1, -0.5);
        matrices.scale(g, g, g);
        matrices.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(180));
        matrices.translate(0, -0.075, 0);

        var _entityRenderDispatcher = ((HeldItemRenderer) (Object) this).entityRenderDispatcher;
        _entityRenderDispatcher.setRenderShadows(false);
        _entityRenderDispatcher.render(entity2, 0.0, 0.0, 0.0, 0.0f, 0.0f, matrices, vcp, light);

        matrices.pop();
      }
    }
  }
}
