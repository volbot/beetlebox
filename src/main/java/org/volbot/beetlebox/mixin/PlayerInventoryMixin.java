package org.volbot.beetlebox.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Nameable;
import net.minecraft.world.World;
import org.volbot.beetlebox.item.equipment.BeetlepackUtils;
import org.volbot.beetlebox.item.equipment.BeetlepackInventory;
import org.volbot.beetlebox.item.tools.BeetleJarItem;
import org.volbot.beetlebox.item.tools.JarUtils;
import org.volbot.beetlebox.registry.ItemRegistry;
import org.volbot.beetlebox.config.BBConfigRegistry;

@Mixin(PlayerInventory.class)
public abstract class PlayerInventoryMixin implements Inventory, Nameable {

    @Inject(method = "insertStack(Lnet/minecraft/item/ItemStack;)Z", at = @At(value = "HEAD"), cancellable = true)
    public void insertStack(ItemStack stack, CallbackInfoReturnable<Boolean> info) {
        if (stack.getItem() instanceof BeetleJarItem) {
            if(stack.hasNbt()&&!stack.getNbt().contains("EntityType")){
                stack.setNbt(stack.getItem().getDefaultStack().getNbt());
            }
            PlayerEntity player = ((PlayerInventory) (Object) this).player;
            ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
            if (!bp.isEmpty()) {
                var bp_nbt = bp.getOrCreateNbt();
                if(!bp_nbt.contains("ToggleIntake")) {
                    bp_nbt.putBoolean("ToggleIntake",BBConfigRegistry.BEETLEPACK_TOGGLE_INTAKE);
                }
                if(!bp_nbt.getBoolean("ToggleIntake")) {
                    return;
                }
                BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
                for (int i = 0; i < bp_inv.size(); i++) {
                    ItemStack jar = bp_inv.getStack(i);
                    if (jar.isEmpty()) {
                        bp_inv.setStack(i,stack.copy());
                        bp_inv.markDirty();
                        stack.setCount(0);
                        info.setReturnValue(true);
                    } else if (ItemStack.canCombine(jar, stack)) {
                        stack.increment(bp_inv.getStack(i).getCount());
                        bp_inv.setStack(i,stack.copy());
                        bp_inv.markDirty();
                        stack.setCount(0);
                        info.setReturnValue(true);
                    }
                }
            }
        }
    }

    @Inject(method = "scrollInHotbar(D)V", at = @At(value = "HEAD"), cancellable = true)
    public void scrollInHotbar(double scrollAmount, CallbackInfo info) {
        PlayerInventory _this = ((PlayerInventory) (Object) this);
        ItemStack stack1 = _this.getStack(_this.selectedSlot);

        if(stack1.isOf(ItemRegistry.BUGSLINGER)) {
            PlayerEntity player = _this.player;
            if(player.isUsingItem() && player.getActiveItem()==stack1 && stack1.getOrCreateNbt().contains("EntityType")) {
                int bp_index = stack1.getOrCreateNbt().getInt("BPIndex");
                if (bp_index >= 0) {
                    ItemStack bp = BeetlepackUtils.getBeetlepackOnPlayer(player);
                    if (!bp.isEmpty()) {
                        BeetlepackInventory bp_inv = new BeetlepackInventory(bp);
                        if(bp_index >= 0) {
                            int i = bp_index;
                            int j = i+6;
                            int k = i-6;
                            while (i < j && i > k) {
                                i+=(int)scrollAmount;
                                if(i<0) {
                                    i+=6;
                                    j+=6;
                                    k+=6;
                                }
                                ItemStack jar1 = bp_inv.getStack(i%6);
                                if (jar1.getItem() instanceof BeetleJarItem && jar1.hasNbt() && jar1.getNbt().contains("EntityType")) {
                                    World world = player.getWorld();
                                    var jar1_entity = JarUtils.getEntityFromJar(jar1, world);
                                    if(jar1_entity.isPresent()) {
                                        ItemStack jar2 = bp_inv.getStack(bp_index);

                                        JarUtils.cloneJarNbt(stack1,jar2);
                                        JarUtils.cloneJarNbt(jar1,stack1);
                                        JarUtils.clearJarNbt(jar1);

                                        stack1.getOrCreateNbt().putInt("BPIndex",i%6);
                                        _this.markDirty();
                                        bp_inv.markDirty();

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                info.cancel();
            }
        }
    }
}
