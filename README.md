# beetlebox

&emsp;
<a href="https://discord.gg/GyMTgPGM9R" target="_blank" rel="noopener noreferrer">
<img src="https://gitlab.com/volbot/beetlebox/-/wikis/images/logo/BB_logo_discord.png" width=20%>
</a>&emsp;
<a href="https://modrinth.com/mod/beetlebox" target="_blank" rel="noopener noreferrer">
<img src="https://gitlab.com/volbot/beetlebox/-/wikis/images/logo/BB_logo_modrinth.png" width=20%>
</a>&emsp;
<a href="https://www.curseforge.com/minecraft/mc-mods/beetlebox" target="_blank" rel="noopener noreferrer">
<img src="https://gitlab.com/volbot/beetlebox/-/wikis/images/logo/BB_logo_curseforge.png" width=20%>
</a>&emsp;
<a href="https://gitlab.com/volbot/beetlebox/-/wikis/home" target="_blank" rel="noopener noreferrer">
<img src="https://gitlab.com/volbot/beetlebox/-/wikis/images/logo/BB_logo_wiki.png" width=20%>
</a>&emsp;
