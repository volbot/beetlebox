# Beetlebox 1.3 Changelog

## beetles

 - new beetles!
   - Asbolus verrucosus: the Blue Death Feigning Beetle
     - ability: Headbutt
     - other notes: Flightless; subject to gravity when fired
   - Goliathus albosignatus: the Goliath Beetle
     - ability: Headbutt
   - Theodosia viridiaurata: the Green Rhinoceros Beetle
     - ability: Pinch
   - Copris minutus: the Small Black Dung Beetle
     - ability: Flip
 - fights!
   - in the wild, beetles in groups of 3+ will sometimes get into fights!
   - when a beetle receives a hit it wouldn't survive, it will fly away and respawn nearby, and drop 1 or more elytron
 - abilities!
   - beetles now have unique, animated attacks based on their horns!
 - new models! more accurate, more detailed, and more animated!
 - added new sound effects for beetles and beetle projectiles!
 - some beetles have been renamed, and beetle-specific items now bear the scientific name of their source!
 - rewrote tamed beetle elytra flight

## quality of life

 - JEI support
   - boiler recipes
   - upgrade activation recipes

 - new creative tabs: Jelly and Beetle
 - the color of jelly is now dependent on its level

## tweaks

 - rewrote armor and item renderers for beetle products, so they generate automatically align with their spawn eggs. this should lower filesizes
 - removed Slimeball recipes for Gelatin Glue, and added a recipe to make Gelatin Glue directly into Slimeballs, to maximize compatibility
 - beetles no longer drop an elytron on death
 - removed elytron-to-dye recipes, now that they're more uncommon

## bugfixes

 - general streamlining of internal beetle/stat mechanisms
 - fixed boiler water rendering and configured capacity
 - fixed boiler being placeable facing "up" and "down" 
 - fixed bugslinger accepting non-beetles
 - fixed cactus syrup boiler recipe
 - fixed beetle armor speed upgrade recipe
 - fixed inaccurate stats on newly spawned beetles
